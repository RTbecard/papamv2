cask "rtbecard-papamv2-shortcut" do
  version :latest
  sha256 :no_check

  url "https://gitlab.com/RTbecard/papamv2/-/raw/master/scripts/homebrew/papamv2.zip"
  name "paPAMv2"
  desc "App shortcut for papamv2."
  homepage "https://gitlab.com/RTbecard/papamv2"

  container type: :zip
  depends_on formula: "papamv2"
  
  app "papamv2.app"
  
end
