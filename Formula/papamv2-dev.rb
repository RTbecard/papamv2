class Papamv2Dev < Formula
  include Language::Python::Virtualenv

  desc "Bioacoustic analysis for underwater sound."
  homepage "https://gitlab.com/RTbecard/papamv2"
  
  version "dev"
  url "https://gitlab.com/RTbecard/papamv2/-/archive/dev/papamv2-dev.tar.gz"

  license "GPL-3"
  
  depends_on "python@3.10"
  depends_on "freetype"
  depends_on "pyqt@5"

  def install
    # make venv
    system "sh", "./scripts/homebrew/make_env.sh"     

    # symlink homebrew's pyqt version
    libs_pyqt = Dir.glob "#{HOMEBREW_PREFIX}/lib/python3.10/site-packages/PyQt5*"
    puts "------ PyQt5 packages to symlink"
    puts libs_pyqt
    ln_s libs_pyqt, "./venv/lib/python3.10/site-packages"
    puts "------ Show venv package directory contents"
    system "ls", "./venv/lib/python3.10/site-packages/"

    # Install papam python package
    system "sh", "./scripts/homebrew/install_papam.sh"
    # move it to libexec
    libexec.install Dir["venv/*"]
    # add exec file
    bin.install "./scripts/homebrew/papamv2-dev"
    
  end

end
