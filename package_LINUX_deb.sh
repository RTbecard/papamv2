#!/bin/bash

# This script will make a deb package for easy installation into debian-based
# linux systems.  You must have run the script "./build_portable_UNIX.sh" before
# hand, as this script simply packages the portable program made by pyinstaller
# into a deb file structure.

set -e

echo "Making debian Package..."

# Get version number from script
#VERSION=$(grep -oP "(?<=version = )[\d\.]+" ./src/setup.py)
VERSION=$(head -n 1 ./src/papamv2/VERSION)
# Set package name
PKG_NAME="papam_${VERSION}_amd64"
echo "Package name: $PKG_NAME"

if [ -d $PKG_NAME ]; then
    echo "Clearing old directory"
    rm -R ${PKG_NAME}
fi

# =============== Package directory
echo "Make package directory structure"
mkdir -p "${PKG_NAME}/DEBIAN"
mkdir -p "${PKG_NAME}/opt"
mkdir -p "${PKG_NAME}/usr/share/applications"

echo "Make Control File"
cat <<- EOF > "${PKG_NAME}/DEBIAN/control"
	Package: papam
	Version: ${VERSION}
	Architecture: amd64
	Essential: no
	Priority: optional
	Depends: python3.10, build-essential, python3.10-dev, libfreetype6-dev, pkg-config, libqhull-dev, python3-venv
	Maintainer: James Adam Campbell (jamesadamcampbell@gmail.com)
	Description: Bioacoustic analysis for underwater particle motion and sound pressure.
EOF

# ================ Copy protable build into package
# Check that program has been built
SRC="./papamv2-${VERSION}.tar.gz"
if [ ! -f "${SRC}" ]; then
    echo "Source package ${SRC} not found.  Make sure to run \`make all\`. first"
    exit 1  # Exit with general error
fi
# make package directory
APP_DIR="${PKG_NAME}/opt/papam/"
mkdir $APP_DIR
# Copy source package
cp "${SRC}" "$APP_DIR"
#cp "./scripts/deb_package/make_venv.sh" $APP_DIR
sed "s/VERSION/$VERSION/" "./scripts/deb_package/make_venv.sh" >> "$APP_DIR/make_venv.sh"
# copy vector icon
cp "./icons/papam.svg" "$APP_DIR"
# copy pypi requirements
cp "./requirements_UNIX.txt" "$APP_DIR"

# ================ Build dekstop file
cat <<- EOF > "${PKG_NAME}/usr/share/applications/papam.desktop"
	[Desktop Entry]
	Type=Application
	Version=${VERSION}
	Name=paPAMv2
	Comment=Bioacoustic Analysis
	Path=/opt/papam/
	Exec=/opt/papam/run_papamv2
	Terminal=False
	Icon=/opt/papam/papam.svg
EOF

# =============== Post install script
# Script will symlink the papam binary to the opt/papam folder
# Post install
cat <<- END > "${PKG_NAME}/DEBIAN/postinst"
#!/bin/bash
set -e
cd /opt/papam
echo "Making python venv"
bash ./make_venv.sh
END
chmod +x "${PKG_NAME}/DEBIAN/postinst"

# Post removal
#cp "./scripts/deb_package/postrm" "${PKG_NAME}/DEBIAN/"
cat <<- END > "${PKG_NAME}/DEBIAN/postrm"
#!/bin/bash
if [ -d /opt/papam/venv ]; then
    rm -r /opt/papam/venv
fi
END
chmod +x "${PKG_NAME}/DEBIAN/postrm"

# Preinstall
cp "${PKG_NAME}/DEBIAN/postrm" "${PKG_NAME}/DEBIAN/preinst"
chmod +x "${PKG_NAME}/DEBIAN/preinst"

echo "Package structure build!"

echo "Wrapping in deb..."
dpkg-deb -bv ${PKG_NAME}

echo "Cleaning up..."
rm -r ${PKG_NAME}
echo "Finished!"
