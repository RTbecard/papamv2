> This software is currently under development, so if you encounter any bugs (which is very likely) please let me know via email or raising an issue on this repository.

# paPAMv2

paPAMv2 is an overhaul of the MATLAB-based [paPAMv1](https://gitlab.com/RTbecard/paPAM) software for bioacoustic analysis (Nedelec et al. 2016).  Broadly, paPAMv2 provides a comprehensive end-to-end acoustic analysis framework under a single graphical user interface with a particular focus on particle motion.

If you'd like to see a specific new feature added to paPAMv2, feel free to submit an issue with the tag `feature-request`.

![screenshot](./screenshot.png)

### User Guide

You can find the user guide on this [website](https://rtbecard.gitlab.io/papamv2/), or download it as a [pdf book](https://rtbecard.gitlab.io/papamv2/user-guide.pdf).
If your new to paPAMv2, we suggest first skimming through this.
It covers the basic features of paPAMv2's processing pipeline along with detailed descriptions on the math behind the scenes.

Note that we've also written a lot of tool tips within the paPAMv2 user interface.
If you hover your mouse over an option, an explanatory text box usually pops up.

### Downloads

The links below should provide the latest stable version of paPAMv2.

- Python Package
	- [Source package (tar.gz)](https://gitlab.com/RTbecard/papamv2/-/jobs/artifacts/master/raw/papam_source.tar.gz?job=build-ubuntu)

- Installers
	- [Ubuntu 22.04 (deb package)](https://gitlab.com/RTbecard/papamv2/-/jobs/artifacts/master/raw/papam_amd64.deb?job=build-ubuntu)
	- [Windows 10 (Setup.exe)](https://gitlab.com/RTbecard/papamv2/-/jobs/artifacts/master/raw/papamv2_setup.exe?job=build-windows)

### Installing paPAMv2: Windows & Ubuntu

On **Windows 10** or **Ubuntu**, simply download the `deb` or `setup.exe` files listed above.  For Ubuntu, you'll need a C/C++ compiler.  See the section below on how to make sure you have that dependency.

### Installing paPAMv2: MacOS

For **MacOS**, you can get paPAMv2 with [homebrew](https://brew.sh).
Before running this, make sure you have Xcode installed from the app store (_Xcode Command Line Tools_ is not sufficient here, you need the full app store version).
When Xcode and homebrew are installed, go ahead and install paPAMv2 with the following terminal commands:

```sh
# Add the tap (the repository holding the build formula)
brew tap rtbecard/papamv2 https://gitlab.com/RTbecard/papamv2.git
# Update tap (if you had already added it)
brew update
# Clear homebrew's download cache
brew cleanup --prune=all
# Install papamv2
brew install -v papamv2
# Run papam from command line
papamv2
```
Running paPAMv2 from the terminal isn't ideal.  To bypass this, we've provided an extra app package that acts as a shortcut to launch paPAMv2 from your _Applications_ folder.

```sh
# Get App package shortcut for papam
brew install rtbecard-papamv2-shortcut
```
The first time you launch the shortcut you may run into a security warning.  See [here](https://support.apple.com/en-ca/guide/mac-help/mh40616/mac) for directions on how to get around this.

Finally, to update paPAMv2, you can run the following lines:

```sh
# Upgrade to the latest version of papamv2
brew update && brew upgrade papamv2
```

### Running paPAMv2 as a Python Module

You can also run paPAMv2 directly as a python module.  After downloading the source package above, you can install it with `pip`.

```sh
# Install package
pip3 install papamv2_source.tar.gz
# Launch papamv2
python3 -m papamv2
```

paPAMv2 has quite a few dependencies, so its a good idea to install and run it from a virtual environment.
For example, using `venv`:

```sh
# Make virtual environment
python3 -m venv ./venv_papamv2
# Activate environment
source ./venv_papamv2/bin/activate
# Install papamv2 and dependant packages
pip3 install -r requirements_UNIX.txt
pip3 install papamv2_source.tar.gz
# Run papamv2
python3 -m papamv2
# Deactivate venv after session
deactivate
```

### Installing a C/C++ Compiler

paPAMv2 has some Cython code bundled inside.  Hence, to run or install paPAMv2 you'll need a C/C++ compiler on your machine  --- this step isn't necessary for the windows installer.  Below are some quick instructions for each operating system.

- **OSX**: gcc is available through [Command Line Tools for XCode](https://developer.apple.com/download/all/).  Run `xcode-select --install` in your terminal to automatically download and install it.
If you already have the command line tools, it may be worth upgrading it with `softwareupdate --install -a `.
- **Ubuntu**:  Simply run `sudo apt install build-essential`.
- **Windows 10**:  Download and install the _community edition_ of [Visual Studio 2022](https://visualstudio.microsoft.com/downloads/).  When running the installer, select the desktop c++ option and make sure the following additional options are selected: _MSVCv* VS2019 C++ x64/x86 build tools_ and _Windows 10 SDK_.

### Reporting Figures in R

papamv2 automatically prints `png` format figures for those analysis options which support a graphical output. When publishing your results, however, we suggest creating new plots using `ggplot2` in R.
All the data required for creating new figures can be found in the resulting `plots_data` folder after you run an analysis in paPAMv2. To streamline this process, we provide a supplementary R package.

 - ggpapam R package:
   - [Source package (tar.gz)](https://gitlab.com/RTbecard/papamv2/-/jobs/artifacts/master/raw/ggpapam_source.tar.gz?job=build-r)

You can download the above source package and install it in R with:
```r
install.packages("ggpapam_source.tar.gz")
```

ggpapam has a suite of helper functions that return ggplot2 objects.
Here's a handy example R script:

```r
require(ggpapam)

# Set path containing papamv2 output
path <- "/home/james/Downloads/test_plot/plots_data/"

# Load filenames for each plot type
files_waveform <- dir(path, pattern = ".*waveform.*bin$", full.names = T)
files_analyticEnvelope <- dir(path, pattern = ".*analyticEnvelope.*bin$", full.names = T)
files_dd_exposure <- dir(path, pattern = ".*dd_exposure.*csv$", full.names = T)
files_dd_rms <- dir(path, pattern = ".*dd_rms.*csv$", full.names = T)
files_psd <- dir(path, pattern = ".*psd.*csv$", full.names = T)
files_esd <- dir(path, pattern = ".*esd.*csv$", full.names = T)
files_specprobdens <- dir(path, pattern = ".*spectralProbabilityDensity.*csv$", full.names = T)
files_spectrogram <- dir(path, pattern = ".*spectrogram.*csv$", full.names = T)
files_dirspec <- dir(path, pattern = ".*directional-spectrogram.*csv$", full.names = T)
files_percent <- dir(path, pattern = ".*PSDPercentiles.*csv$", full.names = T)

# Make Plots
gg_power_spectral_density(read.csv(files_psd[1]), units = "pressure")
gg_energy_spectral_density(read.csv(files_esd[1]), units = "pressure")
gg_analytic_envelope(files_analyticEnvelope[1], units = "pressure")
gg_waveform(files_waveform[1], units = "pressure")
gg_decidecade_rms(read.csv(files_dd_rms[1]), units = "pressure")
gg_decidecade_exposure(read.csv(files_dd_exposure[1]), units = "pressure")
gg_spectral_probability_density(read.csv(files_specprobdens[1]), units = "pressure")
gg_spectrogram(read.csv(files_spectrogram[1]), units = "pressure")
gg_directional_spectrogram(read.csv(files_dirspec[1]), units = "pressure", type = "bearing")
```

Be sure to read the help docs for each plotting function (`?gg_power_spectral_density`) to get a handle on how you can easily customize the resulting plots.

Here's and example of plotting Spectral Probability Density.

```r
# Load data.frames
df_specprob <- read.csv(files_specprobdens[1])
df_prcnt <- read.csv(files_percent[1])
# Convert percentiles data to long format
df_prcnt_long <- reshape(
  df_prcnt, varying = 4:6, direction = 'long',
  timevar = "Percentile", times = c("0.05", "0.5", "0.95"), v.names = "PSD")

# Plot SPD raster
plot <- gg_spectral_probability_density(
  df_specprob, units = "pressure", legend = "bottom", colorbar_length = 0.15)
# Add PSD percentile lines
plot <- plot + geom_line(
  data = df_prcnt_long, aes(x = Frequency, y = PSD, group = Percentile),
  linetype = 1, color = 'Tomato')
# Extra Formatting
plot <- plot + xlim(1000, 10000) +
  ggtitle("Example Spectral Probability Density with Percentiles")

# Save plot
jpeg("example_plot.jpeg", units = "cm", width = 18, height = 15, res = 150)
print(plot)
dev.off()
```

<img src="example_plot.jpeg" width="800">

### References

- Nedelec, S. L., Campbell, J., Radford, A. N., Simpson, S. D., Merchant, N. D.  2016.  **Particle motion: the missing link in underwater acoustic ecology**.  *Methods in Ecology and Evolution*.  [doi:10.1111/2041-210X.12544](https://doi.org/10.1111/2041-210X.12544).

