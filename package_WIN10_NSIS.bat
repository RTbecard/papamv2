REM Set environmental var so cython libraries are not compiled
SET COMPILE_PAPAM=NONE

REM get python packages
python -m pip install pynsist
python -m pip install -r requirements_WIN10.txt
IF %ERRORLEVEL% NEQ 0 (
	echo "!!!!!!     Failed installing python packages     !!!!!!"
	EXIT /B %ERRORLEVEL%
)

REM Build papamv2 wheel
cd .\src
python setup.py bdist_wheel
IF %ERRORLEVEL% NEQ 0 (
	echo "!!!!!!     Failed building papamv2 wheel     !!!!!!"
	EXIT /B %ERRORLEVEL%
)

REM Make nsis installer
cd ..\
python -m nsist installer_NSIS.cfg
IF %ERRORLEVEL% NEQ 0 (
	echo "!!!!!!     Failed building NSIS installer     !!!!!!"
	EXIT /B %ERRORLEVEL%
)
