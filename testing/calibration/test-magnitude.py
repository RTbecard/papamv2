#!/bin/python3
# install latest papamv2 version with:
# cd ../../src/papamv2
# ./build.sh
# pip3 install dist/papamv2-2.0.3.tar.gz

# This script will test magnitude and phase calibration functions for papamv2

from os import path
import sys

import logging as log
import faulthandler

import numpy as np
import pyqtgraph as pqg

from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout

from papamv2.calibration.calibration import Calibration
from papamv2.calibration import plot as calibplot

# ========================= Script parameters =================================
fs = 44100
pth = path.expanduser("~/Dropbox/papamv2-testing")
file_mag = path.join(pth, "M20-040dBreVnms.csv")
file_phase = path.join(pth, "M20-040_phase_radians.csv")
# =============================================================================

# Setup debug messages
log.basicConfig(level = log.DEBUG)
log.basicConfig()
log.getLogger('papamv2').setLevel(log.DEBUG)

# Open plotting window
app = QApplication(sys.argv)
window = QWidget()
layout = QGridLayout()
window.setLayout(layout)

# Make calibration object and read calibration data
cal = Calibration(fs)
cal.read(file_mag, file_phase, (0,0,0,0), unit_in = "v")

# Create calibration impulses
cal.createCalibrationImpulses("v")
cal.createCalibrationImpulses("a")
cal.createCalibrationImpulses("p")

# Make plots
pw_mag_v = calibplot.plot_mag_pyqt(cal, unit_out = "v")
pw_mag_a = calibplot.plot_mag_pyqt(cal, unit_out = "a")
pw_phase_a = calibplot.plot_phase_pyqt(cal, unit_out = "a")
pw_phase_p = calibplot.plot_phase_pyqt(cal, unit_out = "p")

# Attach plots to window
layout.addWidget(pw_mag_v, 0, 0)
layout.addWidget(pw_mag_a, 0, 1)
layout.addWidget(pw_phase_a, 1, 0)
layout.addWidget(pw_phase_p, 1, 1)

# Plot impulse responses
x = np.array(range(0, len(cal.impulse_phase['p']['p'])))
plot_impulse_a = pqg.plot(x, cal.impulse_phase['x']['a'])
layout.addWidget(plot_impulse_a, 2, 0)
plot_impulse_p = pqg.plot(x, cal.impulse_phase['p']['p'])
layout.addWidget(plot_impulse_p, 2, 1)

# ------ Show plots
window.show()  # IMPORTANT!!!!! Windows are hidden by default.
app.exec()
