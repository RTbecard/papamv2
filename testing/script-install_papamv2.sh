#!/bin/bash

# This script will install the papamv2 python package so it can be tested in the
# scripts within this directory.

set -ev

# Move to script's directory
cd "${0%/*}"

# Build package
export COMPILE_PAPAM="C"
cd ../src/
python3 setup.py build_ext sdist

# Install package
file=dist/papamv2-*.tar.gz
# file=dist/papamv2-*.whl
pip3 install --force-reinstall $file
