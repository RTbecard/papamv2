#!/bin/bash

set -e

# ====== Windows
# Make icon bitmaps
inkscape -V
inkscape -w 16  -h 16  -o 16.png  papam.svg
inkscape -w 32  -h 32  -o 32.png  papam.svg
inkscape -w 48  -h 48  -o 48.png  papam.svg
inkscape -w 64  -h 64  -o 64.png  papam.svg
inkscape -w 128 -h 128 -o 128.png papam.svg
inkscape -w 256 -h 256 -o 256.png papam.svg

convert -background transparent 16.png 32.png 48.png 64.png 128.png 256.png icon.ico

# Clean temp files
find . -type f -name "*.png" -delete

mv icon.ico ../src/papamv2/gui/icon.ico


# ====== OSX
mkdir -p ./iconset
inkscape -o "./iconset/icon_16x16.png"      -w   16 -h   16 "papam.svg"
#inkscape -o "./iconset/icon_16x16@2x.png"   -w   32 -h   32 "papam.svg"
inkscape -o "./iconset/icon_32x32.png"      -w   32 -h   32 "papam.svg"
#inkscape -o "./iconset/icon_32x32@2x.png"   -w   64 -h   64 "papam.svg"
inkscape -o "./iconset/icon_128x128.png"    -w  128 -h  128 "papam.svg"
#inkscape -o "./iconset/icon_128x128@2x.png" -w  256 -h  256 "papam.svg"
inkscape -o "./iconset/icon_256x256.png"    -w  256 -h  256 "papam.svg"
#inkscape -o "./iconset/icon_256x256@2x.png" -w  512 -h  512 "papam.svg"
inkscape -o "./iconset/icon_512x512.png"    -w  512 -h  512 "papam.svg"
#inkscape -o "./iconset/icon_512x512@2x.png" -w 1024 -h 1024 "papam.svg"

# sudo apt-get install icnsutils
png2icns "./papam.icns" $(echo $(find "./iconset" -type f -name "*.png") | sed 's/\n/\s/')
rm -R "./iconset"
mv papam.icns ../src/papamv2/gui/papam.icns

