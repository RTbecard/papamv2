from PyInstaller.utils.hooks import collect_data_files
import os

datas = collect_data_files('papamv2')

# get help.txt (this doesnt get caugh in above line)
import papamv2
from os import path
pth = path.dirname(papamv2.__file__)
pt_help = path.join(pth, 'help.txt')
pt_icon = path.join(pth, 'gui', 'icon.ico')
pt_icon2 = path.join(pth, 'gui', 'icon_dock.png')
pt_icon3 = path.join(pth, 'gui', 'icon_undock.png')
pt_ver = path.join(pth, 'VERSION')

VERSION = path.join(path.dirname(papamv2.__file__), 'VERSION')
datas += [(pt_help, 'papamv2')]
datas += [(pt_ver, 'papamv2')]
datas += [(pt_icon, 'papamv2/gui')]
datas += [(pt_icon2, 'papamv2/gui')]
datas += [(pt_icon3, 'papamv2/gui')]


# Print hooked files (for debugging)
print("------ papamv2: Show hooked data files...")
print(datas)

hiddenimports = ['papamv2.gui', 'papamv2.__main__', 'papamv2.spectral_ellipses_c']

# Print hidden imports files (for debugging)
print("------ papamv2: Show hidden imports files...")
print(hiddenimports)

