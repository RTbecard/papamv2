from PyInstaller.utils.hooks import collect_data_files, collect_submodules
import os

# get help.txt (this doesnt get caugh in above line)
import pyqtgraph
from os import path
pth = path.dirname(pyqtgraph.__file__)

hiddenimports = collect_submodules("pyqtgraph", lambda x: "Template" in x)

# Manually add template files.  This is a workaround for a gitlabCI build issue...
# I think its related to building on a headless machine which causes teh above line to miss these
# modules.
hiddenimports += ['pyqtgraph.examples.VideoTemplate_pyqt5',
 'pyqtgraph.canvas.TransformGuiTemplate_pyside6',
 'pyqtgraph.flowchart.FlowchartTemplate_pyqt6',
 'pyqtgraph.imageview.ImageViewTemplate_pyside6',
 'pyqtgraph.GraphicsScene.exportDialogTemplate_pyside6',
 'pyqtgraph.graphicsItems.ViewBox.axisCtrlTemplate_pyside6',
 'pyqtgraph.graphicsItems.PlotItem.plotConfigTemplate_pyside2',
 'pyqtgraph.flowchart.FlowchartCtrlTemplate_pyside6',
 'pyqtgraph.graphicsItems.PlotItem.plotConfigTemplate_pyqt6',
 'pyqtgraph.canvas.TransformGuiTemplate_pyside2',
 'pyqtgraph.GraphicsScene.exportDialogTemplate_pyside2',
 'pyqtgraph.GraphicsScene.exportDialogTemplate_pyqt5',
 'pyqtgraph.examples.VideoTemplate_pyqt6',
 'pyqtgraph.graphicsItems.PlotItem.plotConfigTemplate_pyside6',
 'pyqtgraph.canvas.CanvasTemplate_pyside2',
 'pyqtgraph.flowchart.FlowchartTemplate_pyside6',
 'pyqtgraph.GraphicsScene.exportDialogTemplate_pyqt6',
 'pyqtgraph.canvas.CanvasTemplate_pyqt5',
 'pyqtgraph.examples.exampleLoaderTemplate_pyside6',
 'pyqtgraph.flowchart.FlowchartCtrlTemplate_pyqt5',
 'pyqtgraph.imageview.ImageViewTemplate_pyside2',
 'pyqtgraph.graphicsItems.ViewBox.axisCtrlTemplate_pyside2',
 'pyqtgraph.canvas.CanvasTemplate_pyqt6',
 'pyqtgraph.graphicsItems.PlotItem.plotConfigTemplate_pyqt5',
 'pyqtgraph.examples.exampleLoaderTemplate_pyqt6',
 'pyqtgraph.flowchart.FlowchartTemplate_pyside2',
 'pyqtgraph.imageview.ImageViewTemplate_pyqt5',
 'pyqtgraph.imageview.ImageViewTemplate_pyqt6',
 'pyqtgraph.examples.VideoTemplate_pyside2',
 'pyqtgraph.graphicsItems.ViewBox.axisCtrlTemplate_pyqt5',
 'pyqtgraph.examples.exampleLoaderTemplate_pyside2',
 'pyqtgraph.flowchart.FlowchartCtrlTemplate_pyqt6',
 'pyqtgraph.canvas.CanvasTemplate_pyside6',
 'pyqtgraph.canvas.TransformGuiTemplate_pyqt5',
 'pyqtgraph.examples.exampleLoaderTemplate_pyqt5',
 'pyqtgraph.graphicsItems.ViewBox.axisCtrlTemplate_pyqt6',
 'pyqtgraph.flowchart.FlowchartTemplate_pyqt5',
 'pyqtgraph.flowchart.FlowchartCtrlTemplate_pyside2',
 'pyqtgraph.canvas.TransformGuiTemplate_pyqt6',
 'pyqtgraph',
 'pyqtgraph.examples.VideoTemplate_pyside6']

print("------ pyqtgraph: Show hidden imports...")
print(hiddenimports)


datas = collect_data_files("pyqtgraph", includes=["**/*.ui", "**/*.png", "**/*.svg"])
# Add missing hooks for color map data
pt_colormaps = path.join(pth, 'colors', 'maps', '*.csv')
datas += [(pt_colormaps, 'pyqtgraph/colors/maps/')]
# Print hooked files (for debugging)
print("------ pyqtgraph: Show hooked data files...")
print(datas)


