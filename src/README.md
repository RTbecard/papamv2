# paPAMv2: Bioacoustic Analysis of Acoustic Particle Motion and Sound Pressure

### Installation

```python
python3 -m pip3 install papam 
```

### Usage

```python
# Graphical Interface
python3 -m papam --gui
# CLI interface
python3 -m papam -h
```

