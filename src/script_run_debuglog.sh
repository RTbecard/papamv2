#!/bin/python3

import logging as log

import faulthandler

import papamv2
import papamv2.__main__

# Setup debug messages
log.basicConfig(level = log.DEBUG)
log.basicConfig()
log.getLogger('papamv2').setLevel(log.DEBUG)

# Enable faulthandler (debug segmentation faults)
faulthandler.enable()

# Run program
papamv2.__main__.main()
