#!/bin/python3

import os
from Cython.Build import cythonize

# for live editing module
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
from numpy import cos, sin, tan, arccos, arcsin, arctan, array
from numpy.random import randn

# build c source file
cythonize("papamv2/spectral_ellipses_c.pyx", annotate = True,
          include_path = [np.get_include()])
# recompile so file
os.system('python3 setup.py build_ext --inplace')

from papamv2 import spectral_ellipses_c as sec

#matplotlib.use('GTK3Agg')
#%matplotlib
# Make ellipse
ax = plt.figure().add_subplot(projection='3d')

# ============ Simulate random complex spectrum
# Generate random complex numbers
cmplx =  np.array([randn() + randn()*1j*np.pi, randn() + randn()*1j*np.pi, randn() + randn()*1j*np.pi,])

# ------ tone parameters
scale_x = np.abs(cmplx[0])
scale_y = np.abs(cmplx[1])
scale_z = np.abs(cmplx[2])

phase_x = np.angle(cmplx[0])
phase_y = np.angle(cmplx[1])
phase_z = np.angle(cmplx[2])

scales = np.array([scale_x, scale_y, scale_z])
phases = np.array([phase_x, phase_y, phase_z])

print("Complex spectrum: %s" % cmplx)
print("Phase: %s" % phases)
print("Scale: %s" % scales)
d = phases[0] - phases[1]
if d < 0:
    d += 2*np.pi
# Keep within 2 pi
if d > 2*np.pi:
    d %= (2*np.pi)
print("d: %s" % d)

# ------ Calculate major/minor axes
ret_vals = sec.directional_pm_py(cmplx)

ellip = sec.get_ellipse_py(cmplx)

print(ret_vals)
print(ellip)
a = ellip[0]
b = ellip[1]

# ====== plot results
ax.clear()
t = np.arange(0, 2*np.pi + 0.1, 0.1)
x = cos(t + phase_x)*scale_x
y = cos(t + phase_y)*scale_y
z = cos(t + phase_z)*scale_z
# plot ellipse
plt.plot(x, y, z)
# plot projected ellipses
plt.plot(x, np.ones(len(t))*scale_y, z)
plt.plot(-np.ones(len(t))*scale_x, y, z)
plt.plot(x, y, -np.ones(len(t))*scale_z)
# plot free ellipse axis
plt.plot((0, a[0]), (0, a[1]), (0, a[2]), color = 'r')
plt.plot((0, b[0]), (0, b[1]), (0, b[2]), color = 'b')
# format plot
plt.grid(True)
plt.xlim(-scale_x, scale_x)
plt.ylim(-scale_y, scale_y)
ax.set_box_aspect([scale_x, scale_y, scale_z])
ax.set_zlim(-scale_z, scale_z)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")

plt.show()
