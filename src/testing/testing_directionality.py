import os
import time

from Cython.Build import cythonize

import numpy as np
from papamv2.analysis import directional_spectrogram as dir_spec

# ============================== build cython code ============================
# build c source file
cythonize("papamv2/spectral_ellipses_c.pyx", annotate = True,
          include_path = [np.get_include()])
# recompile so file
os.system('python3 setup.py build_ext --inplace')

import papamv2.spectral_ellipses_c as sec
import papamv2.spectral_ellipses as se

# ================= Bencmark localization algorithem ==========================
# code profiling

# make fake signal
x = dir_spec.onesided_spectrum(np.random.randn(30000))
y = dir_spec.onesided_spectrum(np.random.randn(30000))
z = dir_spec.onesided_spectrum(np.random.randn(30000))

print(x.dtype)

spectrum_vector = np.array([x, y, z])

print(spectrum_vector.shape)
print("head:\n ", spectrum_vector[:,0:2])
print("tail: \n", spectrum_vector[:,-3:])

print("============ Benchmark pythyon")
def test_py():
      np.apply_along_axis(se.directional_pm, 0, spectrum_vector[:, 1:])
# cProfile.run('test_py()', sort = 'time')
clk = time.time()
test_py()
print("------ time: ", time.time() - clk)

print("============ Benchmark C")
def test_cy():
     sec.localize_pm(spectrum_vector[:,1:-1].copy())
# cProfile.run('test_cy()', sort = 'time')
clk = time.time()
test_cy()
print("------ time: ", time.time() - clk)


