# The environment variable COMPILE_PAPAM can be used to change the behaviour of Cython
# compiling.
# The default behaviour is that only C libraries will be compiled from the
# existing C source files.  For testing changes to the source code, you'll
# want to also regenerate this C source code from Cython's pyx files.  Set
# COMPILE_PAPAM="ALL" for this. When distributing, I package the compiled windows
# libraries in the distribution so users dont have to have a C compiler on
# their system. COMPILE_PAPAM="NONE" does this.

import os
from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize
import platform
import numpy, scipy

# get descrition test
with open("README.md", "r") as fh:
    long_description = fh.read()

# get version number
with open("papamv2/VERSION", "r") as fh:
    VERSION= fh.read().strip()

# To include cython libraries, see:
#https://cython.readthedocs.io/en/latest/src/userguide/source_files_and_compilation.html

# get environment variable (on windows there are problems matching this string, hence the strip functions)
CMPL = os.getenv('COMPILE_PAPAM')
print("CMPL: %s" % CMPL)

if  CMPL is None or "ALL" in CMPL:
    print("Compiling pyx and C/C++ code")
    args = {'ext_modules': cythonize(
      "papamv2/spectral_ellipses_c.pyx",
      annotate = True,
      include_path = [numpy.get_include()])}

elif "C" in CMPL:
    print("Compiling C code")
    args = {"ext_modules": [Extension(
        "papamv2.spectral_ellipses_c",
        ["papamv2/spectral_ellipses_c.c"],
        include_dirs=[numpy.get_include()])]}

elif "NONE" in CMPL:
    print("Compile nothing (use precompiled libraries)")
    args = {}
else:
    raise ValueError("Invalid COMPILE_PAPAM")


setup(name = 'papamv2',
      version = VERSION,
      description = 'Bioacoustic analysis for underwater particle motion and sound pressure.',
      url = 'https://gitlab.com/RTbecard/papamv2',
      author = 'James Campbell',
      author_email = 'jamesadamcampbell@gmail.com',
      license = 'GPL-3',
      license_files = ('../LICENSE', '../NOTICE'),
      install_requires = ['scipy>=1.6.2', 'numpy>=1.20.2',
                          'matplotlib>=3.2.0', 'PyQt5>=5.15', 'dill',
                           'Cython>=0.29.26', 'pyqtgraph>=0.12.3', 'colorcet>=3.0.0'],
      packages = ['papamv2', 'papamv2.analysis_directional', 'papamv2.analysis', 'papamv2.gui', 'papamv2.calibration'],
      package_dir = {'papamv2': 'papamv2'},
      scripts=["bin/papamv2"],
      zip_safe = False,
      include_package_data = True,
      long_description = long_description,
      long_description_content_type = "text/markdown",
      include_dirs = [numpy.get_include()],
      **args)

# MANIFEST.in holds paths to package data

