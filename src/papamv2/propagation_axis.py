"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions for:
 - Detecting the progataion axis of the signal.
 - Reprojecting and changing the orientation of the vector sensor.
"""

import logging

import numpy as np
import scipy.sparse.linalg

log = logging.getLogger('papamv2')

def getSIAxis(subset_p, subset_v):
    """
    Parameters
    ----------
    data_i: numpy.ndarray
        A 2D array holding the sound intensity signals for each channel of a
        vector sensor.
    channels: str
        A string holding the channel labels for data_i rows.

    Return
    ------
    (float, float, numpy.ndarray)
        A 3 item tuple holding the pitch, yaw, and mean vector of intensity in
        radians.
    """
    # Convert pm channels to sound intensity
    for i in range(0, subset_v.data.shape[0]):
        # Convert velocity to intensity
        subset_v.data[i, :] = subset_v.data[i, :]*subset_p.data[0, :]*1e3
    subset = subset_v

    # ------ Calculate pitch and yaw
    # get xyz channel indexes
    # Calc mean intensity per channel
    # (This is the direction of propagation per channel)
    use_z = 'z' in subset.channels
    SI_x = np.mean(subset['x'])
    SI_y = np.mean(subset['y'])
    if use_z:
        SI_z = np.mean(subset['z'])
    else:
        SI_z = 0

    SI_vec = np.array([SI_x, SI_y, SI_z])

    p_mean = (np.mean(subset_p.data[0, :]**2)**0.5)
    si_mean = (SI_x**2 + SI_y**2 + SI_z**2)**0.5
    pii = 10*np.log10(p_mean**2 / si_mean)

    log.debug("Propagation vector: %s", SI_vec)

    return SI_vec, pii

def getPVAxis(subset):
    """
    Use singular valued decomposition to find the major axis of variance in
    the particle velocity dataset.

    Parameters
    ----------
    data: numpy.ndarray
        A 2D array holding the particle velocity signals for each channel of a
        vector sensor.
    channels: str
        A string holding the channel labels for data_i rows.

    Return
    ------
    (float, float, numpy.ndarray)
        A 3 item tuple holding the pitch, yaw, and mean vector of vecocity in
        radians.
    """

    # chi values represent 95 percentiles
    if 'z' in subset.channels:
        X = np.array([subset['x'], subset['y'], subset['z']])
        chi = 7.81
    else:
        X = np.array([subset['x'], subset['y']])
        chi = 5.991

    # Calc covariace of point cloud
    sigma = np.cov(X)
    # apply SVD
    #_,s,v = np.linalg.svd(sigma, full_matrices = False)
    _,s,v = np.linalg.svd(sigma, full_matrices = True, hermitian = True)
    # get scale values in decreasing order
    order = np.argsort(s)[::-1]
    # Get length of major/minor axes
    a = (chi*s[order[0]])**0.5
    b = (chi*s[order[1]])**0.5
    # Get vector of major axis
    # length indicates 95th percentile of datapoints (multivariate gaussian)
    v = v[order[0]]*a

    # Add z component of vector if missing
    if len(v) < 3:
        v = np.append(v, [0])

    # Calculate eccentricity
    e = np.sqrt(1 - (b/a)**2)

    log.debug("sigma: %s", sigma)
    log.debug("s: %s", s)
    log.debug("v: %s", v)
    log.debug("a: %s", a)
    log.debug("b: %s", b)
    log.debug("e: %s", e)

    # Rotate to keep positive x values (180 deg ambiguity problem)
    if v[0] < 0:
        v = -v

    return v, e

def getPVAxisArray(arr):
    """
    Use singular valued decomposition to find the major axis of variance in
    the particle velocity dataset.

    Parameters
    ----------
    data: numpy.ndarray
        A 2D array holding the particle velocity signals for each channel of a
        vector sensor.
    channels: str
        A string holding the channel labels for data_i rows.

    Return
    ------
    (float, float, numpy.ndarray)
        A 3 item tuple holding the pitch, yaw, and mean vector of vecocity in
        radians.
    """

    if arr.shape[0] == 3:
        chi = 7.81
    else:
        chi = 5.991

    # Calc covariace of point cloud
    sigma = np.cov(arr)
    # apply SVD
    #_,s,v = np.linalg.svd(sigma, full_matrices = False)
    _,s,v = np.linalg.svd(sigma, full_matrices = True, hermitian = True)
    # get scale values in decreasing order
    order = np.argsort(s)[::-1]
    # Get length of major/minor axes
    a = (chi*s[order[0]])**0.5
    b = (chi*s[order[1]])**0.5
    # Get vector of major axis
    # length indicates 95th percentile of datapoints (multivariate gaussian)
    v = v[order[0]]*a

    # Add z component of vector if mossing
    if len(v) < 3:
        v = np.append(v, [0])

    # Calculate eccentricity
    e = np.sqrt(1 - (b/a)**2)

    log.debug("sigma: %s", sigma)
    log.debug("s: %s", s)
    log.debug("v: %s", v)
    log.debug("a: %s", a)
    log.debug("b: %s", b)
    log.debug("e: %s", e)

    # Rotate to keep positive x values (180 deg ambiguity problem)
    if v[0] < 0:
        v = -v

    return v, e

def vec2rad(prop_vec):
    """
    Calculate bearing and verticle angle for given particle motion vector.
    """

    bearing = -np.arctan2(prop_vec[1], prop_vec[0])

    if prop_vec[2] == 0:
        v_angle = 0
    else:
        xy = (prop_vec[0]**2 + prop_vec[1]**2)**0.5
        v_angle = np.arctan2(prop_vec[2], xy)

    return bearing, v_angle

def projectNewAxis(subset, bearing, v_angle, new_chan):
    """
    Reproject xyz channels to create a new axis.

    This to create a new channel with an axis that points along the
    direction of sound propagation (i.e. the mean sound intensity vector).

    Using the identity cos(theta) = (a * b) / ( ||a|| ||b|| ), we can project
    our sample vector 'a' onto our new intensity axis 'b'.

    See https://en.wikipedia.org/wiki/Vector_projection

    Parameters
    ----------
    data_i: numpy.ndarray
        A 2D array holding the sound intensity signals for each channel of a
        vector sensor.
    channels: str
        A string holding the channel labels for data_i rows.
    pitch: float
        Pitch of new channel axis, in radians.
    yaw: float
        Yaw of  new channel axis, in radians.

    Return
    ------
    (data, channels)
        Return the data array and channel string with the newly added
        propagation axis.
    """

    # Get channel indicies
    i_x = subset.channels.find('x')
    i_y = subset.channels.find('y')
    i_z = subset.channels.find('z')

    # ---------------------------------------------------------------------
    # Project values onto propagation axis (project a onto b)
    # divide projected values by axis vector to get values
    # ---------------------------------------------------------------------

    # Flip direction of bearings for equations below
    n_bearing = -bearing

    # Make axis column vector [x, y, z] to project to (unit length)
    if i_z == -1:  # Ignore z channel
        b = np.array([np.cos(n_bearing)*np.cos(v_angle),
                      np.sin(n_bearing)*np.cos(v_angle)],
                     dtype = 'f')
    else:
        b = np.array([np.cos(n_bearing)*np.cos(v_angle),
                      np.sin(n_bearing)*np.cos(v_angle),
                      np.sin(v_angle)],
                     dtype = 'f')

    log.debug("Propagation vector: %s", b)

    # Inner axis is xyz vectors, outer is sample number
    a = np.array([
        subset.data[i_x],
        subset.data[i_y],
        subset.data[i_z]])

    # Calculate projection lengths
    proj = np.apply_along_axis(np.dot, 0, a, b)

    # Add axis to vector data object
    subset.data = np.append(subset.data, np.array([proj]), axis=0)
    subset.channels = subset.channels + new_chan
