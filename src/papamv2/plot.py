"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Default plotting function for the CLI and GUI interfaces
"""

import os
import platform
import types

import PyQt5
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt

import pyqtgraph as pqg

import papamv2
from papamv2.debug import debug

class PamPlot:
    """
    Class for plotting paPAM results.

    If GUI mode is used, plots will be pushed to a Qt window, if in CLI mode,
    no plots will be shown while the analysis is running (plots can still be
    output to file in this mode).
    """

    def __init__(self, hide_plots, ui = None):

        self.ui = ui
        self.hide_plots = hide_plots

    # def plot(self, tab, fig):
    def plot(self, tab, fcn, lst):
        """
        In GUI mode, push figure opbject to Qt PlotWindow subclass for display.
        """
        if self.hide_plots or self.ui is None:
            return

        #fig = fig_build['fun'](*fig_build['args'])

        # self.ui.plotWindow.plot2.emit(tab, fig)
        self.ui.plotWindow.plot.emit(tab, fcn, lst)

class Console():
    """
    This class is a replacement for a file object.  It will redirect
    text to a QTextEdit instead of writing to file.  This is used to write
    console output on a QTextEdit obejct instead of the default system console.

    Use signals to update console, so threading is supported.

    Parameters
    ----------
    text: str
        Console text currently displayed
    te: PyQt5.QtWdigets.QTextEdit
        The text edit object holding the console text to be displayed.
    wc: WriteConsole
        Class used to send Qt Signals for appending console text.


    """

    class WriteConsole(PyQt5.QtCore.QObject):
        """
        This class holds the signal for pasing text to console.  Inherits a
        QObject class, so the function connect() can be applied to it.

        Attributes
        ----------
        write_console: PyQt5.QtCore.pyqtSignal
            Signal object for passing text to paPAM console on seperate UI
            window.
        """

        # Signals must be declared as class (static) variables (as opposed to
        # instance variables)
        write_console = PyQt5.QtCore.pyqtSignal(str)
        # Also, signals must be declared in classes which inherid QObjects,

    def __init__(self, te):
        self.text = ""
        self.te = te
        # wipe text edit contents
        te.setPlainText("")

        # init signal
        self.wc = self.WriteConsole()
        self.wc.write_console.connect(te.setPlainText)


    def write(self, string):
        """
        Append text to console output.  Emits a signal.
        """
        # Append to console text
        self.text = self.text + string
        # Update qtextedit
        # te.setPlainText(self.text)
        self.wc.write_console.emit(self.text)
        self.te.textChanged.emit()
        # Must manually send textChanged signal for some reason :/

        # output to system default stdout also (terminal)
        # This will cause windows pyinstaller no-consolve version to crash
        # sys.__stdout__.write(string)

    def clear(self):
        """
        Clear console text and update.
        """
        # wipe console text
        self.text = ""
        # Update qTextEdit
        self.wc.write_console.emit(self.text)

class PlotWindow(PyQt5.QtWidgets.QWidget):
    """
    Class holding the plotting window.

    This has a custom slot for accepting new data to plot, along with
    a signal to send the data.
    """

    # signals must be defined here (as static class variable)
    # plot = pyqtSignal(str, matplotlib.figure.Figure)
    plot = pyqtSignal(str, types.FunctionType, list)
    clear = pyqtSignal()
    subplot = pyqtSignal(str, types.FunctionType, list, str)


    def __init__(self):
        super().__init__()

        # ------ Define signal for adding new plot
        self.plot.connect(self.plot_window)
        self.clear.connect(self.plot_clear)
        self.subplot.connect(self.plot_subwindow)

        self.subplots = []

        # ------ set window title
        self.setWindowTitle("paPAMv2 Output")

        # ------ Add tab widget
        layout = QtWidgets.QVBoxLayout()
        self.plottabs = QtWidgets.QTabWidget(self)
        self.plottabs.setTabsClosable(True)
        layout.addWidget(self.plottabs)
        self.setLayout(layout)
        # Initate action for tab close button
        #self.plottabs.tabCloseRequested.connect(
        #    lambda idx: self.plottabs.removeTab(idx))
        self.plottabs.tabCloseRequested.connect(self.plottabs.removeTab)

        # ------ Add undock button to tab
        path = os.path.dirname(papamv2.gui.__file__)
        icon_undock  = QtGui.QIcon(os.path.join(path, "icon_undock.png"))
        pb_undock = QtWidgets.QPushButton(self)
        # pb_undock.setStyleSheet("border: none; outline: none")
        pb_undock.setIcon(icon_undock)
        pb_undock.clicked.connect(self.undock)

        self.plottabs.setCornerWidget(pb_undock, Qt.TopRightCorner)

        # ------ Make console output tab
        page = QtWidgets.QWidget()
        page.setObjectName("console")
        page_layout = QtWidgets.QVBoxLayout()
        page.setLayout(page_layout)

        # ------ Add textedit
        self.te = QtWidgets.QTextEdit(page)
        self.te.setReadOnly(True)
        self.te.setMinimumSize(800, 500)
        self.te.textChanged.connect(self.bottom)  # Autoscroll

        # ------ Apply solaried dark terminal colors
        pal = PyQt5.QtGui.QPalette()
        pal.setColor(PyQt5.QtGui.QPalette.Base,
                     PyQt5.QtGui.QColor(0, 43, 54))
        pal.setColor(PyQt5.QtGui.QPalette.Text,
                     PyQt5.QtGui.QColor(131, 148, 150))
        self.te.setPalette(pal)

        # ------ Monospace font for console
        f = self.te.currentFont()
        f.setFamily("Courier New")
        f.setStyleHint(PyQt5.QtGui.QFont.Monospace)
        self.te.setFont(f)

        # ------ Add tab to bar
        page_layout.addWidget(self.te)
        self.plottabs.addTab(page, "Console")
        # ------ Remove close button on tab
        tabbar = self.plottabs.tabBar()
        if platform.system() == "Darwin":
            # OSX has close button on left side
            tabbar.setTabButton(0, QtWidgets.QTabBar.LeftSide, None)
        else:
            # Linux and windows has close button on right side
            tabbar.setTabButton(0, QtWidgets.QTabBar.RightSide, None)

    @pyqtSlot()
    def bottom(self):
        """
        After console tab updates, this function will move scroll bar to
        bottom.  Without this, the scroll bar resets to the top of console
        after every update.
        """
        sb = self.te.verticalScrollBar()
        sb.setValue(sb.maximum())

    @pyqtSlot(str, types.FunctionType, list)
    def plot_window(self, tab, fcn, args):
        """
        Make new plot tab pages for showing data.

        Parameters
        ----------
        ui: QWidget
            The main window of gpaPAMv2.  Contains the ploting window.
        tab: str
            Name of the plotwindow tab to draw in.
        fig: matplotlib.figure
            Figure to plot in tab page.
        """

        debug(f"function: {fcn}")

        # ------ Check if tab already exists, if so, remove it.
        page = self.plottabs.findChild(QtWidgets.QWidget, tab)

        if page is not None:
            idx = self.plottabs.indexOf(page)
            self.plottabs.removeTab(idx)

        # ------ Make new tab
        page = QtWidgets.QWidget()
        page.setObjectName(tab)
        page_layout = QtWidgets.QVBoxLayout()
        page.setLayout(page_layout)

        pw = fcn(*args)

        # ------ Add canvas to new tab
        page_layout.addWidget(pw)
        self.plottabs.addTab(page, tab)

    @pyqtSlot()
    def plot_clear(self):
        """
        Clear all tabs (except console)
        """

        tab_n = self.plottabs.count()

        while tab_n > 1:
            self.plottabs.removeTab(tab_n - 1)
            tab_n = self.plottabs.count()

    @pyqtSlot(str, types.FunctionType, list, str)
    def plot_subwindow(self, title, fcn, args, meta):
        """
        Clear all tabs (except console)
        """

        # ------ make plotting object
        pw = fcn(*args)

        # ------ Show in new plotting window
        subplot = PlotSubWindow(self, title, pw, meta)
        self.subplots.append(subplot)
        subplot.show()

    def undock(self):
        """
        Undock current figure into indepedent window.
        """

        self.clean_subplots()

        # Delete closed windows
        debug(f"self.subplots: {self.subplots}")

        # ------ get plotting object from current tab
        tab = self.plottabs.currentWidget()
        children = tab.findChildren((pqg.GraphicsLayoutWidget, pqg.PlotWidget))
        debug(f"children: {children}")

        if len(children) == 0:
            return

        pw = children[0]

        # ------ Show in new plotting window
        title = self.plottabs.tabText(self.plottabs.currentIndex())
        subplot = PlotSubWindow(self, title, pw)
        self.subplots.append(subplot)
        debug(f"self.subplots, undock: {self.subplots}")
        subplot.show()

        # # ------ Remove tab
        self.plottabs.removeTab(self.plottabs.currentIndex())


    def dock(self, tab, pw):
        """
        Dock a PlotWidget back into tab bar
        """

        # ------ Make new tab
        page = QtWidgets.QWidget()
        page.setObjectName(tab)
        page_layout = QtWidgets.QVBoxLayout()
        page.setLayout(page_layout)

        # ------ Add canvas to new tab
        page_layout.addWidget(pw)
        self.plottabs.addTab(page, tab)

        self.clean_subplots()

    def clean_subplots(self):
        """
        Remove window items from memory which have been closed.
        """
        # windows will not be deleted until their python references are removed
        debug(f"self.subplots, before: {self.subplots}")

        closed_bool = list(map(lambda x: x.isHidden(), self.subplots))
        debug(f"closed_bool: {self.subplots}")

        closed_idx = [i for i, x in enumerate(closed_bool) if x]

        if len(closed_idx) > 0:
            for i in sorted(closed_idx, reverse = True):
                del self.subplots[i]

        debug(f"self.subplots, after: {self.subplots}")

class PlotSubWindow(PyQt5.QtWidgets.QWidget):
    """
    Class holding the undocked sub plotting window.

    This class allows multiple plots to be undocked as standalone windows.
    """

    def __init__(self, plotwindow, title, pw, meta = None):
        super().__init__()

        self.title = title
        self.pw = pw
        self.plotwindow = plotwindow

        # ------ Remove closed subplot windows
        plotwindow.clean_subplots()

        # ------ set window title
        self.setWindowTitle("paPAMv2 plot")

        # ------ Add elements
        layout = QtWidgets.QVBoxLayout(self)

        # --- title and dock button
        layout_horiz = QtWidgets.QHBoxLayout()
        layout.addLayout(layout_horiz)
        # dock button
        path = os.path.dirname(papamv2.gui.__file__)
        icon_dock  = QtGui.QIcon(os.path.join(path, "icon_dock.png"))
        pb_dock = QtWidgets.QPushButton()
        pb_dock.setIcon(icon_dock)
        pb_dock.setSizePolicy(QtWidgets.QSizePolicy.Maximum,
                              QtWidgets.QSizePolicy.Maximum)
        pb_dock.clicked.connect(self.dock)
        # title
        if meta is None:
            ql_title = QtWidgets.QLabel(title)
        else:
            ql_title = QtWidgets.QLabel(title + ", " + meta)
        ql_title.setSizePolicy(QtWidgets.QSizePolicy.Maximum,
                               QtWidgets.QSizePolicy.Maximum)
        # spacer
        h_spacer = QtWidgets.QSpacerItem(
            20, 40,
            QtWidgets.QSizePolicy.Expanding,
            QtWidgets.QSizePolicy.Maximum)
        # add to layout
        layout_horiz.addWidget(pb_dock)
        layout_horiz.addWidget(ql_title)
        layout_horiz.addItem(h_spacer)

        # Add canvas to new tab
        layout.addWidget(pw)
        self.setLayout(layout)

        # ------ Set flag to delete object on close
        # make sure that memory is not eaten up by invisible background
        # windows
        # self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

    def dock(self):

        self.plotwindow.dock(self.title, self.pw)
        self.close()
