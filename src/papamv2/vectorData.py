"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

VectorData and VectorSubset Classes
------------
This file holds functions associated with storing and manipulating data from a
vector sensor.

A VectorData object holds references to the wavfiles and metadata
(channel units), calibration data, desired bandpass frequency.

A VectorSubset is the relized extration of this data.  Upon initalization,
the selected raw data is copied into memory, bandpass and calibration
operations are applied, and a numpy array is returned which can further be used
for analysis.  This class also holds some functions for streamlining analysis
proceedures across multiple channels.

"""

# PIP modules
import re
import numpy as np
import scipy.io.wavfile as wav
from scipy import signal
# paPAM modules
from papamv2.calibration import calibration as cal

from papamv2.debug import debug

class VectorData:
    """
    This class is for reading and analyzing vector sensor data.

    Channels can be indexed by their character names: {'p', 'x', 'y', 'z'} for
    pressure, and x, y, and z particle motion axes, respectively.

    Upon initialization, memory maps are created for the wav file (i.e. files
    are read directly from disk, not loaded into memory).  When part of the
    signal is requested with the subset() function, the raw clip will be copied
    into memory, bandpass filtered, and then calibrated before being returned.
    Hence, calibration and bandpass proceedures can be changed at any time
    without needing to create a new vectorData object.  By accessing the wav
    data as a memmpry map, this will greatly reduce the memmory cost in cases
    where the user only wishes to analyze a small segment of the file.

    Attributes
    ----------
    fs: int
        Sample rate.
    channels: str
        Characters from the set {'x', 'y', 'z', 'p', 'i'}
    n: int
        Numer of samples in each channel.
    data: [ numpy.ndarray(shape=(samples), ... ]
        List holding a numpy.memmap array for signals from each channel.
    dtype: str
        Number format of memmapped data.
    calib_mag: {'x': numpy.ndarray, 'y': numpy.ndarray, 'z': numpy.ndarray,
    'p': numpy.ndarray}
        Dict holding the impulse reponse for the FIR calibration filters
        for the frequency response magnitude of each channel.  These filters
        account for particle motion unit conversions.  These should be applied
        with filtfilt.
    calib_phase: {'x': numpy.ndarray, 'y': numpy.ndarray, 'z': numpy.ndarray,
    'p': numpy.ndarray}
        for the phase response phase of each channel.  These filters
        account for particle motion unit conversions.  These should be applied
        with lfilter.
    band:  {'highpass': float , 'lowpass': float, 'order': int ]
        Stored parameters for bandpass filter.
    unit_out: str
        Character indicating which units to output particle motion channels.
        Can be 'd', 'v', 'a', or 'e'.
    Methods
    -------
    subset(rng = range):
        Return a calibrated and bandpassed copy of the signal.  Analysis can
        be run on these subsets.
    setCalibration(magnitude, phase):
        Stores the calibration data.
    setBandpass(low, high, order):
        Store parameters for butterworth filter.
    """

    def __init__(self, wav_files, channels, rho, c):
        """
        Init object by reading input wav files.

        Parameters
        ----------
        wav_files: [str, str, ...]
            List of filenames for the input wav files.
        channels: str
            String holding the vector channel ids: 'p', 'x', 'y', and 'z'.
        """

        self.channels = channels
        self.band = {'highpass': 0, 'lowpass': 0, 'order': 0}
        self.calib = None
        self.unit_in = None # particle motion calibration units
        self.units_out = None
        self.rho = rho
        self.c = c
        self.sos = None

        # ---- Load wavs
        self.data = []
        if len(wav_files) == 1:

            # Read file
            self.fs, tmp = wav.read(wav_files[0], mmap = True)

            # multi channel files are stored as 2d arrays
            # We will convert these to a list of 1d memmap arrays
            if len(tmp.shape) > 1:
                # Transpose data for easier handling
                tmp = tmp.T
                self.n = tmp.shape[1]

                # Copy 2D memmap to list of 1d views
                for i in range(0, tmp.shape[0]):
                    self.data.append(tmp[i])
            else:
                self.data = [ tmp ]
                self.n = tmp.size

            #print("self.data: " + str(self.data))

        else:
            # Loop and import first channel of each wav file
            n = None
            dtype = None
            fs = None
            for i, wav_file in enumerate(wav_files):

                # Load wav file
                fs_tmp, tmp = wav.read(wav_file, mmap = True)
                tmp = tmp.T
                #print(tmp)
                #print(tmp.shape)
                # Check file is valid
                if len(tmp.shape) > 1:
                    raise ValueError(wav_files[i] +
                                     " has multiple channels. " +
                                     "Single channel wav files must be used " +
                                     "when using multiple files as input.")
                if len(tmp) != n and n is not None:
                    raise ValueError("channel " + str(i + 1) +
                                     " does not have the same number of " +
                                     "samples as channel 1")
                if tmp.dtype != dtype and dtype is not None:
                    raise ValueError("channel " + str(i + 1) +
                                     " does not have the same bit depth as " +
                                     "channel 1")
                if fs_tmp != fs and fs is not None:
                    raise ValueError("channel " + str(i + 1) +
                                     " does not have the same sample rate" +
                                     " as channel 1")

                # Save number of samples
                n = len(tmp)
                fs = fs_tmp

                # Save channel data
                self.data.append(tmp)

            # Save metadata
            self.fs = fs
            self.n = n
            self.dtype = dtype


    def setBandpass(self, highpass, lowpass, order):
        """
        Set parameters for bandpass filter.

        To disable the bandpass, set the order to 0.

        Parameters
        ----------
        highpass: float
            High pass frequency.
        lowpass: float
            Low pass frequency.
        order: int
            Order of filter.  The resulting rolloff will be (6*order + 6) dB
            per octave with a flat passband.  The extra (+ 6dB) is for the
            double application of the filter (i.e. filtfilt).
        """

        self.band = {
                'highpass': highpass,
                'lowpass': lowpass,
                'order': order}

        # Check errors
        if lowpass < highpass and lowpass is not None and highpass is not None:
            ValueError(f"Butterworth filter error: lowpass value ({lowpass})" +
                       f" must be  larger than highpass ({highpass}).")

        if order < 1:
            self.sos = None
            return

        # Calculate SOS values
        if highpass is None:
            if lowpass > self.fs:
                ValueError("Butterworth filter error: lowpass value" +
                           f" ({lowpass}) cannot exceed nyquest ({self.fs/2})")
            self.sos = signal.butter(
                    self.band['order'],  # order
                    lowpass, # Wn
                    btype = 'lowpass',
                    fs = self.fs,
                    output = 'sos',
                    analog = False)
        elif lowpass is None:
            if highpass > self.fs:
                ValueError("Butterworth filter error: highpass value" +
                           f" ({highpass}) cannot exceed nyquest ({self.fs/2})")
            self.sos = signal.butter(
                    self.band['order'],  # order
                    highpass, # Wn
                    btype = 'highpass',
                    fs = self.fs,
                    output = 'sos',
                    analog = False)
        else:
            self.sos = signal.butter(
                    self.band['order'],  # order
                    (highpass, lowpass), # Wn
                    btype = 'band',
                    fs = self.fs,
                    output = 'sos',
                    analog = False)


    def setCalibration(self, file_mag, file_phase, recorder_gain, unit_in, units_out):
        """
        Create FIR filters for calibration.

        Calibration filters will only be applied when subsets are requested.

        Parameters
        ----------
        calib_file: str
            Path to csv file holding calibration data.  This is expected
            to be a 8 column file holding paired frequency (Hz) and
            receiver sensitivity (dB re (1V/uPa or 1V/nms^{-1})) for
            channels in the order of x, y, z, pressure.s
        recorder_gain: (float, float, float, float)
            Gain values in dB for each channel (x, y, z, p).
        unit_in: str
            Single character indicating the units of the particle motion
            receiver sensitivity values.  'a' or 'v' for dB re 1um/s^{-2} or
            re 1nm/s^{-1}, respectively.
        unit_out: str
            Single character indicating the output units of the particle motion
            calibration.  May be 'a', 'v', or 'd' for dB re 1um/s^{-2},
            re 1nm/s^{-1}, or re 1pm/s respectively.
        """


        # Save units out
        self.units_out = units_out

        self.calib = cal.Calibration(self.fs)

        # check that 'v' (velocity) is present for 'e' (expected paticle motion)
        if 'e' in  units_out and units_out.find('v') == -1:
            # Add velocity units
            units_out = units_out + 'v'

        # Loop and make calibration impulse for each unit out
        for unit_out in units_out:

            if unit_out == 'p':
                chans = 'p'
            else:
                chans = 'xyz'

            # ------ Read calibration data from file
            self.calib.read(file_mag, file_phase, recorder_gain, unit_in)

            # ------ Create magnitude calibration impulse response
            self.calib.createCalibrationImpulses(unit_out)

    def subset(self, unit_out, rng):
        """
        Extract a subset of the signal and hold in memory.

        Analysis proceedures are applied to datasubsets, and the creating of a
        signal subset will automatically apply the set calibration and bandpass
        parameters.

        Parameters
        ----------
        rng: range
            The range of sample to subset.  When None, whole range will be
            used.
        unit_out: str
            Single character indicating what units to extract from vector data.
            Must be one of {'p', 'd', 'v', 'a'}.
        intensity_axis: (float, float)
            An optional tuple holding the pitch and yaw of the intensity axis.
        rho: float
            The specific density of the medium (kg/m^3).  This is required for
            calculating the expected particle velocity from sound pressure
            values.
        c: float
            The sound speed of the medium.  Required for calculating the
            expected particl;e velocity form sound pressure values.


        Return
        ------
        A VectorSubset object.
        """

        #print(self.n)
        if rng is None:
            rng = range(0, self.n)

        if self.calib is None:
            raise RuntimeError("No calibration has been set yet.")

        sub = VectorSubset(self, unit_out, rng)

        return sub

def _fill_buffer(buffer, sig, rng, pad_len):
    """
    Fill a padded buffer with a signal subset (signal[rng]).

    For padding values that dont have real signal values, we use the `odd`
    method from scipy.signal.  This just uses a 180 flip of the nearest real
    signal data.
    """

    # fill signal contents
    buffer[pad_len:-pad_len] = sig[rng]

    # ------ Add padding before
    # samples to add from odd (180 rotation of data)
    n_sig = min(rng.start, pad_len)
    # samples to take from signal
    n_odd = pad_len - n_sig
    debug(f"Buffer prepadding")
    debug(f"n_sig: {n_sig}")
    debug(f"n_odd: {n_odd}")
    debug(f"pad_len: {pad_len}")
    if n_odd == pad_len:
        # Use odd as padding
        tmp = sig[rng.start:(rng.start + n_odd)]
        buffer[0:pad_len] = - np.flip(tmp)
    elif n_sig == pad_len:
        # use signal as padding
        buffer[0:pad_len] = sig[(rng.start - n_sig):rng.start]
    else:
        # Use combination of signal and odd
        # signal
        buffer[n_odd:pad_len] = sig[(rng.start - n_sig):rng.start]
        # odd
        idx = (rng.start - n_sig)
        tmp = sig[idx:rng.start]
        buffer[0:n_odd] = - np.flip(tmp)

    # ------ Add padding after
    # samples to add from odd (180 rotation of data)
    n_sig = min(len(sig) - rng.stop, pad_len)
    n_odd= pad_len - n_sig
    debug(f"Buffer postpadding")
    debug(f"n_sig: {n_sig}")
    debug(f"n_odd: {n_odd}")
    debug(f"pad_len: {pad_len}")
    # samples to take from signal
    if n_odd == pad_len:
        # Use odd as padding
        tmp = sig[(rng.stop - n_odd):rng.stop]
        buffer[-pad_len:] = - np.flip(tmp)
    elif n_sig == pad_len:
        # Use signal as padding
        buffer[-pad_len:] = sig[rng.stop:(rng.stop + n_sig)]
    else:
        # Use combination of signal and odd
        # signal
        buffer[-pad_len:-n_odd] = sig[rng.stop:(rng.stop + n_sig)]
        # odd
        idx = (rng.end + n_odd)
        tmp = sig[rng.end:idx]
        buffer[-n_odd:] = - np.flip(tmp)

def _getBufferMargins(sos, vectorData, channels, unit_calib):
    """
    Calculate the minimim size for padding length to use in filters
    """
    # ------ Calculate Margins for filters
    pad_len = 0
    # butterworth margin
    if sos is not None:
        pad_len = 3 * (2 * len(sos) + 1 - \
                       min((sos[:, 2] == 0).sum(),
                           (sos[:, 5] == 0).sum()))
    # calibration margins
    taps = vectorData.calib.taps
    if taps is not None:
        pad_len = max(pad_len, 3* taps)

    return pad_len

class VectorSubset:
    """
    A subset of the VectorData which is copied into memory and has the bandpass
    and calibration proceedures applied to it.

    Subsets will first be extracted with extra margin samples surrounding the
    specified range.  After the calibration is applied, these margins will be
    trimmed.  This ensures that samples at the start of a subset will be
    calibrated to the same values as samples in the middle.  Functionally,
    this is similar to 0 padding in typical filter processes.

    Attributes
    ----------
    fs: int
        Sample rate.
    channels: str
        Characters from the set {'x', 'y', 'z', 'p', 'i'}.  'i' indicates the
        particle motion channel pointing along the axis of the mean sound
        intensity vector.
    nchannels: int
        Number of channels in wav file.
    unit_in: str
        Character 'a', 'v', 'd', or None indicating if acceleration or vecolity
        or displacement is recorded.  None will be set if no particle
        motion channels are used.
    n: int
        Numer of samples in each channel for extracted clip.
    nparent: int
        Numer of samples in each channel for complete parent wavfile.
    band:  [ double highpass, double lowpass, int order ]
        Applied parameters for bandpass filter.
    data: numpy.array(shape=(channels, n))
        A numpy array holding the subsetted data for each channel in memory.
        These vectors are calibrated and bandpassed.
    unit_out: char
        Unit of the subsetted data.  One of the set {'p', 'd', 'v', 'a', 'i'}.
        Channels which do not hold signals in this unit will be removed from
        the subset.
    rng: range
        The sample range of the subset (with respect to the parent signal)

    Methods
    -------
    map(fcn, sum=False):
        Apply fcn to each channel of vector data.  Return a dictonary of the
        results.  if sum=True, the vector sum of particle motion channels will
        also be returned.
    apply(fcn, args, args, kargs):
        Apply the function fct to each channel of vector data.  args and kargs
        will supply the positional and keyword arguments, respectively.

    """

    def __init__(self, vectorData, unit_out, rng):
        """
        vectorData: VectorData
            Parent object holding metadata about signal to extract.
        rng: range
            Range object indicating range of samples to extract from wav files.
        """
        # ------ Copy parameters from VectorData
        self.fs = vectorData.fs
        self.channels = vectorData.channels
        self.unit_in = vectorData.unit_in
        self.n = len(rng)
        self.nparent = vectorData.n
        self.band = vectorData.band
        self.nchannels = len(vectorData.data)
        self.unit_out = unit_out
        self.rng = rng
        self.rho = vectorData.rho
        self.c = vectorData.c
        self.sos = vectorData.sos

        # Expected particle motion is calulated from pressure channel
        if self.unit_out == 'e':
            unit_calib = 'p'
        else:
            unit_calib = self.unit_out

        # Possible wav data formats
        formats = {'float32': (1, 0),
                   'int32': ((2**31), 0),
                   'int16': ((2**15), 0),
                   'uint8': ((2**8), 8*16)}
        dstr = str(vectorData.data[0].dtype) # Realized format

        # ------ Remove unnecessary channels
        # Use regex patterns to filter out unnecessary channels
        if self.unit_out == 'p':
            pat = 'p'
        elif self.unit_out in 'dva':
            pat = '[xyz]'
        elif self.unit_out == 'e':
            # Expected particle velocity calculated from pressure
            pat = 'p'
        # Index of channels to keep
        regex = re.compile(pat)
        match = regex.finditer(self.channels)
        chan_idx = tuple(map(lambda x: x.span()[0], list(match)))
        # Subset channels
        self.channels = ''.join(map(lambda x: self.channels[x], list(chan_idx)))
        self.nchannels = len(chan_idx)

        # Calculate padding for filters
        pad_len = _getBufferMargins(self.sos, vectorData, self.channels,
                                    unit_calib)
        debug(f"pad_len: {pad_len}")
        debug(f"rng: {rng}")

        # ------ Init numpy array for holding extracted wav data
        self.data = np.ndarray(
                shape=(self.nchannels, self.n + (2*pad_len)),
                dtype='float64')
        # Use 64-bit to avoid overflow errors on large units
        debug(f"self.data.shape: {self.data.shape}")

        # ------ Extract data from memmap
        for i in range(0, self.nchannels):
            debug(f"channel: {i}")

            buffer = self.data[i, :]
            sig = vectorData.data[chan_idx[i]]
            debug(f"sig.shape: {sig.shape}")
            debug(f"buffer.shape: {buffer.shape}")
            debug(f"buffer.dtype: {buffer.dtype}")
            _fill_buffer(buffer, sig, rng, pad_len)

            # Normalize to (-1, 1) scale
            buffer = buffer - formats[dstr][1]
            buffer = buffer / formats[dstr][0]

            chan = self.channels[i]

            # ------ Apply Magnitude Calibration
            debug("Apply magnitude calibration...")
            mag_taps = vectorData.calib.impulse_magnitude[chan][unit_calib]
            if mag_taps is not None:
                debug(f"mag_taps.shape: {mag_taps.shape}")
                debug(f"mag_taps.dtype: {mag_taps.dtype}")
                buffer = signal.filtfilt(mag_taps, 1.0, buffer, padtype = None)

            # ------ Apply Phase Calibration
            phase_taps = vectorData.calib.impulse_phase[chan][unit_calib]
            if phase_taps is not None:
                debug("Apply phase calibration...")
                debug(f"phase_tags.shape: {phase_taps.shape}")
                debug(f"phase_tags.dtype: {phase_taps.dtype}")
                buffer = signal.lfilter(phase_taps, 1.0, buffer)
                # Adjust for group delay
                n_shift = int(np.floor(phase_taps.size/2))
                buffer = np.concatenate(
                    (buffer[n_shift:], np.zeros(n_shift)),
                    dtype = buffer.dtype)

            # ------ Apply magnitude constant scaling (recorder gain)
            debug("Apply magnitude constant (recorder gain)...")
            buffer = buffer * vectorData.calib.scale_magnitude[chan]

            # Return channel into data container
            debug("Export buffer to container...")
            self.data[i, :] = buffer

        # ------ Apply bandpass
        if self.sos is not None:
            # Apply to all channels at once
            self.data = signal.sosfiltfilt(self.sos, self.data, axis=-1,
                                           padtype = None)

        # ------ Trim margins
        self.data = self.data[:, pad_len:-pad_len].copy()
        # copy here to make sure origional padded array is garbage collected

        # ------ Calculate expected particle motion
        if self.unit_out == 'e':
            # Use longitudinal wave equation to convert uPa into nm/s units.
            self.data[:, :] = (self.data[:, :] * 1e3) / (vectorData.rho * vectorData.c)

    def map(self, fcn, sum_agg=False, sum_samples=False, x=None, trns = None, rast = False):
        """
        Calculate aggregate values from channels and return results in a
        dictionary with keys indicating the channel.

        When appending results to an existing dictionary, only apply function
        to channels which have no existing keys in the dictionary.

        ------
        A dictionary holding then result for each channel or channel sum.
        """

        # init return dict
        ret = dict()

        # Set x value
        if x is None:
            x = self.data

        # ===================== Loop sensor channels ==========================
        for idx in range(0, x.shape[0]):

            # ------ Run function on channel
            chan = self.channels[idx]
            ret[chan] = fcn(x[idx])

        if sum_agg:
            # ====================== Vector sum results =======================
            # for results which are 2D arrays, only sum the rightmost column
            if 'x' in ret.keys() and 'y' in ret.keys():

                if len(ret['x'].shape) == 2 and not rast:
                    n_col = ret['x'].shape[1]
                    col = n_col - 1
                    col_idx = list(range(0,col))
                    # ------ vector sum horizontal axes
                    xy = (ret['x'][:,[col]]**2 + ret['y'][:,[col]]**2)**0.5
                    ret['xy'] = np.hstack((ret['x'][:,col_idx], xy))
                else:
                    # ------ vector sum horizontal axes
                    xy = (ret['x']**2 + ret['y']**2)**0.5
                    ret['xy'] = xy

            if 'x' in ret.keys() and 'y' in ret.keys() and 'z' in ret.keys():

                # ------ vector sum all axes
                if len(ret['x'].shape) == 2 and not rast:
                    n_col = ret['x'].shape[1]
                    col = n_col - 1
                    col_idx = list(range(0,col))
                    # ------ vector sum horizontal axes
                    xyz = (ret['x'][:,[col]]**2 + ret['y'][:,[col]]**2 + ret['z'][:,[col]]**2)**0.5
                    ret['xyz'] = np.hstack((ret['x'][:,col_idx], xyz))
                else:
                    xyz = (ret['x']**2 + ret['y']**2 + ret['z']**2)**0.5
                    ret['xyz'] = xyz

        elif sum_samples:
            # =============== Vector sum data then calc results ===============
            if 'x' in self.channels and 'y' in self.channels:

                # ------ vector sum horizontal axes
                i_x = self.channels.find('x')
                i_y = self.channels.find('y')
                vec = (x[i_x]**2 + x[i_y]**2)**0.5

                # ------ calc result
                ret['xy'] = fcn(vec)

            if 'x' in self.channels and 'y' in self.channels and \
                    'z' in self.channels:

                # ------ vector sum horizontal axes
                i_x = self.channels.find('x')
                i_y = self.channels.find('y')
                i_z = self.channels.find('z')
                vec = (x[i_x]**2 + x[i_y]**2 + x[i_z]**2)**0.5

                # ------ calc result
                ret['xyz'] = fcn(vec)

        if trns is not None:
            # Transform output data
            for k, v in ret.items():
                if len(v.shape) == 2 and not rast:
                    col = (v.shape[1]-1)
                    v[:, col] = trns(v[:,col])
                    ret[k] = v
                else:
                    ret[k] = trns(v)

        return ret


    def __getitem__(self, i):
        """
        Return a view of the array data for the given channel.

        Parameters
        ----------
        i: str
            Single character indicating which channel (p, x, y, or z) to
            extract.
        """
        if type(i).__name__ != "str":
            raise ValueError("index must be a single character")
        if len(i) > 1:
            raise ValueError("index must be a single character")
        if 'pxyz'.find(i) == -1:
            raise ValueError("index must be a character from the set" +
                             " {'p', 'x', 'y', 'z'}.")

        idx = self.channels.find(i)

        return self.data[idx]
