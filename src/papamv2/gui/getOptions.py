"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions for scraping parameter input from the GUI.
"""

import os
import glob

from scipy.io import wavfile

from papamv2 import optionParsing as parsing

from papamv2.debug import debug

def get_options(ui):
    """
    Grab options from GUI and pass into argument parser.

    The resulting output arguments must be in CLI format.
    """
    # ======================== Get values from GUI ============================
    # init empty dict
    options = dict()

    # ------------------------ Analysis type ----------------------------------
    if ui.rb_subsets.isChecked():
        options["subsets"] = ui.le_ranges.text().replace(" ", "")
    elif ui.rb_sequentialSubsets.isChecked():
        options['sequential_subsets'] = \
                ui.le_subsetDuration.text().replace(' ', '')

    # ------------------------- Analysis Features -----------------------------
    # Waveform
    options["waveform"] = ui.cb_waveform.isChecked()
    # Analytic envelope
    options["analytic_envelope"] = ui.cb_analyticEnvelope.isChecked()
    # Time Exceeding Level
    if ui.cb_timeExceedingThreshold.isChecked():
        options["time_exceeding_threshold"] = ",".join([
            ui.le_timeExceedingThreshold_binSize.text(),
            ui.le_timeExceedingThreshold_overlap.text(),
            ui.le_timeExceedingThreshold_threshold.text()])
    # Binned Analysis
    if ui.cb_binnedAnalysis_kurtosis.isChecked():
        options["binned_kurtosis"] =  True
    if ui.cb_binnedAnalysis_rootMeanSquare.isChecked():
        options["binned_rootMeanSquare"] =  True
    if ui.cb_binnedAnalysis_zeroToPeak.isChecked():
        options["binned_zeroToPeak"] =  True
    if ui.cb_binnedAnalysis_crestFactor.isChecked():
        options["binned_crestFactor"] =  True
    if ui.cb_binnedAnalysis_decidecadeRms.isChecked():
        options["binned_decidecadeRms"] =  True
    if ui.cb_binnedAnalysis_particleMotionVector.isChecked():
        options["binned_particleMotionVector"] =  True
    if ui.cb_binnedAnalysis_soundIntensity.isChecked():
        options["binned_soundIntensity"] =  True

    if ui.cb_binnedAnalysis.isChecked():
        options["binned_analysis"] = ",".join([
            ui.le_binnedAnalysis_binSize.text(),
            ui.le_binnedAnalysis_overlap.text()])

    # ------ Time domain --- Continuous analysis
    options["rms"] = ui.cb_rms.isChecked()
    options["kurtosis"] = ui.cb_kurtosis.isChecked()
    # ------ Time Domain --- Transient Analysis
    options["impulse_kurtosis"] = ui.cb_impulseKurtosis.isChecked()
    impulseRMS = ",".join(
        [ui.le_impulseRMS_lower.text(),ui.le_impulseRMS_upper.text()])
    if ui.cb_impulseRMS.isChecked(): options["impulse_rms"] = impulseRMS
    options["zero_to_peak"] = ui.cb_zeroToPeak.isChecked()
    options["exposure"] = ui.cb_exposure.isChecked()
    options["crest_factor"] = ui.cb_crestFactor.isChecked()
    rise_time = ",".join(
        [ui.le_riseTime_lower.text(), ui.le_riseTime_upper.text()])
    if ui.cb_riseTime.isChecked(): options["rise_time"] = rise_time
    pulse_length = ",".join(
        [ui.le_pulseLength_lower.text(), ui.le_pulseLength_upper.text()])
    if ui.cb_pulseLength.isChecked(): options["pulse_length"] = pulse_length

    # ------ Frequency Domain
    spectrogram = ",".join([
        ui.le_spectrogram_windowLength.text(),
        ui.cb_spectrogram_windowType.currentText(),
        ui.le_spectrogram_overlap.text()])
    if ui.cb_spectrogram.isChecked(): options["spectrogram"] = spectrogram
    # Directogram
    directogram_units = ""
    if ui.cb_directogram_soundIntensity.isChecked():
        directogram_units += "i"
    if ui.cb_directogram_particleVelocity.isChecked():
        directogram_units += "v"
    if ui.cb_directogram_particleAcceleration.isChecked():
        directogram_units += "a"
    directogram = ",".join([
        ui.le_directogram_windowLength.text(),
        ui.cb_directogram_windowType.currentText(),
        ui.le_directogram_overlap.text(),
        directogram_units])
    if ui.cb_directogram.isChecked():
        options["directional_spectrogram"] = directogram
    spectralProbabilityDensity = ",".join([
        ui.le_spectralProbabilityDensity_windowLength.text(),
        ui.le_spectralProbabilityDensity_overlap.text(),
        ui.cb_spectralProbabilityDensity_windowType.currentText(),
        ui.le_spectralProbabilityDensity_psdBinSize.text(),
        ui.le_spectralProbabilityDensity_percentiles.text().replace(",", ";")])
    if ui.cb_spectralProbabilityDensity.isChecked():
        options["spectral_probability_density"] = spectralProbabilityDensity
    # ------ Frequency Domain --- Continuous
    psd = ",".join([
        ui.le_psd_windowLength.text(),
        ui.cb_psd_windowType.currentText(),
        ui.le_psd_overlap.text()])
    if ui.cb_psd.isChecked(): options["psd"] = psd
    options['decidecade_rms'] = ui.cb_decidecadeRms.isChecked()
    # ------ Frequency Domain --- Transient
    esd = ",".join([
        ui.le_esd_windowLength.text(),
        ui.cb_esd_windowType.currentText(),
        ui.le_esd_overlap.text()])
    if ui.cb_esd.isChecked(): options["esd"] = esd
    options['decidecade_exposure'] = ui.cb_decidecadeExposure.isChecked()
    # ---------------------- Input/Output -------------------------------------
    # ------ wav and channel info
    # Init chan and file objects to populate
    channels = ''
    wav_files = []
    if ui.rb_fileType_singleChannelFiles.isChecked():
        # ------ Single chan files
        def parse(x, chan):
            # Ignore empty fields
            if x != '':
                wav_files.append(x)
                nonlocal channels
                channels = channels + chan
        parse(ui.le_particleMotionX.text(), 'x')
        parse(ui.le_particleMotionY.text(), 'y')
        parse(ui.le_particleMotionZ.text(), 'z')
        parse(ui.le_particleMotionOmni.text(), 'p')
    else:
        # ------ Multi chan files
        wav_files = [ui.le_file.text()]

        fl = glob.glob(wav_files[0])
        if len(fl) ==  0:
            raise ValueError('No wavefile provided.')
        fl = fl[0]
        if not os.path.isfile(fl):
            raise ValueError('Invalid or no wavefile provided.')
        # Read number of channels from file
        wav = wavfile.read(fl, mmap = True)
        if len(wav[1].shape) == 1:
            raise RuntimeError("Only 1 channel detected.  Not a multi-channel file.")
        chans = wav[1].shape[1]
        del wav
        # Set channels
        if ui.rb_pressureChannel_firstChannel.isChecked():
            channels = 'p' + 'xyz'[0:(chans-1)]
        elif ui.rb_pressureChannel_lastChannel.isChecked():
            channels = 'xyz'[0:(chans-1)] + 'p'
        else:
            channels = 'xyz'[0:(chans)]
    options["channels"] = channels
    options["wav_files"] = wav_files
    # ------ Hide/show plots
    options["hide_plots"] = ui.cb_dontShowFigures.isChecked()
    # ------ Output directory
    if ui.le_outputDirectory != '':
        options["output_dir"] = ui.le_outputDirectory.text()
    if ui.le_outputName.text() != '':
        options["output_name"] = ui.le_outputName.text()
    # ------ Figure size
    options["fig_size"] = ",".join([
        ui.le_output_width.text(),
        ui.le_output_height.text(),
        ui.le_output_dpi.text()])

    # --------------------------- Calibration ---------------------------------
    # ------ calibration file units
    if ui.rb_particleMotionReceiverSensitivityUnits_velocity.isChecked():
        pm_calibration = 'v'
    else:
        pm_calibration = 'a'
    options["calibration_file_unit"] = pm_calibration
    # ------ Recorder gain
    recorder_gain = ",".join([
        ui.le_recorderX.text(),
        ui.le_recorderY.text(),
        ui.le_recorderZ.text(),
        ui.le_recorderOmni.text()])
    options["recorder_gain"] = recorder_gain

    # ------ Get calibration files
    if ui.le_calibration_magnitude.text() != '' and \
            ui.rb_receiverSensitivityAndRecorderGain.isChecked():
        options["calibration_magnitude"] = ui.le_calibration_magnitude.text()

    if ui.le_calibration_phase.text() != '' and \
            ui.rb_receiverSensitivityAndRecorderGain.isChecked():
        options["calibration_phase"] = ui.le_calibration_phase.text()

    # ------------------------ General Settings -------------------------------
    # ------ Bandpass filter
    bandpass = ",".join([
        ui.le_bandpass_high.text(),
        ui.le_bandpass_low.text(),
        ui.le_bandpass_order.text()])
    if ui.gb_bandpass.isChecked():
        options["bandpass"] = bandpass
    # ------ Soundfield components for analysis
    soundfield_components = "".join([
        ui.rb_soundfieldComponent_pressure.isChecked()*'p',
        ui.rb_soundfieldComponent_displacement.isChecked()*'d',
        ui.rb_soundfieldComponent_velocity.isChecked()*'v',
        ui.rb_soundfieldComponent_acceleration.isChecked()*'a',
        ui.rb_soundfieldComponent_estimatedVel.isChecked()*'e'])
    options["units_out"] = soundfield_components
    # ------ Propagation Axis
    options["si_axis"] = ui.cb_addSoundIntensityAxis.isChecked()
    options["pm_axis"] = ui.cb_addParticleMotionAxis.isChecked()
    # ------ Far field velocity predicitons
    options["rho"] = ui.le_rho.text()
    options["c"] = ui.le_c.text()

    # ============================= Parse values ==============================
    return parsing.parse(options)


def batch_glob(paths):
    """
    Parse filenames in a glob
    """
    # Paths is a list of file paths grabbed from UI

    # mark non-empty paths from textedits
    chk = list(map(lambda x: x != '', paths))
    chk_idx = list(filter(lambda x: chk[x], range(0,len(chk))))
    paths_filt = [paths[i] for i in chk_idx]

    #print(chk)
    #print(chk_idx)
    #print(paths_filt)

    globs = [ sorted(glob.glob(paths_filt[i], recursive=True)) \
            for i in range(0, len(paths_filt)) ]
    #print("globs: ", globs)

    batches = min(map(lambda x: len(x), globs))
    #print("batches: ", batches)

    # Make batch list
    batch = list()
    # Loop batches
    for i in range(0, batches):
        # Loop files per batch
        temp = []
        idx = 0
        for j in range(0, len(paths)):
            #print("j: " + str(j))
            if j in chk_idx:
                #print("j true")
                temp.append(globs[idx].pop(0))
                idx = idx + 1
            else:
                #print("j false")
                temp.append('')
            #print("temp: ", temp)
        batch.append(temp)

    #print("batch: ", batch)

    remaining = globs[0]
    for i in range(1, len(globs)):
        remaining.extend(globs[i])

    #print("remaining: ", remaining)

    return batch, remaining
