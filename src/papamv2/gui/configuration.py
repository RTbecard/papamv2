"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Loading and saving of analysis parameters
-----------------------------------------
This module holds functions for scraping and saving GUI options.

Settings are saved as a dictionary object which can be written to disk using
dill.
"""

from PyQt5.QtWidgets import QLineEdit, QCheckBox, QRadioButton, QGroupBox

# If no defaults exist, use these
default_options = dict(
    checkObjects=[
        'rb_soundfieldComponent_pressure',
        'rb_soundfieldComponent_acceleration',
        'rb_particleMotionReceiverSensitivityUnits_velocity',
        'rb_receiverSensitivityAndRecorderGain',
        'rb_fileType_multipleChannelFiles',
        'cb_saveOutput',
        'rb_thresholdUnits_pressure',
        'rb_directionalAnalysis_particleVelocity',
        'rb_binnedAnalysis_rootMeanSquare',
        'le_directogram_particleAcceleration'
        ],
    lineEdits=dict(
        le_recorderX="0",
        le_recorderY="0",
        le_recorderZ="0",
        le_recorderOmni="0",
        le_calibration_windowLength="2048",
        le_bandpass_low = "5000",
        le_bandpass_high = "100",
        le_bandpass_order="3",
        le_output_width="11.75",
        le_output_height="8.25",
        le_output_dpi="150",
        le_event_bandpass_order="0",
        le_timeDomainAnalysis_order="10",
        le_rho="1024",
        le_c="1500",
        le_binnedAnalysis_binSize="1",
        le_binnedAnalysis_overlap="0.5",
        le_timeExceedingThreshold_binSize="1",
        le_timeExceedingThreshold_overlap="0.5",
        le_timeExceedingThreshold_threshold="120",
        le_crestFactor_lower="0.05",
        le_crestFactor_upper="0.95",
        le_riseTime_lower="0.05",
        le_riseTime_upper="0.95",
        le_impulseRMS_lower="0.05",
        le_impulseRMS_upper="0.95",
        le_pulseLength_lower="0.05",
        le_pulseLength_upper="0.95",
        le_spectrogram_windowLength='2048',
        le_spectrogram_overlap='0.5',
        le_spectralProbabilityDensity_windowLength='1024',
        le_spectralProbabilityDensity_overlap='0.5',
        le_spectralProbabilityDensity_psdBinSize="1",
        le_spectralProbabilityDensity_downsampleFactor="1",
        le_spectralProbabilityDensity_percentiles="0.05,0.5,0.95",
        le_directogram_windowLength='2048',
        le_directogram_overlap='0.5',
        le_esd_windowLength='2048',
        le_esd_overlap='0.5',
        le_psd_windowLength='2048',
        le_psd_overlap='0.5',
        le_eventMerging_minEventSpacing='0.1',
        le_eventMerging_minEventDuration='0.1',
        le_eventMargins_padBefore='0.1',
        le_eventMargins_padAfter='0.1'
    ),
    comboboxes=dict(
        le_spectrogram_windowType="0",
        le_directogram_windowType="0",
        le_psd_windowType="0",
        le_esd_windowType="0"
    ))


def readSettings(ui):
    """
    Read settings from all qLineEdits and qComboBoxes
    """

    params = dict(checkObjects=[],
                  radioButtons=[],
                  lineEdits=dict())

    # Read lineEdits
    for le in ui.findChildren(QLineEdit):
        name = le.objectName()
        val = le.text()
        params['lineEdits'][name] = val

    # Read checkBoxes, radio buttons, and groupboxes
    # event analysis groupboxes have checkmarks
    checkObjects = ui.findChildren(QCheckBox) + \
        ui.findChildren(QGroupBox) + \
        ui.findChildren(QRadioButton)

    # Loop checkObjects in UI
    for co in checkObjects:
        if co.isChecked():
            # Save object name is in checked state
            name = co.objectName()
            params['checkObjects'].append(name)

    return params


def writeSettings(ui, params=None):
    """
    Write settings from dictionary to the GUI
    """

    if params is None:
        params = default_options

    # Loop lineEdits in GUI
    for le in ui.findChildren(QLineEdit):
        # Get object name
        name = le.objectName()

        # Check if line edit has entry in params dict
        if name in params['lineEdits'].keys():
            # Update GUI param
            le.setText(params['lineEdits'][name])

    # Loop checkBoxes and groupboxes
    # event analysis groupboxes have checkmarks
    checkObjects = ui.findChildren(QCheckBox) + \
        ui.findChildren(QGroupBox) + \
        ui.findChildren(QRadioButton)

    for co in checkObjects:
        # Get object name
        name = co.objectName()

        if name in params['checkObjects']:
            co.setChecked(True)
        else:
            co.setChecked(False)
