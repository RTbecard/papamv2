"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

These functions are usein in the module papamv2.gui.interface

They are used to check values the user inputs into textfields in the gui and
prevent the entry of incorrect values.
"""

import re
import glob
import os

# ======================= File/Folder Paths ===================================
def validateFile(pattern, te):
    """
    Verify a regex pattern and update the textedit field accordingly

    Return
    ------
    Regular Expression Object if verified, None if not.
    """

    valid = os.path.isfile(pattern) or pattern == ''

    if not valid:
        te.setText("<p style=\"color:red\"> &#10007; </p>")
    else:
        te.setText("<p style=\"color:green\"> &#10003; </p>")

    return valid

def validateGlob(pattern, te):
    """
    Verify a regex pattern and update the textedit field accordingly

    Return
    ------
    Regular Expression Object if verified, None if not.
    """

    glb = glob.glob(pattern, recursive=True)

    if glb:
        valid = os.path.isfile(glb[0])
    else:
        valid = False or pattern == ''

    if not valid:
        te.setText("<p style=\"color:red\"> &#10007; </p>")
    else:
        te.setText("<p style=\"color:green\"> &#10003; </p>")

    return valid

def validateFolder(pattern, te):
    """
    Verify a regex pattern and update the textedit field accordingly

    Return
    ------
    Regular Expression Object if verified, None if not.
    """

    valid = os.path.isdir(pattern)

    if not valid:
        te.setText("<p style=\"color:red\"> &#10007; </p>")
    else:
        te.setText("<p style=\"color:green\"> &#10003; </p>")

    return valid

def validateRanges(strg, te):
    """
    Verify subset range information is formatted correctly
    """

    # Remove whitespaces
    strg = strg.replace(" ", "")

    ptrn = re.compile(pattern="\\d+\\.?\\d*-\\d+\\.?\\d*(?:,\\d+\\.?\\d*-\\d+\\.?\\d*)*")
    mtch = ptrn.fullmatch(strg)

    if mtch is None:
        te.setText("<p style=\"color:red\"> &#10007; </p>")
    else:
        te.setText("<p style=\"color:green\"> &#10003; </p>")

# ================== Verify line edit inputs ==============================
def verifyFloat(strg):
    """
    Verify text is formatted as float
    """
    # Find valid number string in text
    ptrn = re.compile(pattern="-$|-?\\d+\\.?\\d*")
    mtch = ptrn.search(strg)

    if mtch is None:
        return ""

    return mtch.group()

def verifyFloatlist(strg):
    """
    Verify text is formatted as float
    """
    # Find valid number string in text
    ptrn = re.compile(pattern="-$|-?\\d+\\.?\\d*(?:,-?\\d+\\.?\\d*|,)*")
    mtch = ptrn.search(strg)

    if mtch is None:
        return ""

    return mtch.group()

def verifyInt(strg):
    """
    Verify text is formatted as int
    """
    ptrn = re.compile(pattern="\\d+")
    mtch = ptrn.search(strg)

    if mtch is None:
        return ""

    return mtch.group()

def verifyProp(strg):
    """
    Verify text is formatted as float between 0 and 1
    """
    ptrn = re.compile(pattern="0$|0?\\.\\d*")
    mtch = ptrn.search(strg)

    if mtch is None:
        return ""

    return mtch.group()
