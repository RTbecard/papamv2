"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions for interactive GUI behaviour.
"""

import sys
import os
import re
import traceback
import threading

import dill
from scipy.io import wavfile as wav
import numpy as np

# plotting
from matplotlib.figure import Figure

# PyQt Modules
import PyQt5
from PyQt5.QtWidgets import QWidget, QTextEdit, QVBoxLayout
from PyQt5.QtWidgets import QFileDialog, QInputDialog, QMessageBox
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtGui import QPalette, QColor

# paPAM modulesa
from papamv2 import run
from papamv2 import plot
from papamv2 import outputDictionaries as od
from papamv2 import bandpass
from papamv2.calibration import plot as calibplt
from papamv2.calibration.calibration import Calibration
from papamv2.gui.plots_pyqtgraph import wavPreview
from papamv2.gui import configuration as ui_configuration
from papamv2.gui import getOptions as ui_getOptions
from papamv2.gui.textValidation import validateFile, validateGlob, \
        validateFolder, validateRanges, verifyFloat, verifyInt, verifyProp, \
        verifyFloatlist

from papamv2.debug import *

class Interface:
    """
    Attributes
    ----------
    Ui ui:
        Object holding pyqt5 user interface object.
    dict settings_saved:
        Dicitonary object holding default settings.
    """

    class BatchPreview(QWidget):
        """
        Class for displaying window holding preview of batch ananlysis files
        """
        def __init__(self, txt):
            super().__init__()

            # Make text edit populated with content
            txtEdit = QTextEdit()
            txtEdit.setPlainText(txt)

            # Add to layout
            layout = QVBoxLayout()
            layout.addWidget(txtEdit)
            # Set layout in window
            self.setLayout(layout)
            # Set window size
            self.resize(800, 500)
            # Set title
            self.setWindowTitle("Batch Files")

    def __init__(self, ui):
        self.ui = ui
        self.APP_PATH = os.path.relpath(__file__)

        self.sigterm = self.SigTerm()

        # Set color palettes
        self.sel_palletes()

        # Load default settings from file
        file_settings = os.path.join(
            os.path.expanduser("~"),
            "default.pamcfg")

        if os.path.isfile(file_settings):
            # Laod settings
            with open(file_settings, 'rb') as fid:
                config = dill.load(fid)
            # Write settings into UI
            ui_configuration.writeSettings(self.ui, config)
        else:
            ui_configuration.writeSettings(self.ui, None)

        # Set recent folder
        self.folder_recent = self.APP_PATH

        # Link ui elements to functions
        self.connect()

        self.reset_UI()

        # Show first tab
        self.ui.tabs.setCurrentWidget(
            self.ui.tabs.findChild(PyQt5.QtWidgets.QWidget,
                                   "tab_input"))

        self.bp = None      # BatchProcess Class
        self.thrd = None    # Thread Class

    def sel_palletes(self):

        widgets_binned = [
            self.ui.cb_binnedAnalysis,
            self.ui.gb_binnedAnalysis]
        widgets_time = [
            self.ui.gb_timeDomainAnalysis,
            self.ui.gb_timeExceedingThreshold,
            self.ui.gb_impulseRMS,
            self.ui.gb_riseTime,
            self.ui.gb_pulseLength]
        widgets_freq = [
            self.ui.gb_frequencyDomainAnalysis,
            self.ui.gb_spectrogram,
            self.ui.gb_directogram,
            self.ui.gb_spectralProbabilityDensity,
            self.ui.gb_esd]

        for widget in widgets_binned:
            pal = widget.palette()
            pal.setColor(QPalette.Active, QPalette.Base, QColor("#fae7ff"))
            pal.setColor(QPalette.Inactive, QPalette.Base, QColor("#fae7ff"))
            widget.setPalette(pal)

        for widget in widgets_time:
            pal = widget.palette()
            pal.setColor(QPalette.Active, QPalette.Base, QColor("#dbffdc"))
            pal.setColor(QPalette.Inactive, QPalette.Base, QColor("#dbffdc"))
            widget.setPalette(pal)

        for widget in widgets_freq:
            pal = widget.palette()
            pal.setColor(QPalette.Active, QPalette.Base, QColor("#e5f4ff"))
            pal.setColor(QPalette.Inactive, QPalette.Base, QColor("#e5f4ff"))
            widget.setPalette(pal)

    def reset_UI(self):
        """
        Run checks on all dynamic GUI elements.
        """

        # Reset hidden elements
        self.features_energySpectralDensity()
        self.features_spectrogram()
        self.features_directogram()
        self.features_powerSpectralDensity()
        self.features_spectralProbabilityDensity()
        self.features_riseTime()
        self.features_impulseRMS()
        self.features_pulseLength()
        self.features_binnedAnalysis()
        self.features_timeExceedingThreshold()
        self.analysisScopeToggle()
        self.selectInputFiletype()
        self.calibrationType()

        # Reset file path checks
        self.ui.le_file.textChanged.emit(self.ui.le_file.text())
        self.ui.le_particleMotionX.textChanged.emit(
                self.ui.le_particleMotionX.text())
        self.ui.le_particleMotionY.textChanged.emit(
                self.ui.le_particleMotionY.text())
        self.ui.le_particleMotionZ.textChanged.emit(
                self.ui.le_particleMotionZ.text())
        self.ui.le_particleMotionOmni.textChanged.emit(
                self.ui.le_particleMotionOmni.text())
        self.ui.le_outputDirectory.textChanged.emit(
                self.ui.le_outputDirectory.text())
        self.ui.le_calibration_magnitude.textChanged.emit(
                self.ui.le_calibration_magnitude.text())
        self.ui.le_calibration_phase.textChanged.emit(
                self.ui.le_calibration_phase.text())

        # Other checks
        self.ui.le_ranges.textChanged.emit(self.ui.le_ranges.text())
        self.ui.le_bandpass_order.textChanged.emit(self.ui.le_bandpass_order.text())

    def connect(self):
        """
        Connect functions to appropriate Ui elements
        """

        # --------------------------- Buttons --------------------------------
        self.ui.btn_run.clicked.connect(self.button_run)
        self.ui.pb_preview.clicked.connect(self.button_preview)

        # ---------------------------- output directory -----------------------
        # Folder select callback
        def folderValidate():
            validateFolder(self.ui.le_outputDirectory.text(),
                    self.ui.chk_outputDirectory)
        self.ui.le_outputDirectory.textChanged.connect(folderValidate)

        # Input Folder selection
        def folderGet():
            # Get new file
            folderName = QFileDialog.getExistingDirectory(
                    self.ui, "Select Folder", self.folder_recent)
            if folderName != "":
                self.ui.le_outputDirectory.setText(folderName)
                self.folder_recent = os.path.dirname(folderName)
                #return None

        self.ui.pb_outputDirectory.clicked.connect(folderGet)

        # ----------------- Single file input --------------------------------
        self.ui.rb_fileType_singleChannelFiles.toggled.connect(
            lambda x: self.selectInputFiletype())
        self.ui.rb_fileType_multipleChannelFiles.toggled.connect(
            lambda x: self.selectInputFiletype())

        # Input file selection
        def fileGet():
            # Get new file
            fileName, _ = QFileDialog.getOpenFileName(
                self.ui,
                "Select File",
                self.folder_recent,
                "wav Files (*.wav *.WAV);; All Files (*)",
                "wav Files (*.wav *.WAV)")
            if fileName != "":
                self.ui.le_file.setText(fileName)
                self.folder_recent = os.path.dirname(fileName)
                # return None
        self.ui.pb_file.clicked.connect(fileGet)

        # Verify file select
        def fileValidate():
            validateGlob(self.ui.le_file.text(), self.ui.chk_file)
        self.ui.le_file.textChanged.connect(fileValidate)

        # --------------------- Multifile input -------------------------------
        def setGetChanFile(pb, le):
            # Define click callback function
            def callback():
                # Get new file
                fileName, _ = QFileDialog.getOpenFileName(
                    self.ui,
                    "Select File",
                    self.folder_recent,
                    "wav Files (*.wav *.WAV);; All Files (*)",
                    "wav Files (*.wav *.WAV)")
                if fileName != "":
                    le.setText(fileName)
                    self.folder_recent = os.path.dirname(fileName)
            # Assign callback function
            pb.clicked.connect(callback)

        def setChanFileValidation(le, ck):
            le.textChanged.connect(lambda: validateGlob(le.text(), ck))
        # ------ channel X ------
        setGetChanFile(self.ui.pb_particleMotionX, self.ui.le_particleMotionX)
        setChanFileValidation(
                self.ui.le_particleMotionX, self.ui.chk_particleMotionX)
        # ------ channel Y ------
        setGetChanFile(self.ui.pb_particleMotionY, self.ui.le_particleMotionY)
        setChanFileValidation(
                self.ui.le_particleMotionY, self.ui.chk_particleMotionY)
        # ------ channel Z ------
        setGetChanFile(self.ui.pb_particleMotionZ, self.ui.le_particleMotionZ)
        setChanFileValidation(
                self.ui.le_particleMotionZ, self.ui.chk_particleMotionZ)
        # ------ channel Omni ------
        setGetChanFile(self.ui.pb_particleMotionOmni,
                self.ui.le_particleMotionOmni)
        setChanFileValidation(
                self.ui.le_particleMotionOmni, self.ui.chk_particleMotionOmni)

        # ------------------ Subset Analysis ----------------------------------
        self.ui.le_ranges.textEdited.connect(lambda:
                validateRanges(self.ui.le_ranges.text(), self.ui.chk_ranges))

        # -------------------- Event detection --------------------------------
        # Change UI layout based on selected analysis scope
        self.ui.rb_sequentialSubsets.clicked.connect(self.analysisScopeToggle)
        self.ui.rb_subsets.clicked.connect(self.analysisScopeToggle)
        self.ui.rb_wholeFile.clicked.connect(self.analysisScopeToggle)

        # --------------------- Analysis Features -----------------------------
        # Add/remove Analysis Settings groupboxes based on
        self.ui.cb_binnedAnalysis.clicked.connect(self.features_binnedAnalysis)
        self.ui.cb_timeExceedingThreshold.clicked.connect(
                self.features_timeExceedingThreshold)
        self.ui.cb_riseTime.clicked.connect(self.features_riseTime)
        self.ui.cb_impulseRMS.clicked.connect(self.features_impulseRMS)
        self.ui.cb_pulseLength.clicked.connect(self.features_pulseLength)
        self.ui.cb_psd.clicked.connect(self.features_powerSpectralDensity)
        self.ui.cb_esd.clicked.connect(self.features_energySpectralDensity)
        self.ui.cb_spectrogram.clicked.connect(self.features_spectrogram)
        self.ui.cb_spectralProbabilityDensity.clicked.connect(
            self.features_spectralProbabilityDensity)
        self.ui.cb_directogram.clicked.connect(self.features_directogram)

        # ------------------- Calibration input -------------------------------
        def calibGetMag():
            # Get new file
            fileName, _ = QFileDialog.getOpenFileName(
                self.ui,
                "Select File",
                self.folder_recent,
                "csv Files (*.csv *.CSV);; All Files (*)",
                "csv Files (*.csv *.CSV)")
            if fileName != "":
                self.ui.le_calibration_magnitude.setText(fileName)
                self.folder_recent = os.path.dirname(fileName)
                #return None
        self.ui.pb_calibration_magnitude.clicked.connect(calibGetMag)

        def calibGetPhase():
            # Get new file
            fileName, _ = QFileDialog.getOpenFileName(
                self.ui,
                "Select File",
                self.folder_recent,
                "csv Files (*.csv *.CSV);; All Files (*)",
                "csv Files (*.csv *.CSV)")
            if fileName != "":
                self.ui.le_calibration_phase.setText(fileName)
                self.folder_recent = os.path.dirname(fileName)
                #return None
        self.ui.pb_calibration_phase.clicked.connect(calibGetPhase)

        # Verify file select
        def calibValidateMag():
            validateFile(self.ui.le_calibration_magnitude.text(),
                    self.ui.chk_calibration_magnitude)
        self.ui.le_calibration_magnitude.textChanged.connect(calibValidateMag)
        def calibValidatePhase():
            validateFile(self.ui.le_calibration_phase.text(),
                    self.ui.chk_calibration_phase)
        self.ui.le_calibration_phase.textChanged.connect(calibValidatePhase)

        # Calibration preview
        def plotCalibration():

            # dialog for entering samplerate
            text, ok = QInputDialog.getDouble(
                self.ui, 'Calibration Preview', 'Sample Rate (Hz)',
                value = 44100)

            if not ok:
                return

            # init calibration object
            calib = Calibration(float(text))

            # get recorder gain
            recorder_gain = [
                self.ui.le_recorderX.text(),
                self.ui.le_recorderY.text(),
                self.ui.le_recorderZ.text(),
                self.ui.le_recorderOmni.text()]
                
            if(any(x == '' for x in recorder_gain)):
            	 raise ValueError("Missing recorder sensitivity values.")
                
            recorder_gain = list(map(float, recorder_gain))

            # get unit_in
            if self.ui.rb_particleMotionReceiverSensitivityUnits_velocity.isChecked():
                unit_in = 'v'
            else:
                unit_in = 'a'

            file_mag = self.ui.le_calibration_magnitude.text()
            file_phase = self.ui.le_calibration_phase.text()
            debug(f"file_mag: {file_mag}")
            debug(f"file_phase: {file_phase}")

            calib.read(file_mag, file_phase, recorder_gain, unit_in)

            units_out = "".join([
                self.ui.rb_soundfieldComponent_pressure.isChecked()*'p',
                self.ui.rb_soundfieldComponent_displacement.isChecked()*'d',
                self.ui.rb_soundfieldComponent_velocity.isChecked()*'v',
                self.ui.rb_soundfieldComponent_acceleration.isChecked()*'a'])

            for unit_out in units_out:

                # Make calibration impulses
                calib.createCalibrationImpulses(unit_out)

                # Show Magnitude calibration
                tab = "CalibMag-" + od.unit_tab[unit_out]
                fcn = calibplt.plot_mag_pyqt
                args = [calib, unit_out]
                if os.path.exists(file_mag):
                    # plot into output window
                    self.ui.plotWindow.subplot.emit(tab, fcn, args, file_mag)

                # Show phase calibration
                tab = "CalibPhase-" + od.unit_tab[unit_out]
                fcn = calibplt.plot_phase_pyqt
                args = [calib, unit_out]
                if os.path.exists(file_phase):
                    # plot into output window
                    self.ui.plotWindow.subplot.emit(tab, fcn, args, file_phase)

        self.ui.pb_calibration_preview.clicked.connect(plotCalibration)

        # --------------------------- bandpass --------------------------------
        self.ui.le_bandpass_order.textChanged.connect(self.bandpass_text)

        def bandpass_preview():
            low = float(self.ui.le_bandpass_low.text())
            high = float(self.ui.le_bandpass_high.text())
            order = int(self.ui.le_bandpass_order.text())

            if order > 0:

                # dialog for entering samplerate
                fs, ok = QInputDialog.getDouble(
                    self.ui, 'Bandpass Preview', 'Sample Rate (Hz)',
                    value = 44100)
                if not ok:
                    return

                bands = bandpass.getBandFilters(high, low, order, fs, 0)
                debug(f"bands: {bands}")

                fcn = bandpass.plot_butter_pyqt
                args = [bands, fs]
                meta = "highpass: %i, lowpass: %i, order: %i" % (low, high, order)
                self.ui.plotWindow.subplot.emit("Bandpass", fcn, args, meta)

        self.ui.pb_bandpass_preview.clicked.connect(bandpass_preview)

        def decidecade_preview():
            low = float(self.ui.le_bandpass_low.text())
            high = float(self.ui.le_bandpass_high.text())
            order = int(self.ui.gb_bandpass.isChecked())
            dd_order = int(self.ui.le_timeDomainAnalysis_order.text())

            if dd_order > 0:

                # dialog for entering samplerate
                fs, ok = QInputDialog.getDouble(
                    self.ui, 'Decidecade Preview', 'Sample Rate (Hz)',
                    value = 44100)
                if not ok:
                    return

                bands = bandpass.getBandFilters(high, low, order, fs, dd_order)
                # remove first band
                debug(f"bands: {bands}")
                bands = bands[1:]
                fcn = bandpass.plot_butter_pyqt
                args = [bands, fs]
                self.ui.plotWindow.subplot.emit("Decidecade", fcn, args, None)

        # ------ Menu bar
        file_settings = os.path.join(
            os.path.expanduser("~"),"default.pamcfg")
        self.ui.menu_openConfiguration.triggered.connect(
                lambda x: self.loadConfiguration(None))
        self.ui.menu_saveConfigurationAs.triggered.connect(
                lambda x: self.saveConfiguration(None))
        self.ui.menu_setConfigurationAsDefault.triggered.connect(
            lambda x: self.saveConfiguration(file_settings))
        self.ui.menu_resetDefaultConfiguration.triggered.connect(
            self.resetDefaultConfiguration)
        #self.ui.menu_output.triggered.connect(self.ui.plotWindow.show)
        self.ui.menu_output.triggered.connect(
                lambda: self.ui.plotWindow.show())

        # -------------------- Recorder Sensitivity ---------------------------
        self.ui.a_calibrationTone.triggered.connect(
                self.createCalibrationTone)
        self.ui.a_recorderSensitivity.triggered.connect(
                self.calcRecorderSensitivity)
        self.ui.rb_receiverSensitivityAndRecorderGain.toggled.connect(
            lambda x: self.calibrationType())
        # ----------------------- EditText Validation -------------------------
        # ------ Input/Output
        def setTextEditCheck(obj, check):
            """
            When a LineEdit value is changed, only update the value if it passes
            the provided check.  This is for preventing the user from entering
            incorrect values
            """
            obj.textEdited.connect(lambda x: obj.setText(str(check(x))))

        setTextEditCheck(self.ui.le_recorderX, verifyFloat)
        setTextEditCheck(self.ui.le_recorderY, verifyFloat)
        setTextEditCheck(self.ui.le_recorderZ, verifyFloat)
        setTextEditCheck(self.ui.le_recorderOmni, verifyFloat)
        setTextEditCheck(self.ui.le_output_width, verifyFloat)
        setTextEditCheck(self.ui.le_output_height, verifyFloat)
        # ------ General Settings
        setTextEditCheck(self.ui.le_bandpass_high, verifyFloat)
        setTextEditCheck(self.ui.le_bandpass_low, verifyFloat)
        setTextEditCheck(self.ui.le_bandpass_order, verifyInt)
        setTextEditCheck(self.ui.le_rho, verifyFloat)
        setTextEditCheck(self.ui.le_c, verifyFloat)
        # ------ Analysis settings
        setTextEditCheck(self.ui.le_binnedAnalysis_binSize, verifyFloat)
        setTextEditCheck(self.ui.le_binnedAnalysis_overlap, verifyProp)
        setTextEditCheck(self.ui.le_timeExceedingThreshold_binSize, verifyFloat)
        setTextEditCheck(self.ui.le_timeExceedingThreshold_overlap, verifyProp)
        setTextEditCheck(self.ui.le_timeExceedingThreshold_threshold, verifyFloat)
        setTextEditCheck(self.ui.le_riseTime_lower, verifyFloat)
        setTextEditCheck(self.ui.le_riseTime_upper, verifyFloat)
        setTextEditCheck(self.ui.le_impulseRMS_lower, verifyFloat)
        setTextEditCheck(self.ui.le_impulseRMS_upper, verifyFloat)
        setTextEditCheck(self.ui.le_pulseLength_lower, verifyFloat)
        setTextEditCheck(self.ui.le_pulseLength_upper, verifyFloat)
        setTextEditCheck(self.ui.le_spectrogram_windowLength, verifyInt)
        setTextEditCheck(self.ui.le_spectrogram_overlap, verifyProp)
        setTextEditCheck(self.ui.le_directogram_windowLength, verifyInt)
        setTextEditCheck(self.ui.le_directogram_overlap, verifyProp)
        setTextEditCheck(self.ui.le_spectralProbabilityDensity_windowLength, verifyInt)
        setTextEditCheck(self.ui.le_spectralProbabilityDensity_overlap, verifyProp)
        setTextEditCheck(self.ui.le_spectralProbabilityDensity_psdBinSize, verifyFloat)
        setTextEditCheck(self.ui.le_spectralProbabilityDensity_percentiles, verifyFloatlist)
        setTextEditCheck(self.ui.le_psd_windowLength, verifyInt)
        setTextEditCheck(self.ui.le_psd_overlap, verifyProp)
        setTextEditCheck(self.ui.le_esd_windowLength, verifyInt)
        setTextEditCheck(self.ui.le_esd_overlap, verifyProp)

    # ========================= Run analysis ==================================
    class SigTerm(PyQt5.QtCore.QObject):
        """
        Class for allowing analysis to be terminated from outside the worker
        thread.
        """

        # signals must be declared as static class variables
        sig = pyqtSignal()

        def __init__(self):
            super().__init__()
            self.term = False
            self.sig.connect(self.terminate)

        @pyqtSlot()
        def terminate(self):
            """
            Set terminate flag.  This will cause paPAM to stop and exit early
            """
            #print("Terminal signal recieved!")
            self.term = True

    def button_run(self):
        """
        paPAMv2 will run on a worker thread, so the GUI will not be locked.
        """
        # ------ Loop file batches
        # Grab parameters from UI
        if self.ui.btn_run.text() == "Stop":
            # ------ Kill running thread
            # Send termination signal
            self.ui.btn_run.setEnabled(False)
            self.sigterm.sig.emit()
            # disable stop button
        else:
            # ------ Setup run function for worker threaad
            sys.stdout.clear()
            def rn():
                # Reset sigterm
                self.sigterm.term = False
                try:
                    params = ui_getOptions.get_options(self.ui)
                except RuntimeError as e:
                    #message = "\n".join(traceback.format_tb(e.__traceback__))
                    #message = message + "\n" + str(e)
                    #print(message)
                    print(repr(e))
                    print("Invalid parameters... stopping analysis :(")
                    return

                batches, _ = ui_getOptions.batch_glob(params["wav_files"])
                for (i, batch) in enumerate(batches):

                    print(f"Starting file {i + 1} of {len(batches)}...")

                    # Clear plotting window
                    self.ui.plotWindow.clear.emit()
                    params["wav_files"] = batch
                    #self.ui.setEnabled(False)
                    self.ui.tabs.setEnabled(False)
                    self.ui.scrollArea.setEnabled(False)
                    self.ui.cb_saveOutput.setEnabled(False)
                    self.ui.btn_run.setText("Stop")

                    if self.ui.cb_saveOutput.isChecked():
                        # ------ Save output to disk
                        if len(batches) > 1:
                            # Disable plots for batch processing
                            params['hide_plots'] = True
                    else:
                        # ------ No output
                        #print('Grab GUI settings...')
                        # print(params)
                        params['no_output'] = True
                        params['output_dir'] = ''
                        params['output_name'] = ''
                        params['hide_plots'] = False
                        #print("Calling paPAMv2...")

                    # ------ Batch processing
                    try:
                        run.run(params, self.ui, self.sigterm)
                    except Exception as e:
                        message = "\n".join(traceback.format_tb(e.__traceback__))
                        message = message + "\n" + repr(e)
                        print(message)
                        print("File stopped due to error... :(")

                print("All files complete.  Analysis has finished.")

                # Renable UI elements
                self.ui.tabs.setEnabled(True)
                self.ui.scrollArea.setEnabled(True)
                self.ui.cb_saveOutput.setEnabled(True)
                self.ui.btn_run.setText("Run")
                self.ui.btn_run.setEnabled(True)

            # ------ Launch worker thread
            self.thrd = threading.Thread(target=rn)
            self.thrd.start()
            self.ui.plotWindow.show()

    def button_preview(self):
        """
        Show preview of batch processing list
        """
        try:
            # ------ Get batch lists
            if self.ui.rb_fileType_multipleChannelFiles.isChecked():
                # multi-chan files
                batch, remain = ui_getOptions.batch_glob([self.ui.le_file.text()])

                txt_lst = list(map(lambda x: "multi-chan: " + x[0], batch))
            else:
                # single-chan files (multiple files per batch)
                paths = [self.ui.le_particleMotionX.text(),
                        self.ui.le_particleMotionY.text(),
                        self.ui.le_particleMotionZ.text(),
                        self.ui.le_particleMotionOmni.text()]
                batch, remain = ui_getOptions.batch_glob(paths)

                txt_lst = list(map(lambda x: "x: " + x[0] + "\ny: " + x[1] +
                    "\nz: " + x[2] + "\no: " + x[3] + "\n", batch))

            #print('txt_lst')
            #print(txt_lst)

            txt = "Batches:\n" + "\n".join(txt_lst)
            txt = txt + "\n\nRemaining Files:\n" + "\n".join(remain)
            #print('txt')
            #print(txt)

            self.bp = self.BatchPreview(txt)
            self.bp.show()
        except Exception as e:
            print(e)

    # ================== Show/Hide UI elements ================================
    def tabShow(self, name, val):
        """ Show/hide tab with matching object name. """
        idx = self.ui.tabs.indexOf(self.ui.tabs.findChild(QWidget, name))
        self.ui.tabs.setTabVisible(idx, val)

    def goToTab(self, name):
        """ Focus tab matching name. """
        tab = self.ui.tabs.findChild(QWidget, name)
        self.ui.tabs.setCurrentWidget(tab)

    def selectInputFiletype(self):
        """
        Automatically show/hide UI elements based on file input type
        """
        # Exit if not selected (prevent duplicate function callbacks)
        if self.ui.rb_fileType_singleChannelFiles.isChecked():
            self.ui.gb_singleFileSelect.setVisible(True)
            self.ui.gb_multipleFileSelect.setVisible(False)
        else:
            self.ui.gb_singleFileSelect.setVisible(False)
            self.ui.gb_multipleFileSelect.setVisible(True)

    def analysisScopeToggle(self):
        """
        Show/hide analysis scope tabs
        """
        if self.ui.rb_subsets.isChecked():
            # Show subset tab
            self.tabShow("tab_subsets", True)
            self.tabShow("tab_sequentialSubsets", False)
            self.goToTab("tab_subsets")
        elif self.ui.rb_sequentialSubsets.isChecked():
            # Show sequential subsets tab
            self.tabShow("tab_subsets", False)
            self.tabShow("tab_sequentialSubsets", True)
            self.goToTab("tab_sequentialSubsets")
        else:
            # Whole file selected
            self.tabShow("tab_subsets", False)
            self.tabShow("tab_sequentialSubsets", False)
            self.goToTab("tab_input")

    def bandpass_text(self):
        """
        Calculate and show bandpass rolloff
        """
        if self.ui.le_bandpass_order.text() in ["0", "", None]:
            self.ui.bandpass_text.setText("Bandpass disabled.")
        else:
            rolloff = int(self.ui.le_bandpass_order.text()) * 12
            self.ui.bandpass_text.setText(
                "Bandpass rolloff: %i dB/octave" % rolloff)

    def features_binnedAnalysis(self):
        """ Show/hide time exceeding threshold options """
        if self.ui.cb_binnedAnalysis.isChecked():
            self.ui.gb_binnedAnalysis.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_binnedAnalysis.setVisible(False)
            self.ui.cb_binnedAnalysis_kurtosis.setChecked(False)
            self.ui.cb_binnedAnalysis_rootMeanSquare.setChecked(False)
            self.ui.cb_binnedAnalysis_zeroToPeak.setChecked(False)
            self.ui.cb_binnedAnalysis_crestFactor.setChecked(False)
            self.ui.cb_binnedAnalysis_decidecadeRms.setChecked(False)

    def features_timeExceedingThreshold(self):
        """ Show/hide time exceeding threshold options """
        if self.ui.cb_timeExceedingThreshold.isChecked():
            self.ui.gb_timeExceedingThreshold.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_timeExceedingThreshold.setVisible(False)

    def features_riseTime(self):
        """ Show/hide risetime analysis options """
        if self.ui.cb_riseTime.isChecked():
            self.ui.gb_riseTime.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_riseTime.setVisible(False)

    def features_pulseLength(self):
        """ Show/hide pulse length options """
        if self.ui.cb_pulseLength.isChecked():
            self.ui.gb_pulseLength.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_pulseLength.setVisible(False)

    def features_impulseRMS(self):
        """ Show/hide pulse length options """
        if self.ui.cb_impulseRMS.isChecked():
            self.ui.gb_impulseRMS.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_impulseRMS.setVisible(False)

    def features_powerSpectralDensity(self):
        """ Show/hide PSD options """
        if self.ui.cb_psd.isChecked():
            self.ui.gb_psd.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_psd.setVisible(False)

    def features_energySpectralDensity(self):
        """ Show/hide ESD options """
        if self.ui.cb_esd.isChecked():
            self.ui.gb_esd.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_esd.setVisible(False)

    def features_spectrogram(self):
        """ Show/hide spectrogram options """
        if self.ui.cb_spectrogram.isChecked():
            self.ui.gb_spectrogram.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_spectrogram.setVisible(False)

    def features_directogram(self):
        """ Show/hide directional spectrogram options """
        if self.ui.cb_directogram.isChecked():
            self.ui.gb_directogram.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_directogram.setVisible(False)

    def features_spectralProbabilityDensity(self):
        """ Show/hide directional spectrogram options """
        if self.ui.cb_spectralProbabilityDensity.isChecked():
            self.ui.gb_spectralProbabilityDensity.setVisible(True)
            self.goToTab("tab_analysisFeatures")
        else:
            self.ui.gb_spectralProbabilityDensity.setVisible(False)

    def calibrationType(self):
        """ Show/hide calibration options """
        if not self.ui.rb_receiverSensitivityAndRecorderGain.isChecked():
            self.ui.gb_receiverCalibration.setVisible(False)
            self.ui.le_calibration_magnitude.setText("")
            self.ui.le_calibration_phase.setText("")
            # Change text lables
            txt = "End-to-end Sensitivity (" + \
                "dB re FS/{\u03bcPa, nm/s, \u03bcm/s\u00b2}) "
            self.ui.gb_RecorderGain.setTitle(txt)
        else:
            self.ui.gb_receiverCalibration.setVisible(True)
            txt = "Recorder Sensitivity (dB re FS/V)"
            self.ui.gb_RecorderGain.setTitle(txt)

    # ======================= save/load configuration =========================
    def saveConfiguration(self, filename=None):
        """ Scrape and save GUI settings to file """
        print(filename)
        # Scrape settings
        config = ui_configuration.readSettings(self.ui)
        if filename is None:
            # Get file path
            filename, _ = QFileDialog.getSaveFileName(
                self.ui, "Save Configuration", "./", "paPAM config (*.pamcfg)")
            print(filename)
            if filename == '':
                # No file selected by user
                return

        # Check for extension at end of file
        pattern = re.compile(".+\\.pamcfg$")
        if pattern.fullmatch(filename) is None:
            filename = filename + ".pamcfg"

        with open(filename, 'wb') as fid:
            dill.dump(config, fid)
        print("settings saved to: " + filename)

    def loadConfiguration(self, filename=None):
        """ Load GUI settings from file """
        if filename is None:
            # Get file to load
            filename = QFileDialog.getOpenFileName(
                    self.ui, "Load Configuration", "./",
                    "paPAM config (*.pamcfg)")
        if filename[0] == '':
            return

        with open(filename[0], 'rb') as fid:
            config = dill.load(fid)
        ui_configuration.writeSettings(self.ui, config)
        self.reset_UI()

    def resetDefaultConfiguration(self):
        # Detele default config
        if os.path.exists("default.pamcfg"):
            os.remove("default.pamcfg")
        # Write default settings
        ui_configuration.writeSettings(self.ui, None)
    # ========================= Recorder Sensitivity ==========================

    def createCalibrationTone(self):
        """ Create 1000Hz tone for recorder sensitivity calculation """
        # ------ Get file to load
        filename = QFileDialog.getSaveFileName(self.ui,
                "Create Calibration Tone", "./CalibrationTone_1kHz.wav",
                filter = "wav file (*.wav *.WAV)")

        if filename[0] == '':
            return

        fs = 44100
        f = 1000
        t = 10
        bit = 16
        bit_scale = 2**(bit - 1)

        sig = np.array(np.int16(
            np.sin(np.arange(0, t, step = 1/fs)* 2*np.pi * f ) * bit_scale
            ), dtype = 'int16')

        wav.write(filename[0], rate = fs,
                data = np.array([sig, sig], dtype = 'int16').T)

    def calcRecorderSensitivity(self):
        """ Calculate recorder sensitivity from recorded calibration tone """
        # ------ Get file to load
        filename = QFileDialog.getOpenFileName(self.ui,
                "Calibration Tone Playback", "./",
                "wav file (*.wav *.WAV)")

        if filename[0] == '':
            return

        # ------ Get RMS Voltage
        voltage, ok = QInputDialog.getText(self.ui,
                "Recorder Sensitivity",
                "Root-mean-square Voltage (V)")
        if not ok:
            return

        # Check that voltage is float
        ptrn = re.compile(pattern="\\d+\\.?\\d*")
        mtch = ptrn.search(voltage)

        if mtch is None:
            txt = "Voltage must be a positive float."
            QMessageBox.critical(self.ui, "Recorder Sensitivity", txt)
            return

        # ------ Read into memory, convert to FS units
        fs, sig = wav.read(filename[0])
        if len(sig.shape) == 1:
            sig = np.array([sig]).T # Convert to 2D array

        # Possible wav data formats
        formats = {'float32': (1, 0),
                   'int32': ((2**31), 0),
                   'int16': ((2**15), 0),
                   'uint8': ((2**8), 8*16)}
        dstr = str(sig.dtype) # Realized format
        # normalize to FS (-1, 1)
        sig = sig - formats[dstr][1]
        sig = sig / formats[dstr][0]

        # Show preview of wav file
        pamPlot = plot.PamPlot(False, ui = self.ui)
        pamPlot.plot("Receiver Calibration Preview", wavPreview, [sig, fs])

        # ------ Calculate Receiver Sensitivity
        L_rSens = [20*np.log10(np.std(sig[:, i])/float(voltage))
                for i in range(sig.shape[1])]

        print("\nRecorder Calibration\n"+ '-'*20)
        print(f"File: {filename[0]}")
        print(f"rmsV: {voltage}")
        for i in range(sig.shape[1]):
            print("".join(["Chan ", str(i + 1), ": ",
                str(round(L_rSens[i],3)), " dB (re 1 FS/V)"]))
        print("\n")
        self.ui.plotWindow.show()
