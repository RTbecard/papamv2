"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions holding code for plotting each analysis feature in paPAM.
Function inputs are the results dictionaryes from the module
analysisFeatures.py.

Each function returns a list of plot arguments/values which are used to run
and save plots in the main paPAMv2 module.

Metadata for each analysis is taken from the signal subset parameter.
"""

import numpy as np

from PyQt5 import QtGui
import pyqtgraph as pqg

from papamv2 import outputDictionaries as od
from papamv2.helpers_pyqtgraph import \
    make_plot, make_layout, make_colorBar, setLabel, rangify, grey1, tomato

from papamv2.debug import debug


def wavPreview(sig, fs):
    """
    Show preview of wav file for receiver sensitivity
    """

    layout = pqg.GraphicsLayoutWidget()
    layout.setBackground('w')

    debug(f"sig.shape {sig.shape}")

    # ------ Plot waveform for each channel
    for i in range(sig.shape[1]):

        debug(f"i {i}")

        pw = layout.addPlot(row = i, col = 0)
        pw.setDefaultPadding(0.0)
        if i == 0:
            pw1 = pw
        else:
            pw.setXLink(pw1)
            pw.setYLink(pw1)

        grid = pqg.GridItem()
        grid.setTickSpacing(x = [None, None], y = [None, None])
        grid.setTextPen(None)
        pw.addItem(grid)

        # ---- Channel plots
        seconds = np.array(range(0, sig.shape[0]), dtype='f')/fs
        # pw.plot(seconds, data, pen = pqg.mkPen(od.chan_color[chan]))
        rangify(pw, seconds, sig[:,i])

        # Add axis labels
        if i == sig.shape[1] - 1:
            setLabel(pw, 'bottom','Time (seconds)')
        ylab = 'Channel %i' % (i + 1)
        setLabel(pw, 'left', ylab)

    return layout

