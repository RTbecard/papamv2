"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Graphical Interface for paPAMv2
-------------------------------
pyqt5 based GUI for paPAMv2.

"""
# Read CLI arguemnts
import sys
import os

# For getting correct pyinstaller paths
from os import path
# For building the GUI
from PyQt5 import QtWidgets, uic, QtGui
# Interfacing with UI
from papamv2.gui import interface as intf
from papamv2 import plot

# Version number for display in titlebar
pth = os.path.join(os.path.dirname(__file__), "../", "VERSION")
with open(pth, "r") as fh:
    VERSION = fh.read().strip()

print("version check: " + VERSION)

# This returns stack traces for segfaults.  Hopefully this helps debug
# weird qt issues
import faulthandler
faulthandler.enable()

# ############## Load UI file
"""
base_path is required for pyinstaller.  It points to the folder in the
resulting package that holds files added with the --add-data argument.
"""
try:
    # PyInstaller creates a temp folder for file resources and stores path in
    # sys._MEIPASS
    BASE_PATH = sys._MEIPASS # Ignore the error message here.
except Exception:
    # When not run by pyinstaller, default to cwd
    BASE_PATH = os.path.abspath(".")

# Location of UI file
path = os.path.dirname(os.path.abspath(__file__))
FILE_UI = os.path.join(path, "mainwindow.ui")


############### Setup main window initializer
class Ui(QtWidgets.QMainWindow):
    """
    Load qt ui from file and connect interface functions
    """

    # Location of current app location
    APP_PATH = os.path.relpath(__file__)


    def __init__(self):

        # Disable stdout (causes pyinstaller crashes in no console mode)
        # This is for testing before console output was redirected in the app.
        # In the app stdout is output to a qt console.
        # f = open(os.devnull, 'w')
        #sys.stdout = f
        #sys.stderr = f

        #super(Ui, self).__init__()  # call parent class init
        super().__init__()  # call parent class init
        uic.loadUi(FILE_UI, self)   # Load UI from file


        # Connect signals to interface functions (ui_interface.py)
        self.interface = intf.Interface(self)

        # Set window title
        self.setWindowTitle("paPAM v" + VERSION)

        # ------ Make plotting window
        self.plotWindow = plot.PlotWindow()

        # ------ Reroute stderr and stdout to plotwindow
        self.console = plot.Console(self.plotWindow.te)

        sys.stderr = self.console
        sys.stdout = self.console

        # ------ Display main UI
        self.show()

def run():
    """
    Launch Qt main  application window (i.e. start GUI interface)
    """

    APP = QtWidgets.QApplication(sys.argv)

    #WINDOW = Ui()
    Ui()

    # Specify a platform agnostic style.
    # Necesary for consistent styling across platforms
    # print(QtWidgets.QStyleFactory.keys())
    APP.setStyle(QtWidgets.QStyleFactory.create("Fusion"))

    if sys.platform.startswith('win'):
        # Add window icon on windows
        APP.setWindowIcon(QtGui.QIcon(os.path.join(path, 'icon.ico')))

    # Exit program when UI closes
    sys.exit(APP.exec_())

