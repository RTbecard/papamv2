"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions holding code for plotting each analysis feature in paPAM.
Function inputs are the results dictionaryes from the module
analysisFeatures.py.

Each function returns a list of plot arguments/values which are used to run
and save plots in the main paPAMv2 module.

Metadata for each analysis is taken from the signal subset parameter.
"""

import numpy as np

from PyQt5 import QtGui
import pyqtgraph as pqg

from papamv2 import outputDictionaries as od
from papamv2.helpers_pyqtgraph import \
    make_plot, make_layout, make_colorBar, setLabel, rangify, grey1, tomato, make_plotItem

from papamv2.debug import debug

def dir_spectrogram_image(data, offset, scale):
    """
    Make ImageItem for plotting directional spectrogram.
    """

    img = pqg.ImageItem(image = data.T.copy())
    # --- set transformation for proper axis units
    tr = QtGui.QTransform()
    tr.scale(*scale)
    tr.translate(*offset)
    img.setTransform(tr)

    return img

def dir_spectrogram(d_arrays, f, times, fs, rng, window_len, olap, dims, band, unit_out):
    """
    to-do
    """
    layout = make_layout()

    pw1 = layout.addPlot(row=0, col=1)
    pw2 = layout.addPlot(row=0, col=2)
    pw3 = layout.addPlot(row=1, col=2)
    pw4 = layout.addPlot(row=1, col=1)
    # Make plotWidget
    #pw = make_plot(legend = False, grid = False)
    #layout.addItem(pw)

    layout.addLabel("Frequency (Hz)", row = 0, col = 0, rowspan = 2, angle = -90,
                    color = "#333")
    layout.addLabel("Time (seconds)", row = 2, col = 1, colspan = 2,
                    color = "#333")

    bearing = d_arrays['bearing']
    v_angle = d_arrays['vertical_angle']

    scale = (olap*window_len)/fs, fs/(window_len-2)
    df = window_len/fs
    offset = ( rng.start/(window_len*olap), df )

    # Get frequency indicies for bandpass values
    ylim = (band['highpass'], band['lowpass'])
    ylim_idx = (
        next((x for x in range(0,f.size) if f[x] >= ylim[0]), 0),
        next((x for x in range(0,f.size) if f[x] >= ylim[1]), f.size)
    )

    if unit_out == 'i':
        mag = d_arrays['si']
        eccen = d_arrays['pressure_intensity_index']
        e_lab = "<math>L<sub>K</sub> / dB</math>"
        e_rounding = 1
        mag_lab = '<math>' + \
            'L<sub>%s,f</sub> (re (%s)&middot;Hz<sup>-1</sup>) / dB</math>' % \
            (od.unit_html[unit_out], od.ref_html[unit_out])
        bearing_lab = "SI Rel. Bearing \n(deg re +x)"
        vangle_lab = "SI Vert. Angle \n(deg)"
        deg_lims = (-180, 180)
        eccen_lims = (
            np.min(eccen[range(*ylim_idx),:]),
            np.max(eccen[range(*ylim_idx),:])
        )
    else:
        mag = d_arrays['psd']
        eccen = d_arrays['eccentricity']
        e_lab = "Spectral Eccentricity (e)"
        e_rounding = 0.1
        mag_lab = '<math>' + \
            'L<sub>%s,f</sub> (re (%s)<sup>2</sup>&middot;Hz<sup>-1</sup>) / dB</math>' % \
            (od.unit_html[unit_out], od.ref_html[unit_out])
        bearing_lab = "PM Rel. Bearing \n(deg re +x)"
        vangle_lab = "PM Vert. Angle \n(deg)"
        deg_lims = (-90, 90)
        eccen_lims = (0, 1)

    xlim = (rng.start/fs, rng.stop/fs)

    # ----------------------- PSD row -----------------------------------------
    debug("Make PSD panel")
    debug(f"mag.shape: {mag.shape}")
    # --- Make image
    img_psd = dir_spectrogram_image(mag, offset, scale)
    pw1.addItem(img_psd, row = 0, col = 0)
    pw1.setLimits(xMin = xlim[0], xMax =xlim[1],
                 yMin = band['highpass'],
                 yMax = band['lowpass'])
    pw1.setRange(yRange = (band['highpass'], band['lowpass']))
    # ------ Colorbar
    clab =  mag_lab
    lims = (
        round(mag[range(*ylim_idx),:].min()),
        round(mag[range(*ylim_idx),:].max()))
    make_colorBar(lims, clab, img_psd, pw1)

    # ----------------------------- bearing -----------------------------------
    # --- Make image
    debug("Make horiz bearing panel")
    debug(f"bearing.shape: {bearing.shape}")
    img_bearing = dir_spectrogram_image(bearing, offset, scale)
    pw2.addItem(img_bearing, row = 1, col = 0)
    pw2.setLimits(xMin = xlim[0], xMax =xlim[1],
                 yMin = band['highpass'],
                 yMax = band['lowpass'])
    pw2.setRange(yRange = (band['highpass'], band['lowpass']))
    # ------ Colorbar
    cm = pqg.colormap.get('CET-C2')
    make_colorBar(deg_lims, bearing_lab, img_bearing, pw2, cmap = cm)
    # ------ Link to PSD plot
    pw2.setXLink(pw1)
    pw2.setYLink(pw1)

    # -------------------------- verticle angle -------------------------------
    # --- Make image
    debug("Make vert angle panel")
    debug(f"v_angle.shape: {v_angle.shape}")
    img_vangle = dir_spectrogram_image(v_angle, offset, scale)
    pw3.addItem(img_vangle, row = 1, col = 0)
    pw3.setLimits(xMin = xlim[0], xMax =xlim[1],
                 yMin = band['highpass'],
                 yMax = band['lowpass'])
    pw3.setRange(yRange = (band['highpass'], band['lowpass']))
    # ------ Colorbar
    lims = (-90, 90)
    cm = pqg.colormap.get('CET-C2')
    make_colorBar(lims, vangle_lab, img_vangle, pw3, cmap = cm)
    # ------ Link to PSD plot
    pw3.setXLink(pw1)
    pw3.setYLink(pw1)

    # -------------------- eccentricity  --------------------------------------
    # --- make image
    debug("Make eccentricity panel")
    debug(f"eccen.shape: {eccen.shape}")
    img_eccen = dir_spectrogram_image(eccen, offset, scale)
    pw4.addItem(img_eccen, row = 1, col = 0)
    pw4.setLimits(xMin = xlim[0], xMax =xlim[1],
                 yMin = band['highpass'],
                 yMax = band['lowpass'])
    pw4.setRange(yRange = (band['highpass'], band['lowpass']))
    # ------ Colorbar
    make_colorBar(
        eccen_lims, e_lab, img_eccen, pw4,
        rounding = e_rounding)
    # ------ Link to PSD plot
    pw4.setXLink(pw1)
    pw4.setYLink(pw1)

    return layout

def binned_soundIntensity(d_res):

    layout = make_layout()

    pw = make_plotItem(legend = True, grid = True)
    layout.addItem(pw, row = 0, col = 0)
    # ------ Plot channels
    # Build plot objects
    pw.plot(d_res["time"],
            d_res["bearing"]*180/np.pi,
            name = "Horizontal Bearing",
            pen = pqg.mkPen(od.chan_color['x']))

    if d_res["verticle_angle"] is not None:
        pw.plot(d_res["time"],
                d_res["verticle_angle"]*180/np.pi,
                name = "Vertical Angle",
                pen = pqg.mkPen(od.chan_color['y']))

    # ------ Restrict range
    pw.setRange(yRange = (-180, 180), disableAutoRange = True)
    pw.setLimits(xMin = min(d_res["time"]),
                 xMax = max(d_res["time"]),
                 yMin = -180,
                 yMax = 180)

    ylab = 'Degrees'
    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    # ------ Plot pii
    pw = make_plotItem(grid = True)
    layout.addItem(pw, row = 1, col = 0)
    pw.plot(d_res["time"],
            d_res["pii"],
            pen = pqg.mkPen(od.chan_color['x']))

    # ------ Restrict range
    pw.setRange(yRange = (min(d_res["pii"]), max(d_res["pii"])),
                disableAutoRange = True)
    pw.setLimits(xMin = min(d_res["time"]),
                 xMax = max(d_res["time"]),
                 yMin = min(d_res["pii"]),
                 yMax = max(d_res["pii"]))
    ylab = '<math>L<sub>K</sub> / dB</math>'
    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    return layout

def binned_particleMotionVector(d_res):

    layout = make_layout()

    # ------ Plot bearings
    pw = make_plotItem(legend = True, grid = True)
    layout.addItem(pw, row = 0, col = 0)
    pw.plot(d_res["time"],
            d_res["bearing"]*180/np.pi,
            name = "Horizontal Bearing",
            pen = pqg.mkPen(od.chan_color['x']))

    if d_res["verticle_angle"] is not None:
        pw.plot(d_res["time"],
                d_res["verticle_angle"]*180/np.pi,
                name = "Vertical Angle",
                pen = pqg.mkPen(od.chan_color['y']))

    # ------ Restrict range
    pw.setRange(yRange = (-180, 180), disableAutoRange = True)
    pw.setLimits(xMin = min(d_res["time"]),
                 xMax = max(d_res["time"]),
                 yMin = -180,
                 yMax = 180)
    ylab = 'Degrees'
    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    # ------ Plot eccentricity
    pw = make_plotItem(grid = True)
    layout.addItem(pw, row = 1, col = 0)
    pw.plot(d_res["time"],
            d_res["eccentricity"],
            pen = pqg.mkPen(od.chan_color['x']))
    # ------ Restrict range
    pw.setRange(yRange = (0, 1), disableAutoRange = True)
    pw.setLimits(xMin = min(d_res["time"]),
                 xMax = max(d_res["time"]),
                 yMin = 0,
                 yMax = 1)
    ylab = 'Broadband Eccentricity (e)'
    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    return layout
