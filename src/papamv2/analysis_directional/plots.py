import numpy as np

from matplotlib.figure import Figure
from matplotlib import cm, colors
import colorcet as cc

from papamv2 import outputDictionaries as od

from papamv2.debug import debug

def dir_spectrogram(data_d, f, t, fs, rng, dims, band, unit_out):
    """
    to-do
    """
    # ------ Setup figure and axis
    fig = Figure()

    if unit_out == 'i':
        mag = data_d['si']
        mag_lab = '$L_{%s, f}$  (re $\mathrm{(1 %s )^2\cdot Hz^{-1})}$ / dB' % \
            (od.unit_tex['i'], od.ref_tex['i'])

        angle_lim = (-180, 180)
        bearing_lab = "SI Bearing (deg)"
        vangle_lab = "SI Vert. Angle (deg)"
    else:
        mag = data_d['psd']
        mag_lab = '$L_{%s, f}$  (re $\mathrm{(1 %s )^2\cdot Hz^{-1})}$ / dB' % \
            (od.unit_tex[unit_out], od.ref_tex[unit_out])
        angle_lim = (-90, 90)
        bearing_lab = "PM Bearing (deg)"
        vangle_lab = "PM Vert. Angle (deg)"

    ylim = (band['highpass'], band['lowpass'])

    debug(f"ylim {ylim}")

    # Get frequency indicies for bandpass values
    ylim_idx = (
        next((x for x in range(0,f.size) if f[x] >= ylim[0]), 0),
        next((x for x in range(0,f.size) if f[x] >= ylim[1]), f.size)
    )

    debug(f"ylim_idx {ylim_idx}")

    # ------ PSD
    ax = fig.add_subplot(221)
    # ------ pcolor mesh
    offset = rng.start/fs
    pmesh = ax.pcolormesh(t + offset, f, mag,
            cmap='viridis', shading='auto')
    pmesh.set_clim(
        np.min(mag[range(*ylim_idx),:]),
        np.max(mag[range(*ylim_idx),:])
    )
    # ------ Axis labels
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xlabel('Time (seconds)')
    ax.set_axisbelow(True)
    ax.set_ylim(band['highpass'], band['lowpass'])
    # ------ Colorbar
    clab = mag_lab
    fig.colorbar(pmesh, label=clab)

    # ------ Eccentricity
    ax = fig.add_subplot(223)
    # ------ pcolor mesh
    if unit_out == 'i':
        pii = data_d['pressure_intensity_index']
        pmesh = ax.pcolormesh(t + offset, f, pii,
                cmap='viridis', shading='auto')
        fig.colorbar(pmesh, label='$L_K$ / dB')
        pmesh.set_clim(
            np.min(pii[range(*ylim_idx),:]),
            np.max(pii[range(*ylim_idx),:])
        )
    else:
        eccentricity = data_d['eccentricity']
        pmesh = ax.pcolormesh(t + offset, f, eccentricity,
                cmap='viridis', shading='auto')
        pmesh.set_clim(0, 1)
        fig.colorbar(pmesh, label="Spectral Eccentricity ($e$)")
    # ------ Axis labels
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xlabel('Time (seconds)')
    ax.set_axisbelow(True)
    ax.set_ylim(*ylim)
    # ------ Colorbar

    # ------ Bearing
    ax = fig.add_subplot(222)
    # ------ pcolor mesh
    pmesh = ax.pcolormesh(t + offset, f, data_d['bearing'],
            cmap=cc.cm.colorwheel, shading='auto')
    pmesh.set_clim(*angle_lim)
    # ------ Axis labels
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xlabel('Time (seconds)')
    ax.set_axisbelow(True)
    ax.set_ylim(*ylim)
    # ------ Colorbar
    fig.colorbar(pmesh, label=bearing_lab)

    # ------ Vertical angle
    ax = fig.add_subplot(224)
    # ------ pcolor mesh
    pmesh = ax.pcolormesh(t + offset, f, data_d['vertical_angle'],
            cmap=cc.cm.colorwheel, shading='auto')
    pmesh.set_clim(-90, 90)
    # ------ Axis labels
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xlabel('Time (seconds)')
    ax.set_axisbelow(True)
    ax.set_ylim(*ylim)
    # ------ Colorbar
    fig.colorbar(pmesh, label=vangle_lab)

    return fig

def binned_soundIntensity(d_res):
    """
    to-do
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot(2,1,1)

    # ------ Plot waveform for each channel
    ax.plot(d_res["time"],
            d_res["bearing"]*180/np.pi,
            label = "Relative Bearing",
            color = od.chan_color['x'])
    if "z" in d_res.keys():
        ax.plot(d_res["time"],
                d_res["verticle_angle"]*180/np.pi,
                label = "Vertical Angle",
                color = od.chan_color['y'])
    ax.set_ylim((-180, 180))
    # Add axis labels
    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel('Degrees')
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    ax = fig.add_subplot(2,1,2)
    # ------ Plot waveform for each channel
    ax.plot(d_res["time"],
            d_res["pii"],
            color = od.chan_color['x'])
    # Add axis labels
    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel('$L_K$')
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)

    return fig

def binned_particleMotionVector(d_res):
    """
    to-do
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot(2,1,1)

    # ------ Plot bearings
    ax.plot(d_res["time"],
            d_res["bearing"]*180/np.pi,
            color = od.chan_color['x'],
            label = "Relative Bearing")
    if d_res['verticle_angle'] is not None:
        ax.plot(d_res["time"],
                d_res["verticle_angle"]*180/np.pi,
                label = "Vertical Angle",
                color = od.chan_color['y'])
    ax.set_ylim((-180, 180))

    # Add axis labels
    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel('Degrees')
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    # ------ Plot eccentricity
    ax2 = fig.add_subplot(2,1,2)
    ax2.plot(d_res["time"],
            d_res["eccentricity"],
            color = od.chan_color['x'])
    ax2.set_ylim((0, 1))

    # Add axis labels
    ax2.set_xlabel('Time (seconds)')
    ax2.set_ylabel('Broadband Eccentricity (e)')
    ax2.grid(b=True, which='major', alpha=0.5)
    ax2.set_axisbelow(True)

    return fig
