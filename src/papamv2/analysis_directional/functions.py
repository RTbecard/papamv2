"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Directional analysis functions
"""

import logging

import numpy as np
import scipy
from numpy import fft, angle, sqrt, arctan2, dot

from papamv2 import spectral_ellipses_c as sec
from papamv2 import propagation_axis as pa

from papamv2.debug import debug

def onesided_spectrum(x):
    """
    Given a windiwed signal, return a one-sided complex spectrum.
    """
    wl = len(x)
    n_freqs = int(np.floor(wl/2)) + 1

    wave_fft = fft.fft(x, norm="forward")
    # save one sided complex spectrum
    one_side = wave_fft[0:n_freqs]
    idx = range(1, int(np.ceil(wl/2)))

    # Take power from right side
    one_side[idx] = one_side[idx]*(2**0.5)

    return one_side

def freqs(fs, wl):
    """
    Calculate frequency values for a one-sided spectrum.
    """
    # --- Get frequency values
    df = fs/wl
    n_freqs = int(np.floor(wl/2)) + 1
    f = np.array(range(0, n_freqs))*df

    return f

def dirspec(vd, params, rng, unit_out):
    """
    """

    debug("calculating directional spectrogram")
    wl = int(params[0])
    olap = params[2]
    fs = vd.fs
    df = fs/wl

    # --- Get number of window segments
    step = wl - int(np.floor(wl*olap))
    n = len(rng)
    steps = int(( n - wl ) /  step)
    # --- Get frequency values
    n_freqs = int(np.floor(wl/2)) + 1
    f = freqs(fs, wl)
    #f = freqs(fs, wl)[1:-1] # remove 0 and DC freqs, these have no phase
    # --- set window function
    window = scipy.signal.windows.get_window(params[1], wl, False)
    window_corr = 1 / (np.mean(window**2)**0.5)

    debug("window correction factor: %f" % window_corr)

    # Make output array
    d_out = dict()

    # init holding arrays
    size = [n_freqs-2, steps]
    d_out['bearing'] = np.empty(size, dtype = np.double)
    d_out['vertical_angle'] = np.empty(size, dtype = np.double)

    debug("Extracting data")
    # ------ Extract signals
    if unit_out == 'i':
        subset_v = vd.subset('v', rng)
        subset_p = vd.subset('p', rng)
        d_out['si'] = np.empty(size, dtype = np.double)
        d_out['pressure_intensity_index'] = np.empty(size, dtype = np.double)
    else:
        subset_v = vd.subset(unit_out, rng)
        d_out['psd'] = np.empty(size, dtype = np.double)
        d_out['eccentricity'] = np.empty(size, dtype = np.double)

    chans_v = 'xyz' if 'z' in vd.channels else 'xy'

    # ------ Init container for holding complex spectrums
    complex_spectrums_v = np.empty([len(chans_v), n_freqs-2], dtype = np.cdouble)
    complex_spectrums_p = np.empty([1, n_freqs-2], dtype = np.cdouble)

    # ------ index for trimming DC and nyquest frequences
    idx_trim = range(1, int(np.ceil(wl/2)))
    # remove 0 and nyquest freqs, these have no phase and will cause errors

    debug("Looping bins")
    # ------ Loop time bins
    for t in range(0, steps):


        # ------ Define sample range of window segment
        win_rng = range(t*step, t*step + wl)

        # ------ Fill array of particle velocity complex spectrums
        for i, chan in enumerate(chans_v):
            x = subset_v[chan][win_rng] * window * window_corr
            complex_spectrums_v[i] = onesided_spectrum(x)[idx_trim].copy()
        # indexing is to trim DC and nyquest (they have 0 phase and cause
        # division errors)

        if unit_out == 'i':
            # ------ Sound intensity
            x = subset_p['p'][win_rng]*window
            complex_spectrums_p[0] = onesided_spectrum(x)[1:-1].copy()
            mag, bearing, vert_angle, pii = \
                sec.localize_si(complex_spectrums_v, complex_spectrums_p)

            d_out['si'][:, t] = 10*np.log10(mag[:]/df)
            d_out['bearing'][:, t] = bearing*180/np.pi
            d_out['vertical_angle'][:, t] = vert_angle*180/np.pi
            d_out['pressure_intensity_index'][:, t] = pii
        else:
            # ------ Particle motion
            mag, bearing, vert_angle, eccentricity = \
                sec.localize_pm(complex_spectrums_v)

            d_out['psd'][:, t] = 10*np.log10((mag[:]**2)/df)
            d_out['bearing'][:, t] = bearing*180/np.pi
            d_out['vertical_angle'][:, t] = vert_angle*180/np.pi
            d_out['eccentricity'][:, t] = eccentricity

    debug("Settings times")
    times = np.array(range(0, steps), dtype = float)*step/fs

    # Add time offset
    offset = rng.start/vd.fs
    times += offset

    return d_out, f[idx_trim], times

def binned_soundIntensity(vd, rng, bin_size_n, olap_n):

    # Get velocity and intensity vectors
    subset_p = vd.subset('p', rng)

    # convert velocity to intensity
    subset_si = vd.subset('v', rng)
    subset_si.data *= subset_p.data * 1e3

    d_res = dict()
    # bin indexes in subsets
    starts = np.arange(start = 0,
                       stop = subset_p.n - bin_size_n,
                       step = bin_size_n - olap_n)
    debug("starts: %s" % starts)
    debug("subset_si.data: \n %s" % subset_si.data)

    # ------ Calculate mean intensity values
    for chan_idx, chan in enumerate(subset_si.channels):
        d_res[chan] = np.ndarray((len(starts)), dtype = float)
        # Loop bins
        for idx, start in enumerate(starts):
            # Get indicies for subset
            samples = range(start, (start + bin_size_n))
            # Calc mean value
            d_res[chan][idx] = np.mean(subset_si.data[chan_idx, samples])
    debug("d_res: \n %s" % d_res)

    # ------ Save times
    d_res['time'] = (starts + subset_si.rng.start) / subset_si.fs

    # ------ Mean square pressure pressure
    d_res['p_ms'] = np.mean(subset_p.data**2)

    # ------ Mean SI vector magnitude
    d_res["si"] = np.zeros(d_res[subset_si.channels[0]].shape, dtype = float)
    for chan in subset_si.channels:
        d_res["si"] += d_res[chan]**2
    d_res["si"] = d_res["si"]**0.5

    # ------ pressure intensity index
    d_res["pii"] = 10*np.log10(d_res['p_ms'] / d_res['si'])

    # Calculate horizontal bearing
    if (all( [ chan in d_res.keys() for chan in ["x", "y"] ] )):
        d_res["bearing"] = -np.arctan2(d_res['y'], d_res['x'])
    else:
        d_res["bearing"]  = None

    # Calculate vertical angle
    if (all( [ chan in d_res.keys() for chan in ["x", "y", "z"] ] )):
        d_res["verticle_angle"] = np.arctan2(
            d_res["z"], np.sqrt(d_res["x"]**2 + d_res["y"]**2))
    else:
        d_res["verticle_angle"] = None

    return(d_res)

def binned_particleMotionVector(subset, bin_size_n, olap_n):

    # bin indexes in subsets
    starts = np.arange(start = 0,
                       stop = subset.n - bin_size_n,
                       step = bin_size_n - olap_n)

    d_res = dict(
        magnitude = np.ndarray((len(starts),), dtype = float),
        bearing = np.ndarray((len(starts),), dtype = float),
        eccentricity = np.ndarray((len(starts),), dtype = float))
    if subset.nchannels == 3:
        d_res['verticle_angle'] = np.ndarray((len(starts),), dtype = float)

    # ------ loop bins
    for idx, start in enumerate(starts):

        samples = range(start, (start + bin_size_n))
        prop_vec, e = pa.getPVAxisArray(subset.data[:, samples])
        bearing, v_angle = pa.vec2rad(prop_vec)

        debug("v_angle: %s" % v_angle)

        d_res['magnitude'][idx] = sqrt((prop_vec**2).sum())
        d_res['bearing'][idx] = bearing
        d_res['eccentricity'][idx] = e
        if subset.nchannels == 3:
            d_res['verticle_angle'][idx] = v_angle

    d_res['time'] = (starts + subset.rng.start) / subset.fs

    return(d_res)


