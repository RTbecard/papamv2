"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Holds code for calculating directional spectrogram
"""
import logging

import numpy as np

from papamv2.analysis_directional import plots_pyqtgraph as apg
from papamv2.analysis_directional import plots as ap
from papamv2.analysis_directional import functions as af
from papamv2.analysis_directional import write as aw
from papamv2 import outputDictionaries as od


from papamv2.debug import debug

# =========================== Internal Functions ==============================


def dirspec(vectorData, params, Rpt, rng, PamPlot, unit_out):
    """
    Print and save directional spectrogram
    """

    if not params: return

    Rpt.pamPrint(f"------ Generating {od.unit_label[unit_out]} directional spectrogram")

    # count particle mopiton axes
    dims = sum(pat in vectorData.channels for pat in 'xjz')

    if dims < 2:
        txt  = "Skipping.  Require at least x and y particle motion channels."
        Rpt.pamPrint(txt)
        return

    # ------ Run analysis
    data, f, t = af.dirspec(vectorData, params, rng, unit_out)

    # ------ Write to file
    debug("Writing to file")
    aw.write_directional_spectrogram(Rpt, data, f ,t, unit_out)

    debug(f"data: {data}")

    band = [ vectorData.band['highpass'], vectorData.band['lowpass'] ]


    # ------ Show plots
    debug("Showing plots")
    tab_name = f'DSpec-{od.unit_tab[unit_out]}'
    window_len = params[0]
    olap = params[2]
    args = [data, f, t, vectorData.fs, rng, window_len, olap, dims, vectorData.band, unit_out]
    PamPlot.plot(tab_name, apg.dir_spectrogram, args)

    # ------ Write plot to file
    debug("Writing plots to file")
    filename = 'directional-spectrogram_rangeID-%s_unit-%s' % (Rpt.rng_id, unit_out)
    args = [data, f, t, vectorData.fs, rng, dims, vectorData.band, unit_out]
    Rpt.printPlots(filename, ap.dir_spectrogram, args)

def binned_soundIntensity(vectorData, binned_params, Rpt, rng, PamPlot):
    """
    Print and saved binned sound intensity vector
    """

    bin_size_n = int(binned_params[0] * vectorData.fs)
    olap_n = int(np.floor(binned_params[1] * bin_size_n))

    debug("bin_size_n %s" % bin_size_n)
    debug("olap_n %s" % olap_n)

    # ------ Run analysis
    Rpt.pamPrint("------ Running binned sound intensity...")
    d_res = af.binned_soundIntensity(vectorData, rng, bin_size_n, olap_n)
    debug("binned sound intensity: \n %s" % d_res)

    # ------ Write results
    aw.write_binned_SoundIntensity(Rpt, d_res)

    # ------ Write plots
    filename = 'binnedSI_band-%s' % (Rpt.band)
    Rpt.printPlots(filename, ap.binned_soundIntensity, [d_res])

    # ------ Show plots
    tab_name = 'Binned SI'
    PamPlot.plot(tab_name, apg.binned_soundIntensity, [d_res])

def binned_particleMotionVector(subset, binned_params, Rpt, rng, PamPlot, unit_out):
    bin_size_n = int(binned_params[0] * subset.fs)
    olap_n = int(np.floor(binned_params[1] * bin_size_n))

    debug("bin_size_n %s" % bin_size_n)
    debug("olap_n %s" % olap_n)

    # ------ Run analysis
    Rpt.pamPrint("------ Running binned particle motion vector...")
    d_res = af.binned_particleMotionVector(subset, bin_size_n, olap_n)
    debug("binned pm vec: \n %s" % d_res)

    # ------ Write results
    aw.write_binned_particleMotionVector(Rpt, d_res)

    # ------ Write plots
    filename = 'binned-PMVec_unit-%s_band-%s' % (Rpt.unit_out, Rpt.band)
    Rpt.printPlots(filename, ap.binned_particleMotionVector, [d_res])

    # ------ Show plots
    tab_name = 'Binned PM Vector'
    PamPlot.plot(tab_name, apg.binned_particleMotionVector, [d_res])

