import os

import numpy as np
import pandas as pd

from papamv2.debug import debug

def write_directional_spectrogram(Rpt, data, f, t, unit_out):
    # ------ Save resuts to file
    if Rpt.no_output: return

    # ------ name
    filename = '%s_directional-spectrogram_band-%s_rangeID-%s_fs-%s.csv' % \
        (Rpt.filestamp, Rpt.band, Rpt.rng_id, Rpt.fs)

    left = np.array([(row, col) for row in f for col in t ])

    if unit_out == 'i':
        filename = '%s_directional-spectrogram_i_band-%s_rangeID-%s_fs-%s.csv' % \
            (Rpt.filestamp, Rpt.band, Rpt.rng_id, Rpt.fs)

        out = np.vstack((left[:, 0], left[:, 1],
                         data['si'].flatten(),
                         data['bearing'].flatten(),
                         data['vertical_angle'].flatten(),
                         data['pressure_intensity_index'].flatten())).T
    else:
        filename = '%s_directional-spectrogram_%s_band-%s_rangeID-%s_fs-%s.csv' % \
            (Rpt.filestamp, unit_out, Rpt.band, Rpt.rng_id, Rpt.fs)

        out = np.vstack((left[:, 0], left[:, 1],
                         data['psd'].flatten(),
                         data['bearing'].flatten(),
                         data['vertical_angle'].flatten(),
                         data['eccentricity'].flatten())).T

    with open(os.path.join(Rpt.output_data, filename), 'w') as fid:
        # Write header
        if unit_out == 'i':
            fid.write(",".join([
                'Frequency','Time','SI','Relative_bearing',
                'Vertical_angle','Pressure_intensity_index\n']))
        else:
            fid.write(",".join([
                'Frequency','Time','PSD','Relative_bearing',
                'Vertical_angle','Spectral_Eccentricity\n']))

        for r in range(0, out.shape[0]):
            # convert row to comma seperated text
            txt = ",".join(list(map(str, out[r,:].tolist())))
            # write to file
            fid.write(txt + "\n")

def write_binned_SoundIntensity(Rpt, d_res):

    if Rpt.no_output: return

    # ========================= write bearings ================================
    filename = '%s_binned-soundIntensity_band-%s_rangeID-%s_fs-%s.csv' % \
        (Rpt.filestamp, Rpt.band, Rpt.rng_id, Rpt.fs)
    filepath = os.path.join(Rpt.output_data, filename)

    df = pd.DataFrame(data = d_res)
    df.rename(
        {"p_ms": "Mean_square_pressure",
         "si": "Mean_sound_intensity_vector_magnitude",
         "pii": "Pressure_intensity_index",
         "bearing":"Relative_bearing",
         "verticle_angle": "Vertical_angle"},
        inplace = True, axis = 'columns')

    df.to_csv(filepath, index = False)

def write_binned_particleMotionVector(Rpt, d_res):

    if Rpt.no_output: return

    # ========================= write bearings ================================
    filename = '%s_binned-particleMotionDirection_unit-%s_band-%s_rangeID-%s_fs-%s.csv' % \
        (Rpt.filestamp, Rpt.unit_out, Rpt.band, Rpt.rng_id, Rpt.fs)
    filepath = os.path.join(Rpt.output_data, filename)

    df = pd.DataFrame(data = d_res)

    df.rename(
        {'magnitude': 'Magnitude',
         'bearing': 'Relative_bearing',
         'verticle_angle': 'Vertical_angle',
         'eccentricity': 'Spectral_eccentricity'},
        inplace = True, axis = 'columns')

    df.to_csv(filepath, index = False)
