"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions related to calculating and plotting butterworth bandpass filters.
"""
# Supress math warnings (overflow on bandpass plotting)
import warnings
import numpy as np

from matplotlib.figure import Figure
import pyqtgraph as pqg
from PyQt5 import QtCore

from papamv2.analysis import plots_pyqtgraph

from papamv2.debug import *

def getBandFilters(lower, upper, order, fs, decidecade_order):
    """
    Define butterworth (bandpass) filter coefficients.

    If decidecade feature is selected, generate decidecade bands centered
    around 100Hz which encompass the analysis bandwidth.  If there is a user
    specified bandpass filter, decidecade bands will be generated within that
    bandpass range.

    Parameters
    ----------
    lower: float
        Lower frequency for user specified bandpass.
    upper: float
        Upper frequency for user specified bandpass.
    order: int
        Butterworth order for user specified bandpass.
    fs: int
        Sample rate of signal to bandpass.
    decidecade_order: int
        Whena value greater than 0 is provided, coefficients for decidecade
        bands will be generetaed.

    Return
    ------
    ret: [(hp, lp, n, band_label), ...]
        List of tuples containing the bandpass range, hp and lp, filter order,
        n, and the string holding the band label.
    """
    # ---- Set bands to analyze
    # Add full band to list

    # ------------------------- Calc passbands --------------------------------
    if order > 0:
        # If order is set, apply bandpass
        label = str(round(lower)) + "-" + str(round(upper)) + "Hz"
    else:
        lower = 0
        upper = fs/2
        order = 0
        label = "full-band"
    ret = [(lower, upper, order, label)]

    # ---- Add decidecades
    # Highest band should include upper limit of bandpass
    if decidecade_order > 0:

        # Set 10Hz as lowest frequency
        if lower == 0:
            lower = 10

        # Calculate range of decidecade bands to analyze (starting from 1Hz)
        band_lower = int(np.ceil(10*np.log10(lower) + 0.5))
        band_upper = int(np.floor(10*np.log10(upper) - 0.5))

        for band in range(band_lower, band_upper + 1):
            # calc upper and lower freqs of band
            freq_lower = 10**((band - 0.5)/10)
            freq_mid = 10**(band/10)
            freq_upper = 10**((band + 0.5)/10)

            ret = ret + [(freq_lower, freq_upper, decidecade_order,
                          "decidecade-" + str(round(freq_mid)) + "Hz")]

    return ret

def butter_response(low, high, order, fs, min_dB):

    # Generate frequencies
    freq = np.arange(1, int(fs/2))

    # Make flat response
    y = np.ones(freq.shape)

    # apply highpass
    y *= 1 / (1 + (freq / high)**(2*order) )
    y *= 1 / (1 + (low / freq)**(2*order) )

    y = 10*np.log10(y)*2

    # index points above min_dB
    idx = y >= min_dB

    return freq[idx].copy(), y[idx].copy()

def plot_butter_pyqt(bands, fs, min_dB = -24):

    pw = plots_pyqtgraph.make_plot(legend = False, grid = True)

    pen = pqg.mkPen(size = 2, style=QtCore.Qt.DashLine, color = (0,128,255))

    for band in bands:

        x, y = butter_response(band[0], band[1], band[2], fs, min_dB)
        pw.plot(x, y, antialias = True, pen = pen)

    plots_pyqtgraph.setLabel(pw, 'bottom','Frequency (Hz)')
    plots_pyqtgraph.setLabel(pw, 'left', "Gain (dB)")

    pw.setLogMode(True, False)

    return pw

def plot_butter(bands, fs, min_dB = -24):

    fig = Figure()
    ax = fig.add_subplot(111)

    for band in bands:
        x, y = butter_response(band[0], band[1], band[2], fs, min_dB)
        ax.fill_between(x, y, min_dB)

    ax.ylabel = "Gain (dB)"
    ax.xlabel = 'Frequency (Hz)'
    ax.set_xscale("log")

    return fig
