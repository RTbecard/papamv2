"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------
"""

# Manipulating file paths
import os
# CLI argument parsing
import sys
import argparse

# Modules holding paPAM subroutines
from papamv2 import optionParsing
from papamv2 import run

from papamv2.gui import run as grun

# ================================ Main Function ==============================
def main():
    """
    This is the command line interface for paPAMv2

    Arguments will be parsed here and then passed to the function papam, which
    will run the specified analysis features.
    """

    # ========================= Parse CLI arguments ===========================
    use = """
    CLI version:
        paPAMv2 run <channels> <wav> [<wav> ...] [options]"
        paPAMv2 detect <channels> <wav> [<wav> ...] [options]"

    GUI version:
        papamv2

    Run 'paPAMv2 run -h' or 'paPAMv2 detect -h' for detailed guidelines for use.
    """

    if len(sys.argv) == 1:
        # ======================== Launch GUI mode ============================
        print("No CLI arguments detected.  Launching GUI...")
        grun.run()
    elif sys.argv[1] == 'run':

        # ------ Init argument parser
        parser = argparse.ArgumentParser(usage=use, add_help=False)
        # Add help file
        dir_app = os.path.dirname(os.path.abspath(__file__))
        # Load help txt
        with open(os.path.join(dir_app, 'help_run.txt'), 'r') as fid:
            help_txt = fid.readlines()
        parser.add_argument('-h', '--help', action='help',
                            default=argparse.SUPPRESS,
                            help=help_txt)

        # ------ File input (manditory positional arguments)
        # All arguments will be stored as strings.  The class AnalysisFeatures
        # will be used to check the values provided and convert to the proper
        # class.
        # ------ Input files
        parser.add_argument('channels', type=str)
        parser.add_argument('wav_files', type=str, nargs='+')
        # ------ Analysis type
        parser.add_argument('--subsets', type=str)
        parser.add_argument('--sequential-subsets', type=str)
        # ------ Calibration
        parser.add_argument('--calibration-file', type=str)
        parser.add_argument('--calibration-wl', type=str)
        parser.add_argument('--recorder-gain', type=str)
        # ------ Global options
        parser.add_argument('--bandpass', type=str)
        parser.add_argument('--decidecade', type=str)
        parser.add_argument('--units-out', type=str)
        parser.add_argument('--propagation-axis', action='store_true')
        parser.add_argument('--use-sound-intensity', action='store_true')
        parser.add_argument('--rho', type=str)
        parser.add_argument('--c', type=str)
        # ------ Analysis Features
        parser.add_argument('--waveform', action='store_true')
        parser.add_argument('--analytic-envelope', action='store_true')
        parser.add_argument('--spectrogram', type=str)
        parser.add_argument('--directogram', type=str)
        # -- Transient Features
        parser.add_argument('--impulse-rms-level', action='store_true')
        parser.add_argument('--impulse-kurtosis', action='store_true')
        parser.add_argument('--exposure-level', action='store_true')
        parser.add_argument('--zero-to-peak', action='store_true')
        parser.add_argument('--crest-factor', type=str)
        parser.add_argument('--rise-time', type=str)
        parser.add_argument('--pulse-length', type=str)
        parser.add_argument('--esd', type=str)
        parser.add_argument('--decidecade-exposure', action="store_true")
        # -- Continuous Features
        parser.add_argument('--rms-level', action='store_true')
        parser.add_argument('--kurtosis', action='store_true')
        parser.add_argument('--psd', type=str)
        parser.add_argument('--decidecade-rms', action="store_true")
        # ------ Output
        parser.add_argument('--output-dir', type=str)
        parser.add_argument('--output-name', type=str)
        parser.add_argument('--no-output', action='store_true')
        # ======================== Parse arguments ============================
        # ---- Setup analysis features
        args = parser.parse_args()
        features = optionParsing.parse(vars(args))
        # ---- Run Analysis
        # ======================== Run Analysis ===============================
        run.run(features)
    elif sys.argv[1] == 'detect':

        # ------ Init argument parser
        parser = argparse.ArgumentParser(usage=use, add_help=False)
        # Add help file
        dir_app = os.path.dirname(os.path.abspath(__file__))
        # Load help txt
        with open(os.path.join(dir_app, 'help_run.txt'), 'r') as fid:
            help_txt = fid.readlines()
        parser.add_argument('-h', '--help', action='help',
                            default=argparse.SUPPRESS,
                            help=help_txt)
        # ------ Event Detection
        parser.add_argument("--detector-resolution", type=str)
        parser.add_argument('--detector-bandpass', type=str)
        parser.add_argument('--threshold-relative', type=str)
        parser.add_argument('--threshold-absolute', type=str)
        parser.add_argument('--threshold-units', type=str)
        parser.add_argument('--threshold-type', type=str)
        parser.add_argument('--event-min-duration', type=str)
        parser.add_argument('--event-min-spacing', type=str)
        parser.add_argument('--event-padding', type=str)
    else:
        RuntimeError("Invalid mode selected.  See 'paPAMv2 run -h' or 'paPAMv2 detect -h' .")

if __name__ == '__main__':
    main()
