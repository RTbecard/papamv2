"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Dictionaries holding labels and units for plotting and console output.
"""

# =========================== Label dictionaries ==============================
# These hold the label values for each channel for the console and plot
# outputs
unit_ascii = dict(d='delta',
        v='u',
        a='a',
        p='p',
        i='I',
        e='(p/rho*c)')

unit_html = dict(d='&delta;',
        v='u',
        a='a',
        p='p',
        i='I',
        e='(p/&rho;c)')

unit_tex = dict(d='\delta',
        v='u',
        a='a',
        p='p',
        i='I',
        e='(p/\\rho\\cdot c)')

ref_tex = dict(d='pm',
        v='nm \\cdot s^{-1}',
        a='\\mu m \\cdot s^{-2}',
        p='\\mu Pa',
        i='pW \\cdot m^{-2}',
        e='nPa \\cdot \\rho ^{-1} \\cdot c^{-1}')

ref_ascii = dict(d='pm',
        v='nm/s',
        a='um/s^2',
        p='uPa',
        i='pW/m^2',
        e='nm/s^2')

ref_html = dict(d='pm',
        v='nm/s',
        a='&mu;m/s<sup>2</sup>',
        p='&mu;Pa',
        i='pW/m<sup>2</sup>',
        e='nm/s')

unit_label = dict(d='particle-displacement',
        v='particle-velocity',
        a='particle-acceleration',
        p='sound-pressure',
        i='sound-intensity',
        e='estimated-particle-velocity')

unit_tab = dict(d='disp',
        v='velc',
        a='accl',
        p='pres',
        e='eVel',
        i='si')

chan_tab = dict(x='-x',
        y='-y',
        z='-z',
        xy='-xy',
        xyz='-xyz',
        p='-omni',
        i='-si',
        m='-pm')

chan_ascii = dict(
        x='x',
        y='y',
        z='z',
        xy='xy',
        xyz='xyz',
        p='omni',
        i='si',
        m='pm')

chan_color = dict(
        x='#1f77b4', # blue
        y='#d62728', # red
        z='#2ca02c', # green
        xy='#9467bd',# purple
        xyz='#7f7f7f',# Grey
        p='#bcbd22', # turqouise
        i='#17becf', # baby blue
        m='#808000') # olive
# Colors taken from matplotlib's tab10
# https://github.com/matplotlib/matplotlib/blob/7cc785890df60fd6a9010adf838cfee5492b53c8/lib/matplotlib/_cm.py#L1280
