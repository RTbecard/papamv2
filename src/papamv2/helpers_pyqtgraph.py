"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Helper functions for plotting in pyqtgraph
"""


from sys import platform
import pyqtgraph as pqg
import numpy as np

from papamv2.debug import debug

# Antialiasing on all plots
pqg.setConfigOptions(antialias=True)

# color ddefinitions
grey1 = '#333'
tomato = '#ff6347'

# set platform specific label styles
if platform == 'darwin':
    labelStyle = {'font-size': '14pt'}
elif platform == 'win32':
    labelStyle = {'font-size': '10pt'}
else:
    labelStyle = {'font-size': '10pt'}


def make_plot(legend = False, grid = False):
    """
    Make plot widget to draw on
    """
    # ------ Setup figure and axis
    pw = pqg.PlotWidget()
    pw.setBackground('w')

    # Disable mouse context window
    pw.setMenuEnabled(True)
    # pw.autoRange(padding = 0)
    pw.setAutoVisible(x = True, y = True)
    # Disable mouse interactivity
    pw.setMouseEnabled(x = True, y = False)
    # Remove axis view padding
    pw.setDefaultPadding(0.0)
    # Add legend (will fill automatically)
    if legend:
        leg = pw.addLegend(labelTextColor = grey1)
        leg.setBrush('w')
        leg.setPen('grey')

    if grid:
        pw.showGrid(x = True, y = True, alpha = 1)

    return pw

def make_plotItem(legend = False, grid = False):
    """
    Make plot widget to draw on
    """
    # ------ Setup figure and axis
    pw = pqg.PlotItem()

    # Disable mouse context window
    pw.setMenuEnabled(True)
    # pw.autoRange(padding = 0)
    pw.setAutoVisible(x = True, y = True)
    # Disable mouse itneractivity
    pw.setMouseEnabled(x = True, y = False)
    # Remove axis view padding
    pw.setDefaultPadding(0.0)
    # Add legend (will fill automatically)
    if legend:
        leg = pw.addLegend(labelTextColor = grey1)
        leg.setBrush('w')
        leg.setPen('grey')

    if grid:
        pw.showGrid(x = True, y = True, alpha = 1)

    return pw

def make_layout():

    pw = pqg.GraphicsLayoutWidget()
    pw.setBackground('w')

    return pw

def make_colorBar(lims, clab, img, pw, rounding = 1, cmap = None, values = None, reverse = False):

    if cmap is None:
        cmap = pqg.colormap.get('viridis')

    if reverse:
        cmap.reverse()

    if values is None:
        values = lims

    cb = pqg.ColorBarItem(values = values, limits = lims, colorMap = cmap,
                          rounding = rounding)
    cb.setLabel('left', clab, color = grey1)
    cb.setImageItem(img, insert_in=pw)

def setLabel(pw, position, name):

    pw.setLabel(position, name, color = grey1, **labelStyle)

def rangify(pw, x, y, color=None, alpha=None, label=None):
    """
    If arrays (assumed to be 1d) have more than 3000 samples, convert into
    binned ranges.  This is for more legible (and faster) plotting.

    Use this function to build plots for analysis features which return
    time-series data.

    Parameters
    ----------
    ax: axis
        Axis to plot rangify on.
    x: numpy.array
        x axis data to plot.
    y: numpy.array
        y axis data to plot.
    color: str
        Color to plot.
    alpha: float
        Alpha value for ribbon, between 0 and 1.  Does not apply to line
        plots (i.e. if too few samples to make ribbon plot).
    fcn: function
        When set, this will be used to calculate a single return value per bin.

    Return
    ------
    A list holding the plotting actions to take, formatted for input in
    self.plot().
    """

    bins = 500

    if color is None:
        color = '#1f77b4'

    if alpha is None:
        alpha = 1

    # Add alpha to color value
    col = color + hex(int(alpha*255))[2:]
    #col = color
    # col = '#1f77b4'
    debug(f'color: {col}')
    # Must have at least 3 values per bin, otherwise, return normal plot
    if x.size < bins*10:
        pw.plot(x, y, pen = col)
    else:
        # calc bin indicies
        idx = np.linspace(0, x.size - 1, num=bins, endpoint=False)
        idx_left = np.floor(idx).astype('i8')
        idx_right = np.ceil(idx + (x.size/bins)).astype('i8')

        # Default action, return fill_between plot (range of values in bin)
        upper = np.empty((bins))
        lower = np.empty((bins))

        debug(f"upper size: {upper.size}")
        debug(f"lower size: {lower.size}")
        debug(f"idx_left range: {idx_left[[0,-1]]}")
        debug(f"idx_right range: {idx_right[[0,-1]]}")

        # Bin values across x axis
        for i in range(0, bins):
            idx_rng = range(int(idx_left[i]), int(idx_right[i]))
            upper[i] = max(y[idx_rng])
            lower[i] = min(y[idx_rng])

        x_bins = np.append(x[idx_left], x[idx_right[-1] - 1])

        kwargs = {'stepMode': 'center', 'skipFiniteCheck': True}
        pd_upper = pqg.PlotDataItem(x_bins, upper, **kwargs)
        pd_lower = pqg.PlotDataItem(x_bins, lower, **kwargs)
        fb = pqg.FillBetweenItem(pd_upper, pd_lower, pen=None, brush=col)

        # Add plot
        pw.addItem(fb)

        # Add legend
        if label is not None:
            pw.addLegend().addItem(
                pqg.PlotDataItem([], pen = pqg.mkPen(col, width = 3)),
                name = label)

