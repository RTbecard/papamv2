"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

paPAMv2
-------
Calibration functions
"""

# Arrays
import numpy as np
from papamv2.calibration import impulse
from papamv2.debug import *

class Calibration:

    file = None
    fs = None
    unit_in = None
    taps = None

    # These hold numpy arrays, with columns representing frequenry and value.
    # Magnitudes are in dB units, phase in radians
    spectrum_magnitude = { 'x': None, 'y': None, 'z': None, 'p': None}
    spectrum_phase = { 'x': None, 'y': None, 'z': None, 'p': None}

    # Frequency invariant calibration values for magnitude (i.e. receiver gain)
    scale_magnitude = dict(
        x = 1,
        y = 1,
        z = 1,
        p = 1)

    impulse_magnitude = {'p': {}, 'x': {}, 'y': {}, 'z': {} }
    impulse_phase = {'p': {}, 'x': {}, 'y': {}, 'z': {} }

    def __init__(self, fs):
        """
        Initialize a Calibration class object.
        """
        self.fs = fs

    def read(self, file_mag, file_phase, recorder_gain, unit_in):
        """
        Read receiver sensitivity values from csv file.

        Parameters
        ----------
        file_mag: str
            Path to the csv file holding the calibration data.  This expects a 8
            column csv file where each channel (x, y, z, p) is represented by a
            pair of columns holding the frequency, in Hertz, and reciever
            sensitivity (dB re V/unit).  Pressure and particle motion values are
            expected to be micro Pascals and nano meters per second, respectively.
            Channels can have different calibration resultions (i.e. number of
            rows).
        file_phase: str
            Path to the csv file holding the phase calibration data.  This expects a 8
            column csv file where each channel (x, y, z, p) is represented by a
            pair of columns holding the frequency, in Hertz, and phase offset.
        recorder_gain: (float, float, float, float)
            Gain to be applied to each channel of calibration file.
        unit_in: str
            Single character indication the units of the particle motion receiver
            sensitivity values.  'a' or 'v' for dB re 1um/s^{-2} or re 1nm/s^{-1},
            respectively.
        """

        self.unit_in = unit_in

        # ====== Nested function for reading csv rows
        def read_csv(file, cols):
            debug(f"reading: {file}")
            args = {'fname': file,
                    'dtype': np.dtype('float'),
                    'delimiter': ',',
                    'missing_values': '',
                    'autostrip': True,
                    'invalid_raise': False}
            # read column into np array
            data = np.genfromtxt(usecols=cols, **args)
            debug(f"data: {data}")
            # Only 1 row
            if len(data.shape) == 1:
                raise RuntimeError(
                    f"Bad calibration data.  Problem reading columns {cols}")
            # Remove NAN rows
            data = data[~np.isnan(data).any(axis=1)]
            debug(f"data, cleaned: {data}")

            if data.shape[0] == 0 or data.size == 0:
                return None

            return data

        debug(f"file_phase: {file_phase}")
        debug(f"file_mag: {file_mag}")

        # ============ Magnitude constants
        for i, k in enumerate(self.scale_magnitude.keys()):
            # Convert from dB to linear units
            self.scale_magnitude[k] = 10**(-recorder_gain[i]/20)

        # ============ Magnitude response
        if file_mag is None or file_mag == "":
            # No frequency calibration
            self.spectrum_magnitude = { 'x': None, 'y': None, 'z': None, 'p': None}
        else:
            # ---- Load calibration file
            for i, k in enumerate(['x', 'y', 'z', 'p']):
                cols = (i*2, i*2 + 1)
                self.spectrum_magnitude[k] = read_csv(file_mag, cols)

        # ============ Phase response
        if file_phase is None or file_phase == "":
            # No frequency calibration
            self.spectrum_phase = { 'x': None, 'y': None, 'z': None, 'p': None}
        else:
            # ---- Load calibration file
            for i, k in enumerate(['x', 'y', 'z', 'p']):
                cols = (i*2, i*2 + 1)
                self.spectrum_phase[k] = read_csv(file_phase, cols)

        debug(f"spectrum_magnitude {self.spectrum_magnitude}")
        debug(f"spectrum_phase {self.spectrum_phase}")
        debug(f"scale_magnitude {self.scale_magnitude}")

    def createCalibrationImpulses(self, unit_out):
        """
        Calculate calibration filter from calibration csv.

        Returns impulse responses to be convolved with uncalibrated signal.
        Calibration impulse (i.e. taps) is generated using the windowed
        frequency sampling method with Hanning window.  To use these values, with
        filter functions, taps represents the zero coefficents (b=taps) and set
        the pole coefficients to 1 (a=1).

        The impulse response is tailored for a double filter (filtfilt), so the
        magnitudes have had 6dB subtracted to account for the double application.

        Resulting filters will be automatically set to have a resolution of 1Hz
        with a hamming window applied to them to smooth out ripple.

        Parameters
        ----------
        f_response: dict
            End-to-end magnitude calibration values returned by the function
            readCalibValues.
        unit_out: str
            Single character indicating what particle motion unit should be
            returned by the calibration: 'd', 'v', or 'a' for particle
            displacement, velocity, and acceleration, respectively.
        fs: int
            Sample rate of signal to calibrate.

        Return
        ------
        taps: dict(channel: np.array(), ...)
            A dictionary holding the calibration impulse response for each
            channel.
        """

        debug(" ------ CreateCalibrationImpulses")

        # ---- Sound intensity and predicted velocity
        # default to velocity in special cases

        # ---- Linear interplolate calibration values
        # This is "choosing the frequencies we will sample" for the calibration.
        # Use a multiple of the samplerate to keep it consistent across devices.
        # Also, choosing too high a value will make it hard to analyze smaller
        # signals

        # The default value here (fs/10) will result in a minimum signal length of
        # 0.1 seconds.

        # Freqs to interp
        # We evenly spaced calibration values to make our impulse reponse
        # omega_interp[0] = omega_interp[1]
        # 0th index must not be 0, this will mess up interpolation.

        self.taps = int(self.fs/10)
        debug(f'taps: {self.taps}')

        # subset channels for unit types
        if unit_out == 'p':
            chans = ['p']
        else:
            chans = ['x', 'y', 'z']

        for k in chans:

            self.impulse_magnitude[k][unit_out] = \
                self.createImpulseMagnitude(unit_out, k)

            self.impulse_phase[k][unit_out] = \
                self.createImpulsePhase(unit_out, k)

    def createImpulseMagnitude(self, unit_out, chan):
        """
        Create a magnitude calibration impulse response.
        """
        debug(" ------ CreateImpulseMagnitude")

        # Get frequencies for impulse spectrum fft
        freq_interp = impulse.freqs(self.fs, self.taps)

        if self.spectrum_magnitude[chan] is None:
            if unit_out == self.unit_in or chan == 'p':
                # ====== No spectral calibration required
                return None
            # ====== Make flat spectrum to apply unit changes to
            # fill magnitude spectum with zero dB
            mag = np.ones([len(freq_interp)])
            freq = freq_interp
            y = np.zeros((freq_interp.size), dtype = complex)
        else:
            # Get file spectrum in linear units
            # Note the negative sign here
            mag = 10**(-self.spectrum_magnitude[chan][:, 1]/20)
            freq = self.spectrum_magnitude[chan][:, 0]

            debug(f"freq: {freq}")
            debug(f"mag: {mag}")

        # ====== Scaling for particle motion output units
        omega = freq*2*np.pi
        if omega[0] == 0:
            omega[0] = omega[1]  # prevent divide by 0 errors
        if unit_out == 'd' and chan != 'p':
            # Convert to displacement
            mag *= 1e3 * (1/omega)
        elif unit_out == 'a' and chan != 'p':
            # Convert to acceleration
            mag *= 1e-3 * (omega/1)

        # ====== Scaling for input units
        if self.unit_in == 'a' and chan != 'p':
            mag *= 1e3 * (1/omega)


        # ====== divide by dB scale by 2 for filtfilt compensation
        mag = mag**(0.5)


        # ====== Interpolate to fft scale (work in log scale)
        y = np.interp(xp=freq, fp=20*np.log10(mag), x=freq_interp)
        y = 10**(y/20)

        # ====== Convert to complex
        y = y.astype(complex)

        debug(f"y: {y}")
        debug(f"freq_interp: {freq_interp}")

        debug(f"y.shape: {y.shape}")
        debug(f"y: {y}")

        return impulse.spectrum2impulse(y, self.taps)

    def createImpulsePhase(self, unit_out, chan):
        """
        Create a phase calibration impulse response.
        """
        debug(" ------ CreateImpulsePhase")

        # Get frequencies for impulse spectrum fft
        freq_interp = impulse.freqs(self.fs, self.taps)

        if self.spectrum_phase[chan] is None:
            if unit_out == self.unit_in or chan == 'p':
                # ====== No spectral calibration required
                return None
            y = np.zeros(freq_interp.size)
        else:
            # ====== Load user supplied spectrum
            # Note the negative sign here
            phase = -self.spectrum_phase[chan][:, 1]
            freq = self.spectrum_phase[chan][:, 0]
            # Interpolate
            y = np.interp(xp=freq, fp=phase, x=freq_interp)

        if chan != 'p':
            # ====== Add phase offsets for particle motion units
            if unit_out == 'd':
                # Convert to displacement
                y += -np.pi/2
            elif unit_out == 'a':
                # Convert to acceleration
                y += np.pi/2
            if self.unit_in == 'a':
                # convert input units to acceleration
                y += -np.pi/2

        y = np.exp(y*1j)

        debug(f"y.shape: {y.shape}")
        debug(f"y: {y}")

        return impulse.spectrum2impulse(y, self.taps, mag = False)
