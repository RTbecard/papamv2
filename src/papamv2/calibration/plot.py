import numpy as np

from PyQt5 import QtCore
import pyqtgraph as pqg

from matplotlib.figure import Figure

from papamv2.calibration import impulse
from papamv2.analysis import plots_pyqtgraph
from papamv2 import outputDictionaries as od

from papamv2.debug import debug

def plot_mag_pyqt(calibration, unit_out):

    if unit_out == 'p':
        channels = 'p'
    else:
        channels = 'xyz'

    # Make plotWidget
    pw = plots_pyqtgraph.make_plot(legend = True, grid = True)

    # ------ Loop channels
    for chan in channels:

        # Skip if no calibration data
        if calibration.impulse_magnitude[chan][unit_out] is None:
            continue

        # ------ Plot target spectrum
        if calibration.spectrum_magnitude[chan] is not None:
            x = calibration.spectrum_magnitude[chan][:,0]
            y = calibration.spectrum_magnitude[chan][:,1]

            # Unit corrections
            if unit_out == 'a':
                if x[0] == 0:
                    x[0] = x[1]
                y = y - 20*np.log10(x*2*np.pi) + 20*np.log10(1e3)
            elif unit_out == 'd':
                y = y + 20*np.log10(x*2*np.pi) - 20*np.log10(1e3)
                if x[0] == 0:
                    x[0] = x[1]

            pen = pqg.mkPen(
                od.chan_color[chan], size = 1.5, style=QtCore.Qt.DashLine)
            pw.plot(x, y, name=chan + " ideal", antialias = True, pen = pen)

        # ------ Plot realized spectrum
        x, y = realizedCalibrationMag(
            calibration.impulse_magnitude[chan][unit_out], calibration.fs)

        pen = pqg.mkPen(od.chan_color[chan], size = 1.5)
        pw.plot(x, y, name=chan + " realized", antialias = True, pen = pen)

    unit = od.ref_html[unit_out]
    ylab = '<math>' + \
        f'Receiver Sens. (re V/{unit})<sup>2</sup> / dB' + \
        '</math>'

    plots_pyqtgraph.setLabel(pw, 'bottom','Frequency (Hz)')
    plots_pyqtgraph.setLabel(pw, 'left', ylab)

    return pw

def plot_phase_pyqt(calibration, unit_out):

    if unit_out == 'p':
        channels = 'p'
    else:
        channels = 'xyz'

    # Make plotWidget
    pw = plots_pyqtgraph.make_plot(legend = True, grid = True)

    # ------ Loop channels
    for chan in channels:

        # Skip if no calibration data
        if calibration.impulse_phase[chan][unit_out] is None:
            continue

        # ------ Plot target spectrum
        if calibration.spectrum_phase[chan] is not None:
            x = calibration.spectrum_phase[chan][:,0]
            y = calibration.spectrum_phase[chan][:,1]

            # print(calibration.unit_in)
            # print(unit_out)

            # Unit corrections
            if unit_out == 'a':
                y = y - (np.pi/2)
            elif unit_out == 'd':
                y = y + (np.pi/2)
            if calibration.unit_in == 'a':
                y = y + (np.pi/2)

            # keep within +/- pi
            y[y > np.pi] = y[y > np.pi] - 2*np.pi
            y[y < -np.pi] = y[y < -np.pi] + 2*np.pi

            pen = pqg.mkPen(
                od.chan_color[chan], size = 2, style=QtCore.Qt.DashLine)
            pw.plot(x, y, name=chan + " ideal", antialias = True, pen = pen)

        # ------ Plot realized spectrum
        x, y = realizedCalibrationPhase(
            calibration.impulse_phase[chan][unit_out], calibration.fs)

        pen = pqg.mkPen(od.chan_color[chan], size = 1.5)
        pw.plot(x, -y, name=chan + " realized", antialias = True, pen = pen)

    ylab = 'Radians'

    plots_pyqtgraph.setLabel(pw, 'bottom','Frequency (Hz)')
    plots_pyqtgraph.setLabel(pw, 'left', ylab)

    return pw

def plot_mag(calibration, unit_out):

    if unit_out == 'p':
        channels = 'p'
    else:
        channels = 'xyz'

    # Make plotWidget
    fig = Figure()
    ax = fig.add_subplot(111)

    # ------ Loop channels
    for chan in channels:

        # ------ Plot target spectrum
        cal_spectrum = calibration.spectrum_magnitude[chan]
        if cal_spectrum is not None:
            x = cal_spectrum[:,0]
            y = cal_spectrum[:,1]

            # Unit corrections
            if unit_out == 'a':
                if x[0] == 0:
                    x[0] = x[1]
                y = y - 20*np.log10(x*2*np.pi) + 20*np.log10(1e3)
            elif unit_out == 'd':
                y = y + 20*np.log10(x*2*np.pi) - 20*np.log10(1e3)
                if x[0] == 0:
                    x[0] = x[1]

            ax.plot(x, y, label = "ideal " + od.unit_tab[unit_out],
                    color = od.chan_color[chan], linestyle = 'dashed')

        # ------ Plot realized spectrum
        cal_impulse = calibration.impulse_magnitude[chan][unit_out]
        if cal_impulse is not None:
            x, y = realizedCalibrationMag(cal_impulse, calibration.fs)

            ax.plot(x, y, label = "realized " + od.unit_tab[unit_out],
                    color = od.chan_color[chan], linestyle = 'solid')

    ax.ylabel = 'Receiver Sens. $\\mathrm{(re V/%s)^2 / dB}$' % \
        od.unit_tex[unit_out]
    ax.xlabel = 'Frequency (Hz)'
    ax.legend()
    ax.grid(True)

    return fig

def plot_phase(calibration, unit_out):

    if unit_out == 'p':
        channels = 'p'
    else:
        channels = 'xyz'

    # Make plotWidget
    fig = Figure()
    ax = fig.add_subplot(111)

    # ------ Loop channels
    for chan in channels:

        # ------ Plot target spectrum
        cal_spectrum = calibration.spectrum_phase[chan]
        if cal_spectrum is not None:
            x = cal_spectrum [:,0]
            y = cal_spectrum [:,1]

            # Unit corrections
            if unit_out == 'a':
                y += (np.pi/2)
            elif unit_out == 'd':
                y -= (np.pi/2)
            if calibration.unit_in == 'a':
                y -= (np.pi/2)


            ax.plot(x, y, label = "ideal " + od.unit_tab[unit_out],
                    color = od.chan_color[chan], linestyle = 'dashed')

        # ------ Plot realized spectrum
        cal_impulse = calibration.impulse_phase[chan][unit_out]
        if cal_impulse is not None:
            x, y = realizedCalibrationPhase(cal_impulse, calibration.fs)

            ax.plot(x, -y, label = "realized " + od.unit_tab[unit_out],
                    color = od.chan_color[chan], linestyle = 'solid')

    ax.ylabel = 'Receiver Sens. $\\mathrm{(re V/%s)^2 / dB}$' % \
        od.unit_tex[unit_out]
    ax.xlabel = 'Frequency (Hz)'
    ax.legend()
    ax.grid(True)

    return fig

def realizedCalibrationMag(x, fs):
    """
    Plot calibration sensitivity from impulse response
    """
    # We're multiplying the calibration fft length to a larger value so we
    # can see the ripple effect in our frequency response
    mlt = 10

    # Extract one-sided fft magnitudes
    # zero pad impulse response to its 10x the length.  This should result in
    # 1Hz frequency in resulting data.
    impulse_padded = np.append(x, np.zeros(x.size*(mlt-1)))
    n_padded = impulse_padded.size

    mag = abs(np.fft.fft(impulse_padded))

    # Get Frequencies for positive side
    freq = impulse.freqs(fs, n_padded)
    idx = np.where(freq >= 0)[0]

    # Adjust for 1-sided spectrum
    # multiple magnitudes above 0Hz and below nyquest by 2
    if freq[idx[-1]] == fs:
        # nyquest in range
        x = idx[1:-1]
    else:
        # nyquest outside range
        x = idx[1:]
    mag[x] = mag[x]

    # Return dB and freq values for plotting
    # multiply dB by 2 (filtfilt doubles filter order)
    out = -2*20*np.log10(mag[idx])

    return freq[idx], out

def realizedCalibrationPhase(x, fs):
    """
    Plot calibration sensitivity from impulse response
    """
    # We're multiplying the calibration fft length to a larger value so we
    # can see the ripple effect in our frequency response
    mlt = 10

    # move start of impulse to center
    x_shift = impulse.fft_shift(x, inverse = True)

    # Extract one-sided fft magnitudes
    # zero pad impulse response to its 10x the length.  This should result in
    # 1Hz frequency in resulting data.
    # we pad in the center of the signal to prevent shapse shifts (zero-phase
    # padding)
    n_mid = int(np.ceil(len(x)/2))
    impulse_padded = np.concatenate(
        (x_shift[:n_mid], np.zeros(x.size*(mlt-1)), x_shift[n_mid:]))
    n_padded = impulse_padded.size

    phase = np.angle(np.fft.fft(impulse_padded))

    # Get Frequencies for positive side
    freq = impulse.freqs(fs, n_padded)
    idx = np.where(freq >= 0)[0]

    return freq[idx], phase[idx]

def ideal(calib_ideal, chan, unit_out):
    """
    Plot ideal calibration sensitivity from calibration file
    """

    # ------ Particle motion
    # Frequencies
    freq = calib_ideal[0][chan][:, 0]
    freq_interp = np.array(range(int(round(freq[0])),
                                 int(round(freq[-1]))), dtype='f')

    # Convert to angular frequency
    omega = 2*np.pi*freq_interp

    # ---- Scaling coefificents for transforming particle motion units
    scale = np.ones(len(freq_interp))

    # Adjust scaling for desired output values
    if unit_out == 'd':
        # Convert to displacement
        scale = 1e-3 * (scale/omega)
        # Convert to from nm to pm
    elif unit_out == 'a':
        # Convert to acceleration
        scale = 1e3 * (omega/scale)

    dB_interp = np.interp(xp=freq, fp=calib_ideal[0][chan][:, 1],
                          x=freq_interp)
    value = dB_interp + 20*np.log10(scale)

    out = np.array([freq_interp, -value])
    return out

