"""
Standalone functions for generating impulse responses from calibration spectrums.
"""
import numpy as np
from papamv2.debug import *

def spectrum2impulse(x, n, mag = True):
    """
    Convert reciever sensitivity values to an impulse response for use in
    filtering.  Hilbert transforms will be applied to the FFTs and Hamming
    windows will be used for smoothing spectrum ripple.

    Parameters
    ----------
    x: numpy.array
        Single sided spectrum of V/unit.  These should be complex fft
        magnitudes: Evenly spaced along the frequency range of the signal.
    n: int
        length of impulse response.

    Return
    ------
    impulse: numpy.array
        Real valued impulse response (i.e. coefficients for FIR filter).
    """

    # ----- Make conjugate symmetrical, 2 sided spectrum of calib data
    if n % 2 == 0:
        # Even window length, nyquest included
        left = x.copy()
        right = x[1:-1][::-1].copy().conjugate()
    else:
        # Odd window length, nyquest not included
        left = x.copy()
        right = x[1:][::-1].copy().conjugate()

    symmetric = np.append(left, right)
    symmetric[0] = 0  # set DC offset to 0

    if n % 2 == 0:
        nq = int(n/2)
        # Set nyquest to 0 for phase
        symmetric.imag[nq] = 0
        # halve power of conjugate frequencies
        if mag:
            symmetric[1:nq] = symmetric[1:nq]
            symmetric[nq:] = symmetric[nq:]
    else:
        if mag:
            # halve power of conjugate frequencies
            symmetric[1:] = symmetric[1:]

    debug(f"symmetric.shape: {symmetric.shape}")
    debug(f"symmetric.head: {symmetric[:10]}")
    debug(f"symmetric.tail: {symmetric[-10:]}")

    # ----- Convert spectrum to time domain
    # Apply ifft (return complex impulse response)
    impulse = np.fft.ifft(symmetric)
    debug(f"impulse.head: {impulse[:10]}")
    debug(f"impulse.shape: {impulse.shape}")

    # Shift so main lobe is in center of signal, not idx 0
    # This is important for applying padding and windowing to the signal
    impulse = fft_shift(impulse)
    debug(f"impulse.head: {impulse[:10]}")
    debug(f"impulse.shape: {impulse.shape}")

    # Apply hamming window to impulse response
    # This will reduce rippling
    window = np.hamming(n)
    impulse = impulse*window

    # ------ Error checking ------
    # implse response should be real and have n samples.
    # If not... my iffts were applied incorrectly.
    # These checks are for my code, not user input errors

    debug(f'impulse response: {impulse[:30]}')

    if impulse.imag.mean() > 1e-10:
        # print(impulse.imag)
        raise RuntimeError("Algorithem Error: " +
                          "Impulse response not real valued.")
    if n != impulse.size:
        raise RuntimeError("Algorithem Error: " +
                          "Impulse response has incorrect size.")

    return np.real(impulse)

def freqs(fs, taps):
    """
    Calculate one-sided frequency values for impulse response spectrum
    """

    tmp = fft_freq(fs, taps)
    freq_interp = tmp[tmp >= 0]

    debug(f'freq_interp (first 10): {freq_interp[:10]}')

    return freq_interp

def fft_shift(x, inverse = True):
    """
    Shift a circular signal by N/2 samples, where N is the signal length.
    Shifts 0Hz from center to the left side.

    This is important for applying window functions to FFT generated impulse
    responses.

    Parameters
    ----------
    x: numpy:ndarray
        One dimensional array holding the signal to shift.
    """

    # Calc length and mid indicies
    N = len(x)
    if inverse:
        mid = int(np.ceil(N/2))
    else:
        mid = int(np.floor(N/2))
    # Make output array (copy)
    x_out = np.array(np.append(x[mid:N], x[0:mid]))

    return x_out

def fft_freq(fs, n):
    """
    Calculate frequencies for Fast Fourier Transform.

    Parameters
    ----------
    fs: float
        Sample rate.
    n: int
        Number of samples in fft.
    """
    # Calculate frequency values for fft results
    freq = np.array(range(0, n))*fs / n
    idx = freq > (fs/2)
    # Calculate negative frequencies
    freq[idx] = -(fs - freq[idx])

    return freq

