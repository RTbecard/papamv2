"""
Conveniance module for logging to text file.
"""

import os
import logging

# logging file
FL = os.path.expanduser("~/paPAM_debug.log")

if os.path.exists(FL):
    os.remove(FL)

logger = logging.getLogger('paPAM')

def enable_debug():
    logger.addHandler(logging.FileHandler(FL))
    logger.setLevel(logging.DEBUG)

def debug(txt):
    logger.debug(txt)

def info(txt):
    logger.info(txt)
