"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

A dictionary containing the default options for an analysis run.
"""
# ------Default options
# These are the values when the user doesnt specify options.  By default, all
# analyses are disabled.
options_default = {
    # ---- Input/Calibration
    "channels": None,
    "wav_files": None,
    "calibration_magnitude": None,
    "calibration_phase": None,
    "calibration_file_unit": 'v',
    "recorder_gain": (0, 0, 0, 0),
    # ------ Analysis scope options
    "events": [None],
    "subsets": None,
    "sequential_subsets": None,
    "transient_detection": (),
    # ------ General Settings
    "units_out": "pa",
    "bandpass": (None, None, 0, None),
    "si_axis": False,
    "pm_axis": False,
    "directional_units": "v",
    "rho": 1024,
    "c": 1500,
    "output_dir": "./",
    "output_name": None,
    "fig_size": (11.75, 8.25, 150),
    "fig_frmt": 'png',
    "no_output": False,
    "hide_plots": False,
    # ------ Analysis
    "waveform": False,
    "analytic_envelope": False,
    "binned_analysis": (1.0,0.5),
    "binned_kurtosis": False,
    "binned_rootMeanSquare": False,
    "binned_zeroToPeak": False,
    "binned_crestFactor": False,
    "binned_decidecadeRms": False,
    "time_exceeding_threshold": False,
    # Transient Analysis
    "impulse_rms": False,
    "impulse_kurtosis": False,
    "exposure": False,
    "zero_to_peak": False,
    "crest_factor": False,
    "rise_time": False,
    "pulse_length": False,
    # Continuous Analysis
    "rms": False,
    "kurtosis": False,
    # ------ Frequency domain
    "spectrogram": False,
    "spectral_probability_density": False,
    "psd": False,
    "decidecade_rms": False,
    "esd": False,
    "decidecade_exposure": False,
    # ------ Directional Analysis
    "directional_spectrogram": False,
    "binned_soundIntensity": False,
    "binned_particleMotionVector": False
}
