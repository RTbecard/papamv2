"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Parsing and validation of provided analysis features.  This will convert the
textual options provided by the CLI or GUI into types for internal use.
"""
# Regular expressions for input checking
import re
# For system paths
from os import path
# For timestamps
import datetime
import copy
# Defauly options values
from papamv2 import optionParsingDefault as default

def parse(options):
    """
    Parse the options and convert them into the proper data types.

    Parameters
    ----------
    params: dict
        Dictionary holding the argument and values pairs describing the
        analysis features.

    Return
    ------
    (cmd, options)

    A tuple holding a command string and a dictionary with the parsed analysis
    options.
    """

    # ------ Init default options
    ret = copy.deepcopy(default.options_default)

    # ------ Filter out None values
    # This is necessary for the CLI as None is
    # default value for unsepcified options (i.e. None = use default)
    options = copy.deepcopy(options)
    options = {k: v for k, v in options.items() if v is not None}

    # print(params)

    # ------ Set special defaults
    # set output name as date-time stamp
    if ret['output_name'] is None:
        ret['output_name'] = \
            datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    if ret['no_output']:
        ret['output_dir'] = "./"

    # ------ Parse and validate user provided options
    for key in options.keys():
        # Check if provided option is default value
        if ret[key] != options[key]:
            # Check and store new value
            ret[key] = set_option(key, options[key])

    return ret

def cmd_args(option):
    """
    Convert parsed parameter back into CLI argument string.
    """
    if option is None or option is True:
        # ------ If value is true or nonw, return empty string
        return ''
    if isinstance(option, (tuple, list)):
        # ------ Multiple options
        # Convert tuple into list of strings
        def fnc(x):
            if isinstance(x, (tuple, list)):
                out = []
                for i in x:
                    if isinstance(i, (tuple, list)):
                        out.append("-".join(map(str, i)))
                    else:
                        out.append(str(i))
                return ";".join(out)
            return str(x)
        option = list(map(fnc, list(option)))
        # print(string)
        # append to cmd
        return '=' + ",".join(option)
    if type(option).__name__ == 'str':
        # ------ Single parameter
        return '=\'' + option + '\''
    # other single values
    return '=' + str(option)

def cmd(options):
    """
    Generate a CLI call to reproduce the analyis with the given options.
    """
    # ------ Overwrite defaults with user supplied values
    # add required positional arguments (channels and wav_files)
    wavs = list(map(lambda x: '\'' + x + '\'', options['wav_files']))
    cmmd = 'paPAMv2 ' + options['channels'] + " " + " ".join(wavs)


    for key in options.keys():

        # ------ Check if it matches default value
        if options[key] != default.options_default[key]:

            # ------ Add to cmd string if not matching
            if key not in ('channels', 'wav_files'):
                # Optional keyword argument
                cmmd = cmmd + " --" + key.replace('_', '-') + \
                    cmd_args(options[key])

    return cmmd

def set_option(option, value):
    """
    Set options or features for the analysis.

    This will automatically call the appropriate function for checking
    the option is valid.  If the option is valid, it will be converted to
    the correct type and saved in the options dictionary.

    Parameters
    ----------
    option: str
        String holding name of option to set value for.
    value: Object
        Value to assign to option.  If an invalid type is provided, an
        error will be thrown.
    """

    # Load all function names from this module
    dct = globals()
    # Check options have matching validation functions
    if option not in dct.keys():
        raise ValueError("Invalid option: " + str(option))

    fcn = dct[option]

    # Run vaidation check (will raise error in not valid)
    return fcn(value)

# ========================== Validate option inputs =========================
#
# The following functions take a single input, val, and return the
# appropriately formatted paramter value set.  Essentially, this parses the
# textual input into usable paramters.
#
# Invalid options should result in paPAMv2 throwing a descriptive error
# message.

# ====== Helper function
def strisfloat(val, option):
    """ Return True if string can be read as a float """
    pattern = re.compile("-$|-?\\d+\\.?\\d*")

    if pattern.fullmatch(val) is None:
        raise ValueError(option + ": Must be a float.")

def strisint(val, option):
    """ Return True is string can be read as an integer """
    pattern = re.compile("^\\d+$")

    if pattern.fullmatch(val) is None:
        raise ValueError(option + ": Must be an integer.")

def strisprop(val, option):
    """ Return True is string can be read as a proportion """
    pattern = re.compile("0$|0?\\.\\d*")

    if pattern.fullmatch(val) is None:
        raise ValueError(option + \
                ": Must be a proportion greater than 0 and less than 1.")

def strispositive(val, option):
    """ Return true is string can be read as a positive number """
    pattern = re.compile("\\d+\\.?\\d*")

    if pattern.fullmatch(val) is None:
        raise ValueError(option + ": Must be a positive value.")

def strisfloatlist(val, option, sep = ","):
    """ Check if string is a list of floats"""
    pattern = re.compile(
        pattern="\\d+\\.?\\d*-\\d+\\.?\\d*(?:"+ sep
        + "\\d+\\.?\\d*-\\d+\\.?\\d*)*")

    if pattern.fullmatch(val) is None:
        raise ValueError(option +
            ": Must be a comma seperated list of floats such as " +
                         "\'1.3,3.4,5.6\'")

def strisproplist(val, option, sep = ","):
    """ Check if string is a list of propotions"""
    pattern = re.compile(pattern="(0$|0?\\.\\d*)(?:"+sep+"0?\\.\\d*)*")

    if pattern.fullmatch(val) is None:
        raise ValueError(option +
            ": Must be a comma seperated list of floats greater than 0" +
                         " and less or equal to one such as \'0.5,0.95\'")

def strsep(val, l, option):
    """ Parse comma seperateed string """
    vals = val.split(",")
    if len(vals) != l:
        raise ValueError(
            option + ": Expected " + str(l) +" comma seperated values, got " +
            str(len(vals)) + ": " + val)
    return vals

# ====== Input/Output
def channels(val):
    """ make sure that all characters are valid options and none are chosen
    twice (i.e. you cannot have 2 x channels). """

    chns = "xyzp"
    vals = val
    # Check values
    for v in vals:
        # Check if current chan is in chns string
        ret = chns.find(v)
        if ret == -1:
            # No match
            raise ValueError(
                "<channels> has duplicate or invalid characters")
        # Remove found channel option from list of options
        chns = chns[:ret] + chns[ret+1:]
        # print(chns)

    return val

def wav_files(vals):
    """ Return a list of absolute paths (strings) """
    # val is a list of string objects
    # vals = val.split(sep=",")

    vals = list(map(path.abspath, vals))

    # print(vals)
    def chk(wav_file):
        if not path.isfile(wav_file):
            raise ValueError("<wav-files> has an invalid file path")
        if path.splitext(wav_file) != "wav" or \
                path.splitext(wav_file) != "WAV":
            raise ValueError("<wav-files> files must have extension *.wav")
    # Run check on all files

    map(chk, vals)

    return vals

def subsets(vals):
    """ Return a list of tuples (start second, stop second) """

    # Validate range string
    strisfloatlist(vals, "<subsets>")

    lst = vals.split(",")
    tpls = list(map(lambda x: tuple(x.split('-')), lst))

    def tmp(x):
        if len(x) != 2:
            raise ValueError(
                "--subsets: Expected a semicolon seperated list of ranges" +
                " formatted like \"1.2-2;3.5;6.5\"")
    map(tmp, tpls)

    return (tpls, )

def sequential_subsets(vals):
    """ Return tuple of (seconds, overlap) """

    strispositive(vals, "<sequential-subsets>")

    return float(vals)

def output_dir(val):
    """ Check valid directory path """

    val = path.abspath(val)
    if not path.isdir(val):
        raise ValueError("Invalid output directory")
    return val

def output_name(val):
    """ Check valid name """
    if re.fullmatch("(\\w|\\s)+", val) is None:
        raise ValueError("--output_name: Cannot be empty or contain" +
                         " special characters")
    return val

def fig_size(val):
    """ Check string format """
    vals = strsep(val, 3, "---fig-size")
    strispositive(vals[0], "--fig-size <height>")
    strispositive(vals[1], "--fig-size <width>")
    strispositive(vals[2], "--fig-size <dpi>")

    return float(vals[0]), float(vals[1]), float(vals[2])

def no_output(val):
    """ Set to True when selected"""
    return val

def hide_plots(val):
    """ Set to True when selected """
    return val

# ====== Calibration

def calibration_magnitude(val):
    """ Check file path and appropriate particle motion unit type. """

    ext = path.splitext(val)[1]
    # ------ Check path to calibration file
    if not path.isfile(val):
        raise ValueError("--calibration-magnitude: invalid file path")
    if not ext in (".csv", ".CSV"):
        raise ValueError("--calibration-phase: must be a *.csv file")

    return val

def calibration_phase(val):
    """ Check file path and appropriate particle motion unit type. """

    ext = path.splitext(val)[1]
    # ------ Check path to calibration file
    if not path.isfile(val):
        raise ValueError("--calibration-phase: invalid file path")
    if not ext in (".csv", ".CSV"):
        raise ValueError("--calibration-phase: must be a *.csv file")

    return val

def calibration_file_unit(val):
    """ Check file path and appropriate particle motion unit type. """

    # ------ Check calibration unit
    if len(val) != 1:
        raise ValueError("--unit-calibration-file: must be a single character.")
    if not re.findall('[av]', val):
        raise ValueError("--unit-calibration-file: must be either 'a' or 'v'.")

    return val

def unit_in(val):
    """ Check that valid particle motion character is provided.  This is the units
    of the recorded signal. """
    if len(val) != 1:
        raise ValueError("--unit-in: must be a single character.")

    if re.fullmatch("[av]", val) is None:
        raise ValueError("--unit-in: must be either a or v.")

def recorder_gain(val):
    """ Check formatting of recoder gain input """
    vals = strsep(val, 4, "--recorder-gain")

    # Convert empty strings to '0'
    def fcn(x):
        if x == '':
            return '0'
        return x
    vals = list(map(fcn, vals))

    # Check values
    map(lambda x: strisfloat(x, "--recorder-gain " + x) , vals)

    # Return floats
    return tuple(map(float, vals))

# ====== General Settings ======

def bandpass(val):
    """ Check formatting of bandpass input """
    vals = strsep(val, 3, "--bandpass")
    # verify all values are floats
    strispositive(vals[0],"--bandpass <high-pass>")
    strispositive(vals[1],"--bandpass <low-pass>")
    strisint(vals[2], "--bandpass <order>")

    vals[0] = float(vals[0])
    vals[1] = float(vals[1])

    return (None if vals[0] == 0 else vals[0],
            None if vals[1] == 0 else vals[1],
            int(vals[2]))

def decidecade(val):
    """ Make sure order is an integer """
    strisint(val, "--decidecade <order>")
    return int(val)

def units_out(val):
    """ Check validity of sound field component characters. """
    # if len(val) < 1:
    #     raise ValueError("--units-out: must provide a non-empty " +
    #                      "string holding characters from the set " +
    #                      "{'p', 'd', 'v', 'a', 'e'}")

    # Check for inpropper characters
    components = 'pdvae'
    for v in val:
        # Iteratively remove valid characters from string
        ret = components.find(v)
        if ret == -1:
            # If any characters remain, string is invalid
            raise ValueError("--units-out: " +
                             "Invalid or repeat characters in string.")
        # remove character from pool
        components = components[:ret] + components[ret+1:]
    return val

def si_axis(val):
    """Return true if selected"""
    return val

def pm_axis(val):
    """Return true if selected"""
    return val

def rho(val):
    """ Check that is numeric """
    # Verify its a float
    strisfloat(val, "--rho")
    return float(val)

def c(val):
    """ Check that is numeric """
    strisfloat(val, "--c")
    return float(val)

# ====== Analysis Settings

def waveform(val):
    """ Automatically True if selected """
    return val

def binned_analysis(val):
    """ Check formatting of string """
    vals = strsep(val, 2, "--binned-analysis")
    strisfloat(vals[0], "--binned-analysis <bin size>")
    strisprop(vals[1], "--binned-analysis <proportion overlap>")
    return (float(vals[0]), float(vals[1]))

def binned_kurtosis(val):
    return(val)

def binned_rootMeanSquare(val):
    return(val)

def binned_zeroToPeak(val):
    return(val)

def binned_crestFactor(val):
    return(val)

def binned_decidecadeRms(val):
    return(val)

def binned_soundIntensity(val):
    return(val)

def binned_particleMotionVector(val):
    return(val)

def time_exceeding_threshold(val):
    """ Check formatting of string """
    vals = strsep(val, 3, "--binned-analysis")
    strisfloat(vals[0], "--binned-analysis <bin size>")
    strisprop(vals[1], "--binned-analysis <proportion overlap>")
    strisfloat(vals[2], "--binned-analysis <threshold>")
    return float(vals[0]), float(vals[1]), float(vals[2])

def spectrogram(val):
    """ Check formatting of string """
    vals = strsep(val, 3, "--spectrogram")
    strisint(vals[0], "--spectrogram <window length>")
    strisprop(vals[2], "--spectrogram <proportion overlap>")
    return (int(vals[0]), vals[1], float(vals[2]))

def directional_spectrogram(val):
    """ Check formatting of string """
    vals = strsep(val, 4, "--directogram")
    strisint(vals[0], "--directogram <window length>")
    strisprop(vals[2], "--directogram <proportion overlap>")
    return (int(vals[0]), vals[1], float(vals[2]), vals[3])

def spectral_probability_density(val):
    """ Check formatting of string """
    vals = strsep(val, 5, "--spectral-probability-density")
    strisint(vals[0], "--spectral-probability-density <window length>")
    strisprop(vals[1], "--spectral-probability-density <proportion overlap>")
    strisint(vals[3], "--spectral-probability-density <PSD Bin Size>")
    strisproplist(vals[4], "--spectral-probability-density <Percentiles>", ";")

    return (int(vals[0]), vals[2], float(vals[1]), int(vals[3]),
            list(map(float, vals[4].split(";"))))

def decidecade_rms(val):
    """Return true"""
    return val

def decidecade_exposure(val):
    """Return True"""
    return val

def exposure(val):
    """ Set to True if selected """
    return val

def positive_peak(val):
    """ Set to True if selected """
    return val

def negative_peak(val):
    """ Set to True if selected """
    return val

def zero_to_peak(val):
    """ Set to True if selected """
    return val

def crest_factor(val):
    """ Set to True if selected """
    return val

def rise_time(val):
    """ Check formatting of input string """
    vals = strsep(val, 2, "--rise-time")
    strisprop(vals[0], "--rise-time <lower>")
    strisprop(vals[1], "--rise-time <upper>")
    return float(vals[0]), float(vals[1])

def pulse_length(val):
    """ Check formatting of input string """
    vals = strsep(val, 2, "--pulse-length")
    strisprop(vals[0], "--pulse-length <lower>")
    strisprop(vals[1], "--pulse-length <upper>")
    return float(vals[0]), float(vals[1])

def analytic_envelope(val):
    """ Return true if selected """
    return val

def esd(val):
    """ Check formatting of input string """
    vals = strsep(val, 3, "--esd")
    strisint(vals[0], "--esd <window length>")
    strisprop(vals[2], "--esd <proportion overlap>")
    return (int(vals[0]), vals[1], float(vals[2]))

def rms(val):
    """ Return true if selected """
    return val

def impulse_rms(val):
    """ Check formatting of input string """
    vals = strsep(val, 2, "--impulse-rms")
    strisprop(vals[0], "--impulse-rms <lower>")
    strisprop(vals[1], "--impulse-rms <upper>")
    return float(vals[0]), float(vals[1])

def transient_rms(val):
    """ Check formatting of input string """
    vals = strsep(val, 2, "--impulse-rms-level")
    strisprop(vals[0], "--impulse-rms-level <lower>")
    strisprop(vals[1], "--impulse-rms-level <upper>")

    return (float(vals[0]), float(vals[1]))

def kurtosis(val):
    """ Return True if selected """
    return val

def impulse_kurtosis(val):
    """ Return True """
    return val

def psd(val):
    """ Check formatting of input string """
    vals = strsep(val, 3, "--psd")
    strisint(vals[0], "--psd <window length>")
    strisprop(vals[2], "--psd <overlap>")
    return int(vals[0]), vals[1], float(vals[2])

