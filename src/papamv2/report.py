"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Input/Output
------------
This file holds functions associated with reading/writing files.

Functions for writing results to csv files will append if the file already
exists.
"""

# Standard libraries
import os
import datetime  # Timestamps

import numpy as np  # Arrays

from papamv2 import outputDictionaries as od

class Report:
    """
    This class holds all functions for writing results to the disk.
    This should be initiated once at the start of each run.  It holds folder
    paths and write settings.

    Parameters
    ----------
    no_output: logical
        When True, nothing will be written to disk.
    fig_size: [width, height]
        Size of output figure in inches
    fig_dpi: int
        Resolution of resulting figures printed to disk.
    """
    def __init__(self, parameters):

        # ------ Parameters
        self.no_output = parameters['no_output']
        self.fig_size = parameters['fig_size'][0:2]
        self.fig_dpi = parameters['fig_size'][2]
        self.fig_frmt = parameters['fig_frmt']
        self.output_dir = parameters['output_dir']
        self.output_name = parameters['output_name']
        # Isolate basename (no extension) from wav file list
        self.wav_file, _ = os.path.splitext(os.path.basename(
            parameters['wav_files'][0]))

        self.stamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        self.filestamp = "_".join([self.stamp, self.wav_file])
        # VectorData parameters
        self.fs = None

        # Subset parameters
        self.rng_id = None
        self.rng_str = None
        self.rng = None
        self.unit_out = None
        self.band = None
        self.dir_pm = ""
        self.dir_si = ""
        # ------ Setup folder structure
        self.path = os.path.join(self.output_dir, self.output_name)
        self.output_plots = os.path.join(self.path, "plots_results")
        self.output_calibrations = os.path.join(self.path, "plots_calibration")
        self.output_data = os.path.join(self.path, "plots_data")
        self.output_bandpass = os.path.join(self.path, "plots_bandpass")
        self.output_log_folder = os.path.join(self.path, "logs")

        # ------ Setup generic output files
        self.output_ranges = os.path.join(self.path, "ranges.csv")
        self.output_log = os.path.join(self.output_log_folder, self.stamp + "_log" + ".txt")
        self.output_results = os.path.join(self.path, "results.csv")

        # ------ Create output folders
        if not self.no_output:
            for fldr in [self.path, self.output_plots,
                    self.output_calibrations, self.output_data,
                    self.output_bandpass, self.output_log_folder]:
                try:
                    os.mkdir(fldr)
                except FileExistsError:
                    pass # do nothing

        # ------ Init logfile
        dev = os.devnull if self.no_output else self.output_log
        with open(dev, "w") as fid:
            fid.write("Analysis Started: " + self.stamp + "\n")

    def range(self, rng_id, rng):
        """
        Update reporting parameters for the current signal subset.
        """

        self.rng_id = str(rng_id)
        self.rng = rng
        self.rng_str = str(np.round(rng.start/self.fs, 2)) + "-" + \
                str(np.round(rng.stop/self.fs, 2))

    def set_dir_si(self, bearing, v_angle):

        self.dir_si = "%.2f; %.2f" % (bearing, v_angle)

    def set_dir_pm(self, bearing, v_angle, e):

        self.dir_pm = "%.2f; %.2f; %.2f" % ( bearing, v_angle, e )

    def set_dir_none(self):
        self.dir_pm = "NA"
        self.dir_si = "NA"


    def printPlots(self, filename, fcn, args, path = None):
        """
        Print plots to file.

        Plots are saved as pdf files.

        Parameters
        ----------
        filename: str
            Path to save the image to.
        plots: [ str, [...], {...}]
            A list holding 1) the name of the matplotlib plotting method to
            apply to the axes, 2) a list of positional arguments for the plot
            method, and 3) a dict of keyword arguments for the function.
        size: (width, height)
            A tuple holding integer values for the width and height of the
            plot to output.
        """

        # Exit function if no output option is selected
        if self.no_output:
            return

        # Create figure on canvas object
        fig = fcn(*args)
        fig.set_size_inches(self.fig_size[0], self.fig_size[1])
        fig.set_tight_layout(True)
        # # Set canvas size and print to file
        if path is None:
            # Use default output path
            path = self.output_plots

        # Define figure name
        nm = os.path.join(path,
                "_".join([self.filestamp, filename]) +
                '.' + self.fig_frmt)

        # Print t ofile
        # canvas.print_figure(filename=nm, format=self.fig_frmt, dpi = self.fig_dpi)
        fig.savefig(fname=nm, format=self.fig_frmt, dpi = self.fig_dpi)

    def write_ranges(self):
        """
        Write current analysis range to file.  Range IDs can be used to link
        saved figures to time periods of analysis.
        """

        if self.no_output:
            return

        # Check if range file exists
        exsts = os.path.isfile(self.output_ranges)

        with open(self.output_ranges, 'a') as fid:
            if not exsts:
                # Write headers if new file
                fid.write("run, first_file, range_id, sample_start, " +
                          "sample_end, second_start, second_end," +
                          " pm_direction, si_direction\n")

            fid.write("%s, %s, %s, %i, %i, %.3f, %.3f, %s, %s\n" %
                      (self.stamp, self.wav_file, self.rng_id,
                      self.rng.start, self.rng.stop,
                      self.rng.start/self.fs,
                      self.rng.stop/self.fs,
                      self.dir_pm, self.dir_si))

    def bin_write(self, filename, data):
        """
        Write data to a binary file.  Encoding is little endian, float
        (single precision).

        Parameters
        ----------
        filename: str
            Absolute path to write data to.
        data: numpy.array
            Array to write to disk.  It should be the aim to write channels to
            different files, making the data easier to manage and remove
            ambiguity the format of multidimensional arrays.
        """

        if self.no_output:
            return

        # Open file for writing
        fl = os.path.join(self.output_data, self.filestamp + "_" + filename)
        with open(fl, mode='wb') as fid:
            # Cast array to litte endian and float, then write to file
            data.astype('<f').tofile(fid)

    def write_results(self, x, analysis, unit_analysis, fun = lambda x: x):
        """
        Write dictionary of results to csv.  Each channel gives a single value.
        A single results file is expected for an analysis run.

        Parameters
        ----------
        filename: file
            Filename to write results to.
        x: dict
            Dictionary of results for each channel to write to csv file.
        analysis: str
            String naming the type of analysis being applied.
        unit_analysis: str
            String holding the output units of the analysis.
        """

        if self.no_output:
            return

        exsts = os.path.isfile(self.output_results)

        with open(self.output_results, 'a') as fid:
            if not exsts:
                # Write headers if new file
                fid.write("run, first_file, component, band, range_id, " +
                        "range_str, analysis, channel, value, unit\n")

            for channel in x.keys():
                fid.write(",".join([
                    self.stamp,
                    self.wav_file,
                    self.unit_out,
                    self.band,
                    self.rng_id,
                    self.rng_str,
                    '"' + analysis + '"',
                    channel,
                    str(fun(x[channel])),
                    unit_analysis + '\n']))

    def write_csv(self, name, data_headers, d_res, transpose = False):
        """
        Write dictionary of results to csv.  Each channel can give a table of
        results (such as spectrograms or PSDs).

        Multiple ranges will be written to the same file.  This facilitates the
        creation of plotting mutliple PSDs over the span of a signal.

        Parameters
        ----------
        name: str
            Name object to write results to.
        data_headers: [str, str, ...]
            A list of strings holding headers for the array data in d_res.
        d_res: dict
            Dictionary of results for each channel to write to csv file.
        rng_id: int
            Integer holding the range ID
        """

        if self.no_output:
            return

        filename = os.path.join(self.output_data,
                self.filestamp + '_' + name + ".csv")

        exsts = os.path.isfile(filename)

        self.pamPrint("Writing to csv...")
        with open(filename, 'a') as fid:
            if not exsts:
                # Write headers if new file
                fid.write('range_id, channel, ' +
                    ', '.join(data_headers) + '\n')

            # Loop channels
            for chan in list(d_res.keys()):

                # Extract channel data
                x = d_res[chan]

                if transpose:
                    x = x.T

                # loop rows and write values
                txt = ""
                for row in range(0, x.shape[1]):
                    txt += str(self.rng_id) + ', ' + chan + ', ' + \
                            ', '.join(map(str, x[:, row].tolist())) + '\n'

                fid.write(txt)


    # ============================== Print functions ==========================
    # Functions for printing to console and logfile.
    def pamPrint(self, x):
        """
        print function wrapper.  results printed here will be copied to the log
        file, in addition to the stdout stream.
        """

        print(x)

        dev = os.devnull if self.no_output else self.output_log
        try:
            with open(dev, 'a') as fid:
                fid.write(x + '\n')
        except PermissionError as e:
            print("""
paPAM enountered a permissions error.

This usually means paPAM cannot write to the output folder.  Make sure you have write permissions in the selected output directory.

Alternatively, working on a network drive which may occasionally raise this error.  Thus, working on network drives is strongly discouraged.
""")
            raise

    def printHeader(self, title, char, nl=False):
        """
        Print section title in log and console
        """
        width = 79
        gap = 2 + len(title)
        pad = (width - gap)/2

        if nl:
            self.pamPrint('')

        # Show measure header
        strng = (char*int(np.floor(pad))) + " " + title \
                + " " + (char*int(np.ceil(pad)))
        self.pamPrint(strng)

    def printResult(self, x, measure, analysis_unit="", trnsf = lambda x: x):
        """
        Print results dictionary to console

        Parameters
        ----------
        x: dict
            Dict holding the sigle values result for each channel.
        measure: str
            String holding the header for the results section.  This is
            typically the name of the analysis.
        trnsf: function
            The reported value will have this function applied to it.  You can
            use this to report linear values as decibles.
        """

        self.pamPrint('------ ' + measure)

        # Value to print

        # loop channels
        for channel in x.keys():
            val = trnsf(x[channel])
            fmt = "%s: %.5e %s" if abs(val) > 1000  or abs(val) < 1 else "%s: %.3f %s"
            self.pamPrint( fmt % (od.chan_ascii[channel], val, analysis_unit))
