"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions holding code for each analysis feature in paPAM.

These wrapper functions deal with formatting the analysis function inputs and
outputs.  The actual analysis code is in papamv2.analysis.functions.
"""

import os

import numpy as np

from papamv2 import outputDictionaries as od
from papamv2.analysis import plots as ap
from papamv2.analysis import plots_pyqtgraph as apg
from papamv2.analysis import functions as af
from papamv2.analysis import write as aw

from papamv2.debug import debug
# =============================================================================
# ========================= Time domain analysis ==============================
# =============================================================================

def waveform(**kwargs):
    """
    Print/save results of waveform
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating wavform...")

    d_res = af.waveform(subset)

    # ---- Save binary files
    for chan, value in d_res.items():
        # Generate filename
        name = 'waveform_%s_band-%s_rangeID-%s_chan-%s_fs-%i.bin' % \
            (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, chan, Rpt.fs)
        # Write data to binary file
        Rpt.bin_write(name, value)

    # ------ Save plot
    filename = 'waveform_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)
    Rpt.printPlots(filename, ap.waveform, [d_res, subset])

    # ------ Show plot
    PamPlot.plot("Waveform " + od.unit_tab[subset.unit_out],
                 apg.waveform, [d_res, subset])

def analytic_envelope(**kwargs):
    """
    Print/save results of analytic_envelope
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating analytic envelope...")

    d_res = af.analytic_envelope(subset)

    # ---- Save binary files
    for chan, value in d_res.items():
        filename = 'analyticEnvelope_%s_band-%s_rangeID-%s_chan-%s_fs-%i.bin' %\
            (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, chan, Rpt.fs)
        Rpt.bin_write(filename, value)

    # ------ Save plot
    filename = 'analyticEnvelope_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)
    Rpt.printPlots(filename, ap.analytic_envelope, [d_res, subset])

    # ---- Show plot
    tab_name = "Analytic Envelope " + od.unit_tab[subset.unit_out]
    PamPlot.plot(tab_name, apg.analytic_envelope, [d_res, subset])

def binned_kurtosis(**kwargs):
    """Binned kurtosis"""

    params = kwargs['params']
    if not params: return

    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']
    binned_params = kwargs['binned_params']

    bin_size = binned_params[0]
    olapn = np.floor(binned_params[1] * subset.fs)

    # Extract parameters
    bin_size = int(binned_params[0]*subset.fs)
    olap = binned_params[1]
    olapn = int(np.floor(olap*bin_size))
    if olapn == bin_size:
        raise ValueError("Error in Binned Analysis.  Overlap value too high for bin size.")

    Rpt.pamPrint("------ Running Binned Kurtosis...")

    d_res = af.binned_kurt(subset, bin_size, olapn)

    # ---- Save csv file
    filename = 'binned-kurtosis_%s_band-%s_rangeID-%s_fs-%i.bin' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)

    Rpt.write_csv(filename, ["Seconds", "Kurtosis"], d_res, transpose = True)

    # ------ Save plot
    filename = 'binned-kurtosis_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)
    args = [ d_res, subset,  bin_size, olapn ]
    Rpt.printPlots(filename, ap.binned_kurtosis, args)

    # ---- Show plot
    tab_name = "Binned Kurtosis " + od.unit_tab[subset.unit_out]
    args = [d_res, subset, bin_size, olapn]
    PamPlot.plot(tab_name, apg.binned_kurtosis, args)

def binned_rootMeanSquare(**kwargs):
    """Binned rms"""

    params = kwargs['params']
    if not params: return

    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']
    binned_params = kwargs['binned_params']

    bin_size = binned_params[0]
    olapn = np.floor(binned_params[1] * subset.fs)

    # Extract parameters
    bin_size = int(binned_params[0]*subset.fs)
    olap = binned_params[1]
    olapn = int(np.floor(olap*bin_size))
    if olapn == bin_size:
        raise ValueError("Error in Binned Analysis.  Overlap value too high for bin size.")

    Rpt.pamPrint("------ Running Binned Root-mean-square...")

    d_res = af.binned_rms(subset, bin_size, olapn)

    # ---- Save csv file
    filename = 'binned-rms_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)

    csv_header = 'L_{%s} (re (1 %s)^2) / dB' % \
        (od.unit_ascii[subset.unit_out], od.ref_ascii[subset.unit_out])
    Rpt.write_csv(filename, ["Seconds", csv_header], d_res, transpose = True)

    # ------ Save plot
    filename = 'binned-rms_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)
    args = [ d_res, subset,  bin_size, olapn ]
    Rpt.printPlots(filename, ap.binned_rms, args)

    # ---- Show plot
    tab_name = "Binned RMS " + od.unit_tab[subset.unit_out]
    args = [d_res, subset, bin_size, olapn]
    PamPlot.plot(tab_name, apg.binned_rms, args)

def binned_zeroToPeak(**kwargs):
    """Binned zpk"""

    params = kwargs['params']
    if not params: return

    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']
    binned_params = kwargs['binned_params']

    # Extract parameters
    bin_size = int(binned_params[0]*subset.fs)
    olap = binned_params[1]
    olapn = int(np.floor(olap*bin_size))
    if olapn == bin_size:
        raise ValueError("Error in Binned Analysis.  Overlap value too high for bin size.")

    Rpt.pamPrint("------ Running Binned Zero-to-peak...")

    d_res = af.binned_zeroToPeak(subset, bin_size, olapn)

    # ---- Save csv file
    filename = 'binned-zeroToPeak_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)

    csv_header = '"L_{pk,%s} (re (1 %s)^2) / dB"' % \
        (od.unit_ascii[subset.unit_out], od.ref_ascii[subset.unit_out])
    Rpt.write_csv(filename, ["Seconds", csv_header], d_res, transpose = True)

    # ------ Save plot
    filename = 'binned-zeroToPeak_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)
    args = [ d_res, subset,  bin_size, olapn ]
    Rpt.printPlots(filename, ap.binned_zeroToPeak, args)

    # ---- Show plot
    tab_name = "Binned zeroToPeak " + od.unit_tab[subset.unit_out]
    args = [d_res, subset, bin_size, olapn]
    PamPlot.plot(tab_name, apg.binned_zeroToPeak, args)

def binned_crestFactor(**kwargs):
    """Binned crest factor"""

    params = kwargs['params']
    if not params: return

    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']
    binned_params = kwargs['binned_params']

    bin_size = binned_params[0]
    olapn = np.floor(binned_params[1] * subset.fs)

    # Extract parameters
    bin_size = int(binned_params[0]*subset.fs)
    olap = binned_params[1]
    olapn = int(np.floor(olap*bin_size))
    if olapn == bin_size:
        raise ValueError("Error in Binned Analysis.  Overlap value too high for bin size.")

    Rpt.pamPrint("------ Running Binned Crest Factor...")

    d_res = af.binned_crestFactor(subset, bin_size, olapn)

    # ---- Save csv file
    filename = 'binned-crestFactor_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)

    csv_header = '"L_{cf,%s} (re (1 %s)^2) / dB"' % \
        (od.unit_ascii[subset.unit_out], od.ref_ascii[subset.unit_out])
    Rpt.write_csv(filename, ["Seconds", csv_header], d_res, transpose = True)

    # ------ Save plot
    filename = 'binned-crestFactor_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)
    args = [ d_res, subset,  bin_size, olapn ]
    Rpt.printPlots(filename, ap.binned_crestFactor, args)

    # ---- Show plot
    tab_name = "Binned crestFactor " + od.unit_tab[subset.unit_out]
    args = [d_res, subset, bin_size, olapn]
    PamPlot.plot(tab_name, apg.binned_crestFactor, args)

def binned_decidecadeRms(**kwargs):
    """Binned decidecade RMS"""

    params = kwargs['params']
    if not params: return

    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']
    binned_params = kwargs['binned_params']

    bin_size = binned_params[0]
    olapn = np.floor(binned_params[1] * subset.fs)

    # Extract parameters
    bin_size = int(binned_params[0]*subset.fs)
    olap = binned_params[1]
    olapn = int(np.floor(olap*bin_size))
    if olapn == bin_size:
        raise ValueError("Error in Binned Analysis.  Overlap value too high for bin size.")

    Rpt.pamPrint("------ Running Binned Decidecade RMS...")

    d_res = af.binned_decidecadeRms(subset, bin_size, olapn)

    # ---- Save csv file
    filename = 'binned-decidecadeRms_%s_band-%s_rangeID-%s_fs-%i' % \
        (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, Rpt.fs)

    csv_header = '"L_{%s} (re (1 %s)^2)"' % \
        (od.unit_ascii[subset.unit_out], od.ref_ascii[subset.unit_out])
    Rpt.write_csv(filename, ["Seconds", "Decidecade bin (Hz)", csv_header],
                  d_res, transpose = True)

def time_exceeding_threshold(**kwargs):
    """Time exceeding threshold"""

    params  = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    bin_size = int(params[0]*subset.fs)
    olap = params[1]
    olapn = int(round(np.floor(olap*bin_size)))
    thresh = params[2]

    d_res = af.time_exceeding_threshold(subset, bin_size, olapn, thresh)

    period = subset.n/subset.fs

    # ---- Duration over threshold
    label_1 = "Duration exceeding %.2f dB" % thresh
    unit = "seconds"
    Rpt.printResult(d_res, label_1, unit)
    Rpt.write_results(d_res, label_1, "")
    # ---- Proportion time over threshold
    label_2 = "Proportion of time exceeding %.3fdB " % thresh
    Rpt.printResult(d_res, label_2, '', lambda x: x/period)
    Rpt.write_results(d_res, label_2, "", lambda x: x/period)

def exposure(**kwargs):
    """
    Print and save results for exppsure_level
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.exposure_level(subset)
    # ---- Linear units
    label_linear = "Exposure (Cumulative Energy)"
    unit = "(" + od.ref_ascii[subset.unit_out] + ")^2 * s"
    Rpt.printResult(d_res, label_linear, unit)
    # ---- dB units
    label_dB = "Exposure level, L_{E, " + od.unit_ascii[subset.unit_out] + "} " + \
            "(re (1" + od.ref_ascii[subset.unit_out] + ")^2 * s) / dB"
    Rpt.printResult(d_res, label_dB, 'dB', lambda x: 10*np.log10(x))
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, unit)
    Rpt.write_results(d_res, label_dB, "dB", lambda x: 10*np.log10(x))

def zero_to_peak(**kwargs):
    """
    Print and save results from zero_to_peak
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.zero_to_peak(subset)
    # ---- Linear units
    label_linear = "Zero-to-peak %s" % od.unit_label[subset.unit_out]
    unit = od.ref_ascii[Rpt.unit_out]
    Rpt.printResult(d_res, label_linear, unit)
    # ---- dB units
    label_dB = "Zero-to-peak level, L_{0-pk, %s} (re (1%s)^2) / dB" % \
        (od.unit_ascii[Rpt.unit_out], od.ref_ascii[Rpt.unit_out])
    Rpt.printResult(d_res, label_dB, 'dB', lambda x: 20*np.log10(x))
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, unit)
    Rpt.write_results(d_res, label_dB, "dB", lambda x: 20*np.log10(x))

def crest_factor(**kwargs):
    """
    Print and save crest factor
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.crest_factor(subset)
    # ---- Linear units
    label_linear = "Crest factor"
    unit = od.ref_ascii[Rpt.unit_out]
    Rpt.printResult(d_res, label_linear, "")
    # ---- dB units
    label_dB = "Crest factor level, L_{cf, %s} (re (1%s)^2) / dB" % \
        (od.unit_ascii[Rpt.unit_out], od.ref_ascii[Rpt.unit_out])
    Rpt.printResult(d_res, label_dB, 'dB',
            lambda x: 20*np.log10(x))
    # ---- Save to csv
    Rpt.write_results(d_res, label_dB, "dB")

def pulse_length(**kwargs):
    """
    Print and save puls length
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.pulse_length(subset, params[0:2])
    # ---- Linear units
    label_linear = "Pulse length"
    unit = "ms"
    Rpt.printResult(d_res, label_linear, unit)
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, "ms")

def rise_time(**kwargs):
    """
    print and save rise time
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.rise_time(subset, params[0:2])
    # ---- Linear units
    label_linear = "Rise time"
    unit = "ms"
    Rpt.printResult(d_res, label_linear, unit)
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, "ms")

def rms(**kwargs):
    """
    print and save rms level
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.rms(subset)
    # ---- Linear units
    label_linear = "Root-mean-square %s" % od.unit_label[subset.unit_out]
    unit = od.ref_ascii[subset.unit_out]
    Rpt.printResult(d_res, label_linear, unit)
    # ---- dB units
    label_dB = "Root-mean-square level, L_{%s} (re (1%s)^2) / dB" % \
        (od.unit_ascii[subset.unit_out], od.ref_ascii[subset.unit_out])
    Rpt.printResult(d_res, label_dB, 'dB', lambda x: 20*np.log10(x))
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, unit)
    Rpt.write_results(d_res, label_dB, "dB", lambda x: 20*np.log10(x))

def impulse_rms(**kwargs):
    """
    print and save rms level
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.impulse_rms(subset, params)
    # ---- Linear units
    label_linear = f"Impulse root-mean-square ({params[0]}, {params[1]})"
    unit = od.ref_ascii[subset.unit_out]
    Rpt.printResult(d_res, label_linear, unit)
    # ---- dB units
    label_dB = "Impulse root-mean-square level, L_{%s} (re (1%s)^2) / dB" % \
        (od.unit_ascii[subset.unit_out], od.ref_ascii[subset.unit_out])
    Rpt.printResult(d_res, label_dB, 'dB', lambda x: 20*np.log10(x))
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, unit)
    Rpt.write_results(d_res, label_dB, "dB", lambda x: 20*np.log10(x))

def kurtosis(**kwargs):
    """
    print kurtosis
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.kurtosis(subset)
    # ---- Linear units
    label_linear = "Kurtosis"
    unit = ""
    Rpt.printResult(d_res, label_linear, unit)
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, unit)

def impulse_kurtosis(**kwargs):
    """
    Print impulse kurtosis
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']

    if not params: return

    d_res = af.impulse_kurtosis(subset)
    # ---- Linear units
    label_linear = "Impulse kurtosis"
    unit = ""
    Rpt.printResult(d_res, label_linear, unit)
    # ---- Save to csv
    Rpt.write_results(d_res, label_linear, unit)

# =============================================================================
# ======================= Frequency domain analysis ===========================
# =============================================================================
def esd(**kwargs):
    """
    Print and save esd
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating ESD...")

    d_res = af.welch(subset, window_len = params[0],
            window_type = params[1], olap = params[2], E = True)

    # ------ Save output
    filename = 'esd_%s_band-%s' % (od.unit_label[subset.unit_out],  Rpt.band)
    args = [d_res, subset]
    Rpt.printPlots(filename, ap.esd, args)
    Rpt.write_csv(filename, ['Frequency', 'ESD'], d_res)
    # ------ Show plots
    tab_name = 'ESD %s' % (od.unit_tab[subset.unit_out])
    PamPlot.plot(tab_name, apg.esd, args)

def psd(**kwargs):
    """
    print and save psd
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating PSD...")

    d_res = af.welch(subset, window_len = params[0],
            window_type = params[1], olap = params[2], E = False)

    # ------ Print plots
    filename = 'psd_%s_band-%s' % (od.unit_label[subset.unit_out], Rpt.band)
    args = [d_res, subset]
    Rpt.printPlots(filename, ap.psd, args)

    # ------ Write results
    Rpt.write_csv(filename, ['Frequency', 'PSD'], d_res)

    # ------ Show plots
    tab_name = 'PSD %s' % (od.unit_tab[subset.unit_out])
    PamPlot.plot(tab_name, apg.psd, args)

def decidecade_rms(**kwargs):
    """
    print and save decidecade rms
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating decidecade RMS...")

    d_res = af.decidecade(subset, E = False)

    filename = 'dd_rms_%s_band-%s' % (od.unit_label[subset.unit_out], Rpt.band)

    # ------ Write results
    Rpt.write_csv(filename, ['Band', "dB"], d_res, transpose = True)

    # ------ Print plots
    args = [d_res, subset]
    Rpt.printPlots(filename, ap.decidecade_rms, args)

    # ------ Show plots
    name = 'DD-rms ' + od.unit_tab[subset.unit_out]
    PamPlot.plot(name, apg.decidecade_rms, args)

def decidecade_exposure(**kwargs):
    """
    print and save decidecade exposure
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating decidecade exposure...")

    d_res = af.decidecade(subset, E = True)

    filename = 'dd_exposure_%s_band-%s' % \
        (od.unit_label[subset.unit_out], Rpt.band)

    # ------ Write file
    Rpt.write_csv(filename, ['Band', 'dB'], d_res, transpose = True)

    # ------ Print plots
    args = [d_res, subset]
    Rpt.printPlots(filename, ap.decidecade_exposure, args)

    # ------ Show plots
    name = 'DD-exposure ' + od.unit_tab[subset.unit_out]
    PamPlot.plot(name, apg.decidecade_exposure, args)

def spectrogram(**kwargs):
    """
    print and save spectrogram
    """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating spectrogram")

    d_res = af.spectrogram(subset, window_len = params[0],
            window_type = params[1], olap = params[2])

    # Loop channels
    for key, v in d_res.items():

        # ------ Save output
        args = [d_res[key], subset]
        filename = 'spectrogram_%s_band-%s_rangeID-%s_chan-%s' % \
            (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, key)
        Rpt.printPlots(filename, ap.spectrogram, args)

        # ------ Write spectrogram to disk
        aw.write_spectrogram(Rpt, key, v)

        # ------ Show plots
        args = [d_res[key], subset, params[0], params[2]]
        tab_name = f'Spec {od.unit_tab[subset.unit_out]}{od.chan_tab[key]}'
        PamPlot.plot(tab_name, apg.spectrogram, args)

def spectral_probability_density(**kwargs):
    """ Spectral Probability Density """

    params = kwargs['params']
    subset = kwargs['subset']
    Rpt = kwargs['Rpt']
    PamPlot = kwargs['PamPlot']

    if not params: return

    Rpt.pamPrint("------ Generating SPD")

    d_res, d_res_prcnt = af.spectralProbabilityDensity(
        subset, params[0], params[1], params[2],
        params[3], params[4])

    # ------ Write SPD array
    debug("writing SPD array")
    aw.write_spd(Rpt, d_res)

    # ------ Write PSD percentiles
    debug("writing SPD percentiles")
    prct_str = list(map(str, params[4]))
    Rpt.write_csv('PSDPercentiles', ("Frequency", *prct_str), d_res_prcnt)

    # ------ Save plot
    for key in d_res.keys():
        args = [d_res[key], d_res_prcnt[key], subset, prct_str]
        filename = 'SPD_%s_band-%s_rangeID-%s_chan-%s' % \
            (od.unit_label[subset.unit_out], Rpt.band, Rpt.rng_id, key)
        Rpt.printPlots(filename, ap.spectralProbabilityDensity, args)

    # ------ Show plots
    debug("Showing SPD plots")
    for chan in d_res.keys():
        debug(f'channel: {chan}')
        args = [subset, d_res[chan], d_res_prcnt[chan], params[4]]
        tab_name = f'SPD {od.unit_tab[subset.unit_out]}{od.chan_tab[chan]}'
        PamPlot.plot(tab_name, apg.spectralProbabilityDensity, args)
