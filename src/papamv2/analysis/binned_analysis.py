"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------
"""

import numpy as np
import scipy.signal as sig

from papamv2.debug import debug

def binned_analysis(subset, bin_size, olapn, fcn,
                    sum_samples = False, sum_agg = False, trns = None):
    """
    Subdivide signal into overlaping bins and calculate result for each bin.
    """

    # Calculate bins to analyze
    starts = np.arange(start = 0, stop = subset.n - bin_size,
                       step = bin_size - olapn)

    # Funtion for binning signal
    def fun_outer(signal):
        # output array (calculated binned values)
        # colums are: seconds, analysis result
        out = list()

        # Loop bins
        for idx, start in enumerate(starts):
            # Get indicies for subset
            samples = range(start, (start + bin_size))
            # apply analysis to subset
            val = fcn(signal[samples])
            time = (start + (bin_size/2)) / subset.fs

            # Copy time values (some analyses can return multiple values per bin)
            if type(val) is np.ndarray:
                time2 = np.ones((val.shape[0], 1))*time
            else:
                time2 = np.array([time])

            out.append(np.hstack((time2, val)))

        return np.vstack(tuple(out))

    d_res = subset.map(fun_outer, sum_samples = sum_samples,
                       sum_agg = sum_agg, trns = trns)

    return d_res
