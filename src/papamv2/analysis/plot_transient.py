"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Misc functions related to presentation of results.
"""

import numpy as np
import scipy.signal as sig
from matplotlib import figure
import pyqtgraph as pyqt

from papamv2.analysis import plots as pl
from papamv2 import helpers_pyqtgraph as pyqth
from papamv2.helpers_pyqtgraph import \
    make_plot, make_layout, make_colorBar, setLabel, make_plotItem

from papamv2 import outputDictionaries as od
from papamv2.debug import debug

def plot_transient(parameters, subset, Rpt, PamPlot):
    Rpt.pamPrint("Plotting Transient...")

    for i, chan in enumerate(subset.channels):

        # Only plot pressure and propagation channels
        if chan in "pi":

            y = subset.data[i]
            offset = subset.rng.start
            fs = subset.fs
            unit_out = Rpt.unit_out
            x = np.array(range(offset, offset + len(y)))/fs

            # ------ Print plot
            filename = "Transient_%s_%s_rangeID-%s_%s_fs-%i" %\
                    (od.unit_label[Rpt.unit_out], Rpt.band, Rpt.rng_id, chan, Rpt.fs)
            args = [parameters, x, y, fs, unit_out]
            Rpt.printPlots(filename, plot_transient_matplotlib, args)

            # ------ Show plot
            PamPlot.plot(f"Trns {od.unit_tab[unit_out]}{od.chan_tab[chan]}", plot_transient_pyqt, args)

def plot_transient_matplotlib(parameters, x , y, fs, unit_out):
    """
    Plot a transient waveform with annotations
    """

    # ------ Setup figure and axis
    fig = figure.Figure()
    ax = fig.add_subplot()

    # ------ Waveform
    pl.rangify(ax, x, y, color='C1', alpha=1, label = "Waveform")
    ax.set_xlabel("Time (seconds)")
    ax.set_ylabel("$\\mathrm{%s}$" % od.ref_tex[unit_out])
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)

    # ------ Cumul Energy
    ax2 = ax.twinx()
    E = np.cumsum(y**2)/fs
    ax2.plot(x, E, color='C3', label="Cumul. Energy")
    ylab = "Cumul. Energy $\\mathrm{(%s)}^2 \\cdot s}$" % od.ref_tex[unit_out]
    ax2.set_ylabel(ylab)

    # ------ Pulse length (optional)
    if parameters['pulse_length']:
        # Calculate start and end of transient
        E_lower = E[-1] * parameters['pulse_length'][0]
        E_upper = E[-1] * parameters['pulse_length'][1]
        i_start = np.where(E >= E_lower)[0][0]
        i_end = np.where(E >= E_upper)[0][0]
        ax.axvline((i_start/fs) + x[0], color='C4')
        ax.axvline((i_end/fs) + x[0], color='C4', label='Pulse length')
    # ------ Rise Time (optional)
    if parameters['rise_time']:
        R = np.max(np.abs(y))
        # Calculate start and end of transient
        R_lower = R * parameters['rise_time'][0]
        R_upper = R * parameters['rise_time'][1]
        i_start = np.where(np.abs(y) >= R_lower)[0][0]
        i_end = np.where(np.abs(y) >= R_upper)[0][0]
        ax.axvline((i_start/fs) + x[0], color='C5')
        ax.axvline((i_end/fs) + x[0], color='C5', label='Rise time')
    ax.legend(loc='lower right')

    return fig

def addRiseTime_pyqt(pw, y, parameters, fs, x, legend = False):
    debug("plot widget - rise time")

    E = np.cumsum(y**2)

    # Calculate start and end of transient
    E_lower = E[-1] * parameters['pulse_length'][0]
    E_upper = E[-1] * parameters['pulse_length'][1]
    i_start = np.where(E >= E_lower)[0][0]
    i_end = np.where(E >= E_upper)[0][0]

    # Make line objects
    col = od.chan_color['y']
    pen = pyqt.mkPen(col, antialias = True)
    il1 = pyqt.InfiniteLine(pos = (i_start/fs) + x[0], angle=90, pen = pen)
    il2 = pyqt.InfiniteLine(pos = (i_end/fs) + x[0], angle=90, pen = pen)

    # Add to plot
    pw.addItem(il1)
    pw.addItem(il2)
    if legend:
        pw.addLegend().addItem(
            pyqt.PlotDataItem([], pen = pen),
            name = "Pulse Length")

def addPulseLength_pyqt(pw, y, parameters, fs, x, legend = False):
    debug("plot widget - pulse length")
    R = np.max(np.abs(y))
    # Calculate start and end of transient
    R_lower = R * parameters['rise_time'][0]
    R_upper = R * parameters['rise_time'][1]
    i_start = np.where(np.abs(y) >= R_lower)[0][0]
    i_end = np.where(np.abs(y) >= R_upper)[0][0]

    col = od.chan_color['z']
    pen = pyqt.mkPen(col)
    il1 = pyqt.InfiniteLine(pos = (i_start/fs) + x[0], angle=90, pen = pen)
    il2 = pyqt.InfiniteLine(pos = (i_end/fs) + x[0], angle=90, pen = pen)
    pw.addItem(il1)
    pw.addItem(il2)
    if legend:
        pw.addLegend().addItem(
            pyqt.PlotDataItem([], pen = pen),
            name = "Rise Time")

def plot_transient_pyqt(parameters, x , y, fs, unit_out):
    """
    Plot a transient waveform with annotations
    """

    layout = make_layout()

    # ------ Make plot Widget for waveform
    debug("plot widget - waveform")
    pw1 = make_plotItem(legend = False, grid = True)
    layout.addItem(pw1, row = 0, col = 0)
    col = od.chan_color['x']
    pyqth.rangify(pw1, x, y, color = col)
    # Add axis labels
    ylab = '%s (<math>%s</math>)' % (od.unit_label[unit_out], od.ref_html[unit_out])
    setLabel(pw1, 'left', ylab)
    # set axis limits
    pw1.setLimits(xMin = x[0], xMax = x[x.size - 1])


    # ------ Make plot Widget for Exposure
    debug("plot widget - exposure")
    pw2 = make_plotItem(legend = False, grid = True)
    layout.addItem(pw2, row = 1, col = 0)
    pen = pyqt.mkPen(col, antialias = True)
    E = np.cumsum((y**2)/fs)
    # subsample points if too many (will cause segfault in display)
    subsample = 1000  # maximum 10000 points to display
    if E.size > subsample:
        step = int(np.floor(E.size/subsample))
        x_E = x[0:E.size:step]
        y_E = E[0:E.size:step]
    else:
        x_E = x
        y_E = E
    # Add line to plot
    pw2.addItem(pyqt.PlotDataItem(x = x_E, y = y_E, pen = pen))
    # Add axis labels
    setLabel(pw2, 'bottom','Time (seconds)')
    ylab = 'Exposure (<math>(%s)<sup>2</sup>&#183;s</math>)' % (od.ref_html[unit_out])
    setLabel(pw2, 'left', ylab)
    # Link x axis to above plot
    pw2.setXLink(pw1)
    pw2.setLimits(xMin = x[0], xMax = x[x.size - 1])

    # ------ Pulse length (optional)
    if parameters['pulse_length']:
        addRiseTime_pyqt(pw1, y, parameters, fs, x, legend = True)
        addRiseTime_pyqt(pw2, y, parameters, fs, x, legend = True)

    # ------ Rise Time (optional)
    if parameters['rise_time']:
        addPulseLength_pyqt(pw1, y, parameters, fs, x, legend = True)
        addPulseLength_pyqt(pw2, y, parameters, fs, x, legend = True)


    return layout
