import numpy as np
import os

def write_spectrogram(Rpt, chan, v):
    # ------ Write spectrogram to disk
    if Rpt.no_output: return

    filename = '%s_spectrogram_%s_band-%s_chan-%s.csv' % \
        (Rpt.filestamp, Rpt.unit_out, Rpt.band, chan)

    with open(os.path.join(Rpt.output_data, filename), 'w') as fid:
        # --- write headers
        fid.write("Time, Frequency, PSD\n")

        fr = v[1]
        tm = v[0]
        p = v[2].reshape(v[2].size)

        for r in range(0, p.size):
            tm_i = r % tm.size
            fr_i = int(np.floor(r / tm.size))
            fid.write(f'{tm[tm_i]}, {fr[fr_i]},{p[r]}\n')

def write_spd(Rpt, d_res):

    if Rpt.no_output: return

    for chan, res in d_res.items():
        # ====== Write SPD array
        filename = '%s_spectralProbabilityDensity_%s_band-%s_chan-%s.csv' % \
            (Rpt.filestamp, Rpt.unit_out, Rpt.band, chan)
        with open(os.path.join(Rpt.output_data, filename), 'w') as fid:
            # --- Write headers
            fid.write("Frequency, PSD, SPD\n")
            # --- Write data
            for r in range(0, res[2].size):
                pr_i = r % res[1].size
                fr_i = int(np.floor(r / res[1].size))
                fid.write('%s, %s, %s\n' % \
                          (res[0][fr_i], res[1][pr_i], res[2][fr_i, pr_i]) )

