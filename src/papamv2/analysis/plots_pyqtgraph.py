"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions holding code for plotting each analysis feature in paPAM.
Function inputs are the results dictionaryes from the module
analysisFeatures.py.

Each function returns a list of plot arguments/values which are used to run
and save plots in the main paPAMv2 module.

Metadata for each analysis is taken from the signal subset parameter.
"""

import numpy as np

from PyQt5 import QtGui
import pyqtgraph as pqg

from papamv2 import outputDictionaries as od
from papamv2.helpers_pyqtgraph import \
    make_plot, make_layout, make_plotItem, \
    make_colorBar, setLabel, rangify, grey1, tomato

from papamv2.debug import debug


def waveform(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    # pw = make_plot(legend = True, grid = True)

    layout = make_layout()

    offset = subset.rng.start

    ylab = "Waveform (<math>%s</math>)" % \
        (od.ref_html[subset.unit_out])

    # ------ Plot waveform for each channel
    i = 0
    for chan, data in d_res.items():

        pw = make_plotItem(grid = True)
        layout.addItem(pw, row = i, col = 1)

        if i == 0:
            pw1 = pw
        else:
            pw.setXLink(pw1)
        # ---- Channel plots
        seconds = np.array(range(offset + 0, offset + data.size),
                dtype='f')/subset.fs
        # pw.plot(seconds, data, pen = pqg.mkPen(od.chan_color[chan]))
        rangify(pw, seconds, data, color = od.chan_color[chan])

        setLabel(pw, 'left', od.chan_ascii[chan])

        # ------ Restrict range
        lim = max(abs(data))
        pw.setLimits(xMin = seconds[0], xMax = seconds[-1],
                     yMin = -lim, yMax = lim)

        i += 1

    layout.addLabel(ylab, row = 0, col = 0, rowspan = i, angle = -90,
                    color = "#333")
    layout.addLabel("Time (seconds)", row = i, col = 1,
                    color = "#333")

    return layout

def analytic_envelope(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # layout = pqg.GraphicsLayoutWidget()
    # layout.setBackground('w')
    layout = make_layout()

    offset = subset.rng.start

    ylab = '<math>L<sub>q,%s</sub> (re (1 %s)<sup>2</sup>)</math> / dB' % \
    (od.unit_html[subset.unit_out], od.ref_html[subset.unit_out])

    # ------ Plot waveform for each channel
    i = 0
    for chan, data in d_res.items():

        # pw = layout.addPlot(row = i, col = 0)
        pw = make_plotItem(legend = False, grid = True)
        layout.addItem(pw, row = i, col = 1)
        if i == 0:
            pw1 = pw
        else:
            pw.setXLink(pw1)

        # ---- Channel plots
        seconds = np.array(range(offset + 0, offset + data.size),
                dtype='f')/subset.fs
        # pw.plot(seconds, data, pen = pqg.mkPen(od.chan_color[chan]))
        rangify(pw, seconds, 20*np.log10(data),
                color = od.chan_color[chan])

        setLabel(pw, 'left', od.chan_ascii[chan])

        # ------ Restrict range
        pw.setLimits(xMin = seconds[0], xMax = seconds[-1],
                     yMin = min(20*np.log10(data)),
                     yMax = max(20*np.log10(data)))

        i += 1

    layout.addLabel(ylab, row = 0, col = 0, rowspan = i, angle = -90,
                    color = "#333")
    layout.addLabel("Time (seconds)", row = i, col = 1,
                    color = "#333")

    return layout

def binned_kurtosis(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Plot channels
    for chan, data in d_res.items():
        # Build plot objects
        pw.plot(data[:,0], data[:,1], name = chan,
                pen = pqg.mkPen(od.chan_color[chan]))

    # ------ Restrict range
    pw.setLimits(xMin = min(data[:,0]), xMax = max(data[:,0]))

    ylab = 'Kurtosis'

    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    return pw

def binned_rms(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Plot channels
    for chan, data in d_res.items():
        # Build plot objects
        pw.plot(data[:,0], data[:,1], name = chan,
                pen = pqg.mkPen(od.chan_color[chan]))

    # ------ Restrict range
    pw.setLimits(xMin = min(data[:,0]), xMax = max(data[:,0]))

    ylab = '<math>' + \
        f'L<sub>{od.unit_html[subset.unit_out]}</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>) / dB' + \
        '</math>'

    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    return pw

def binned_zeroToPeak(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Plot channels
    for chan, data in d_res.items():
        # Build plot objects
        pw.plot(data[:,0], data[:,1], name = chan,
                pen = pqg.mkPen(od.chan_color[chan]))

    # ------ Restrict range
    pw.setLimits(xMin = min(data[:,0]), xMax = max(data[:,0]))

    ylab = '<math>' + \
        f'L<sub>pk,{od.unit_html[subset.unit_out]}</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>) / dB' + \
        '</math>'

    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    return pw

def binned_crestFactor(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Plot channels
    for chan, data in d_res.items():
        # Build plot objects
        pw.plot(data[:,0], data[:,1], name = chan,
                pen = pqg.mkPen(od.chan_color[chan]))

    # ------ Restrict range
    pw.setLimits(xMin = min(data[:,0]), xMax = max(data[:,0]))

    ylab = '<math>' + \
        f'L<sub>cf,{od.unit_html[subset.unit_out]}</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>) / dB' + \
        '</math>'

    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', ylab)

    return pw

def esd(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array or results for each channel (frequency, ESD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Loop channels
    for key in d_res.keys():
        x = d_res[key][0]
        y = 10*np.log10(d_res[key][1])
        pw.plot(x, y, name=key,
                pen = pqg.mkPen(od.chan_color[key], size = 1.5))

    # Add axis labels
    ylab = '<math>' + \
        f'L<sub>E,{od.unit_html[subset.unit_out]},f</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>' + \
        '&middot;s&middot;Hz<sup>-1</sup>) / dB' + \
        '</math>'
    # Add axis labels
    setLabel(pw, 'bottom','Frequency (Hz)')
    setLabel(pw, 'left', ylab)

    # ------ Restrict range
    pw.setLimits(xMin = subset.band['highpass'], xMax = subset.band['lowpass'])
    pw.setRange(xRange = (subset.band['highpass'], subset.band['lowpass']))

    return pw

def psd(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array of results for each channel (frequeny, PSD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Loop channels
    for key in d_res.keys():
        x = d_res[key][0]
        y = 10*np.log10(d_res[key][1])
        pw.plot(x, y, name=key, pen = pqg.mkPen(od.chan_color[key], size = 1.5))

    ylab = '<math>' + \
        f'L<sub>{od.unit_html[subset.unit_out]},f</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>' + \
        '&middot;Hz<sup>-1</sup>) / dB' + \
        '</math>'
    # Add axis labels
    setLabel(pw, 'bottom','Band (Hz)')
    setLabel(pw, 'left', ylab)

    # ------ Restrict range
    pw.setLimits(xMin = subset.band['highpass'], xMax = subset.band['lowpass'])
    pw.setRange(xRange = (subset.band['highpass'], subset.band['lowpass']))

    return pw

def decidecade_exposure(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array or results for each channel (frequency, ESD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Restrict range
    pw.setLimits(xMin = 0, xMax = subset.fs/2)
    pw.setRange(xRange = (subset.band['highpass'], subset.band['lowpass']))

    # ------ Loop channels
    for key in d_res.keys():
        x = d_res[key][:,0]
        y = d_res[key][:,1]
        pw.plot(x, y, name=key, pen=pqg.mkPen(od.chan_color[key], size = 1.5))
        pw.setLogMode(x = True)

    ylab = '<math>' + \
        f'L<sub>E,{od.unit_html[subset.unit_out]}</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})' + \
        '<sup>2</sup>&middot;s) / dB' + \
        '</math>'
    setLabel(pw, 'bottom','Decidecade Band (Hz)')
    setLabel(pw, 'left', ylab)


    return pw

def decidecade_rms(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array or results for each channel (frequency, ESD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # Make plotWidget
    pw = make_plot(legend = True, grid = True)

    # ------ Restrict range
    pw.setLimits(xMin = 0, xMax = subset.fs/2)
    pw.setRange(xRange = (subset.band['highpass'], subset.band['lowpass']))

    # ------ Loop channels
    for key in d_res.keys():
        # Make plot
        x = d_res[key][:,0]
        y = d_res[key][:,1]
        pw.plot(x, y, name=key, pen=pqg.mkPen(od.chan_color[key], size = 1.5))
        pw.setLogMode(x = True)

    # Add axis labels
    ylab = '<math>' + \
        f'L<sub>{od.unit_html[subset.unit_out]}</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>) / dB' + \
        '</math>'
    setLabel(pw, 'left', ylab)
    setLabel(pw, 'bottom', 'Decidecade Band (Hz)')

    return pw

def spectrogram(res, subset, window_len, olap):
    """
    Parameters
    ----------
    res: numpy.array
        Array of results for a channel (frequency, time, PSD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return:
    ------
    A figure object
    """

    layout = make_layout()

    pw = layout.addPlot(row=0, col=0)

    # Make plotWidget
    #pw = make_plot(legend = False, grid = False)
    #layout.addItem(pw)

    debug(f"subset.band: {subset.band}")
    debug(f"res: {res}")

    # Make subset of bandpassed data
    idx_band = [ i for (i,f) in enumerate(res[1]) \
                if f >= subset.band['highpass'] and \
                  f <= subset.band['lowpass'] ]
    debug(f"idx_band: {idx_band}")
    debug(f"res[3].shape: {res[2].shape}")

    spec_band = res[2][idx_band[0]:idx_band[-1],:]
    debug(f"spec_band: {spec_band}")


    # --- make image
    offset = subset.rng.start/(window_len*olap)
    img = pqg.ImageItem(image = res[2].transpose())
    # --- set transformation for proper axis units
    tr = QtGui.QTransform()
    tr.scale((olap*window_len)/subset.fs, subset.fs/(window_len))
    tr.translate(offset, 0)
    img.setTransform(tr)
    pw.addItem(img)

    # ------ Restrict view range
    x_range = (subset.rng.start/subset.fs, subset.rng.stop/subset.fs)
    pw.setLimits(xMin = x_range[0], xMax = x_range[1],
                 yMin = subset.band['highpass'],
                 yMax = subset.band['lowpass'])
    pw.setRange(yRange = (subset.band['highpass'], subset.band['lowpass']))

    # ------ Colorbar
    clab = '<math>' + \
        f'L<sub>{od.unit_html[subset.unit_out]},f</sub>' + \
        f' (re (1{od.ref_html[subset.unit_out]})<sup>2</sup>' + \
        '&middot;Hz<sup>-1</sup>) / dB' + \
        '</math>'
    lims = (round(res[2].min()), round(res[2].max()))
    vals = (round(spec_band.min()),
            round(spec_band.max()))
    debug(f"vals {vals}")
    make_colorBar(vals, clab, img, pw, values = vals)

    # ------ Axis labels
    setLabel(pw, 'bottom','Time (seconds)')
    setLabel(pw, 'left', 'Frequency (Hz)')


    return layout

def spectralProbabilityDensity(subset, res, res_prcnt, prcnt):
    """ Plot SPD with overlayed psd percentiles """

    layout = make_layout()
    pw = layout.addPlot(row=0, col=0)

    # --- make image
    img = pqg.ImageItem(image = res[2])
    # --- set transformation for proper axis units
    tr = QtGui.QTransform()
    scale_freq = subset.fs/(2*res[0].size)
    scale_psd = (res[1][-1] - res[1][0]) / res[1].size
    trans_psd = res[1][0]
    tr.scale(scale_freq, scale_psd)
    tr.translate(0, trans_psd)
    img.setTransform(tr)
    pw.addItem(img)

    # Get ranges
    debug(f'res[0] {res[0]}')
    idx_f = [ i for (i, f) in enumerate(res[0]) \
             if f >= subset.band['highpass'] and \
                 f <= subset.band['lowpass'] ]
    debug(f'idx_f {idx_f}')
    idx_p = [i for i in range(0, res[1].size) \
             if sum(res[2][idx_f, i]) > 0 ]
    debug(f'idx_p {idx_p}')

    # ------ Restrict viewbox
    pw.setLimits(xMin = subset.band['highpass'],
                  xMax = subset.band['lowpass'],
                  yMin = res[1][idx_p[0]],
                  yMax = res[1][idx_p[-1]])
    pw.setRange(xRange = (subset.band['highpass'], subset.band['lowpass']),
                yRange = (idx_p[0], idx_p[-1]))

    # ------ Colorbar
    clab = 'Density'
    lims = (res[2].min(), res[2].max())
    r = -round(np.log10(lims[1] - lims[0])) + 2
    lims = (round(lims[0], r), round(lims[1], r))
    debug(f"lims {lims}")
    make_colorBar(lims, clab, img, pw, rounding = 10**(-r))

    # ------ Axis labels
    ylab = '<math>' + \
        f'L<sub>{od.unit_html[subset.unit_out]},f</sub>' + \
        f' (re (1 {od.ref_html[subset.unit_out]})<sup>2</sup>' + \
        '&middot;Hz<sup>-1</sup>) / dB'
    setLabel(pw, 'bottom','Frequency (Hz)')
    setLabel(pw, 'left', ylab)

    if res_prcnt is not None:
        # ------ Enable legend
        leg = pw.addLegend(labelTextColor = grey1)
        leg.setBrush('w')
        leg.setPen('grey')
        # ------ Draw percentiles
        pen = pqg.mkPen(tomato)
        res_prcnt = res_prcnt.T
        for col in range(1, res_prcnt.shape[1]):
            pw.plot(res_prcnt[:, 0], res_prcnt[:,col],
                    name = prcnt[col - 1], pen = pen)

    return layout

