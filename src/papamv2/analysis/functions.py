"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions holding code for each analysis function in paPAM.  The code for
reporting and saving the outputs of these functions in paPAM is found in
papamv2.analysis.wrappers.

Each function takes a VectorData.subset as an argument and outputs a dictionary
which holds the numeric results for each channel.
"""

import numpy as np
import scipy.signal as sig

from papamv2.analysis.binned_analysis import binned_analysis

from papamv2.debug import debug

def waveform(subset):
    """
    Return calibrated waveform of subset.  This can be plotted and saved to a
    little-endian float binary file.

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the wavform (1D numpy array) for each channel.
    """

    d_res = dict()

    # Extract wavforms for each channel
    for i, chan in enumerate(subset.channels):
        # Get channel name
        # make filename

        d_res[chan] = subset.data[i]

    return d_res

def analytic_envelope(subset):
    """
    Return analytic signal (magnitude) of subset.  This can be plotted and
    saved to a little-endian float binary file.

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    d_res = dict()

    # Extract envelopes for each channel
    for i, chan in enumerate(subset.channels):

        d_res[chan] = abs(sig.hilbert(subset.data[i]))

    return d_res

def binned_kurt(subset, bin_size, olapn):
    """
    Binned kurtosis
    """


    # Set analysis function
    def fun(signal):
        t = (0, signal.size/subset.fs)
        dt = 1/subset.fs # seconds per sample
        num = sum((signal**4) * dt)
        dnm =  sum((signal**2) * dt) ** 2
        beta = (t[1] - t[0]) * num/dnm
        return beta

    d_res = binned_analysis(subset, bin_size, olapn, fun)

    return d_res

def binned_rms(subset, bin_size, olapn):
    def fun(signal):
        return np.mean(signal**2)**0.5

    d_res = binned_analysis(subset, bin_size, olapn, fun, sum_agg = True,
                            trns = lambda x: 20*np.log10(x))

    return d_res

def binned_zeroToPeak(subset, bin_size, olapn):
    def fun(signal):
        return np.max(np.abs(signal))

    d_res = binned_analysis(subset, bin_size, olapn, fun, sum_samples = True,
                            trns = lambda x: 20*np.log10(x))

    return d_res

def binned_crestFactor(subset, bin_size, olapn):
    def fun(signal):
        return 20*np.log10(
            np.max(np.abs(signal) /
                   np.mean(signal**2)**0.5))

    d_res = binned_analysis(subset, bin_size, olapn, fun)

    return d_res

def binned_decidecadeRms(subset, bin_size, olapn):
    def fun(signal):
        ret = decidecade_calc(signal, subset.fs, subset.n, False)
        return ret.T

    d_res = binned_analysis(subset, bin_size, olapn, fun, sum_agg = True,
                            trns = lambda x: 20*np.log10(x))

    return d_res

def time_exceeding_threshold(subset, bin_size, olapn, thresh):
    """
    Subdivide signal into overlaping bins and calculate result for each bin.
    """

    # Calculate binned rms
    d_res = binned_rms(subset, bin_size, olapn)

    # Calculate total time over threshold per channel from bin arrays
    for key in d_res:
        prop = sum(d_res[key][1] >= thresh)/len(d_res[key][1])
        d_res[key] = subset.n*prop/subset.fs

    return d_res

def exposure_level(subset):
    """
    Calculate sound expose levels

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate Cumul energy (exposure)
    dt = 1/subset.fs
    d_res = subset.map(lambda x: sum((x**2)*dt), sum_agg=True)

    return d_res

def zero_to_peak(subset):
    """
    Calculate sound expose levels

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate Cumul energy (exposure)
    d_res = subset.map(lambda x: np.max(np.abs(x)), sum_samples=True)

    return d_res

def crest_factor(subset):
    """
    Calculate sound exposure

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate Cumul energy (exposure)
    d_res = subset.map(lambda x: np.max(np.abs(x)) / np.mean(x**2)**0.5)

    return d_res

def pulse_length(subset, limits):
    """
    Calculate pulse length

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    limits: (float, float)
        The energy percentiles to calculate the period between.

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate
    def p_len(x):
        # Calc unscaled exposure
        E = np.cumsum(x**2)

        # Mark start and end points
        start = np.where(E >= E[-1]*limits[0])[0][0]
        end = np.where(E >= E[-1]*limits[1])[0][0]

        # return value
        return (end - start)*1e3 / subset.fs

    d_res = subset.map(p_len)

    return d_res

def rise_time(subset, limits):
    """
    Calculate rise time

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    limits: (float, float)
        The energy percentiles to calculate the period between.

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate
    def r_t(x):
        # Calc exposure
        peak = max(abs(x))

        # Mark start and end points
        start = np.where(abs(x) >= peak*limits[0])[0][0]
        end = np.where(abs(x) >= peak*limits[1])[0][0]

        # return value
        return (end - start)*1e3 / subset.fs

    d_res = subset.map(r_t)

    return d_res

def rms(subset):
    """
    Calculate rms levels

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    d_res = subset.map(lambda x: (np.mean(x**2))**0.5, sum_agg=True)

    return d_res

def impulse_rms(subset, limits):
    """
    Calculate rms levels

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate
    def irms(x):
        # Calc exposure
        E = np.cumsum(x**2)

        # Mark start and end points
        start = np.where(E >= E[-1]*limits[0])[0][0]
        end = np.where(E >= E[-1]*limits[1])[0][0]

        # return value
        return (np.mean(x[start:end]**2))**0.5


    d_res = subset.map(irms)

    return d_res

def kurtosis(subset):
    """
    Calculate kurtosis

    Reference:
        Müller, R. A. J., Benda-Beckmann, A. M. von, Halvorsen, M. B. and
    Ainslie, M. A. (2020). Application of kurtosis to underwater sound.
    J Acoust Soc Am 148, 780–792.

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Start and stop times
    t = (subset.rng.start/subset.fs, subset.rng.stop/subset.fs)
    dt = 1/subset.fs # seconds per sample

    def kurt(signal):

        num = sum((signal**4) * dt)
        dnm =  sum((signal**2) * dt) ** 2

        beta = (t[1] - t[0]) * num/dnm

        return beta

    d_res = subset.map(kurt)

    return d_res

def impulse_kurtosis(subset):
    """
    Calculate impulse kurtosis

    Reference:
        Müller, R. A. J., Benda-Beckmann, A. M. von, Halvorsen, M. B. and
    Ainslie, M. A. (2020). Application of kurtosis to underwater sound.
    J Acoust Soc Am 148, 780–792.

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Start and stop times
    dt = 1/subset.fs # seconds per sample

    def pulse_kurt(signal):

        num = sum((signal**4) * dt)
        dnm =  sum((signal**2) * dt) ** 2

        beta =  num/dnm

        return beta

    d_res = subset.map(pulse_kurt)

    return d_res

def transient_rms_level(subset, limits):
    """
    Calculate rms levels

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    limits: (float, float)
        Start and end points in proportion of cumulative energy.

    Return
    ------
    A dictionary object holding the analytic envelope (1D numpy array) for
    each channel.
    """

    # Calculate
    def trns_rms(x):
        # Calc exposure
        E = np.cumsum(x**2)

        # Mark start and end points
        start = np.where(E >= E[-1]*limits[0])[0][0]
        end = np.where(E >= E[-1]*limits[1])[0][0]

        # Return RMS in pulse length
        return np.mean(x[start:end]**2)**0.5

    d_res = subset.map(trns_rms)

    return d_res

# =============================================================================
# ======================= Frequency domain analysis ===========================
# =============================================================================

def welch(subset, window_len, window_type, olap, E = False):
    """
    Calculate Power or Energy Spectral Density

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    window_len: int
        Window length of fft.
    window_type: str
        Window type for spectrum.  Typically 'hamming' or 'hann'.
    olap: float
        Proportion of overlap for window segments.  Value between [0, 1).
    E: boolean
        When true, return the energy spectrum, as opposed to the power
        spectrum.


    Return
    ------
    A dict holding an array of the sample frequncies and the mean, or summed,
    spectrum values for each channel.
    """

    # Mapping function
    def fcn(x):
        _, psd = sig.welch(
                x,
                fs=subset.fs,
                nperseg=window_len,
                window=window_type,
                noverlap=np.floor(window_len*olap),
                scaling='density')
        # Convert power to energy
        # (multiple by signal duration)
        if E:
            # Return ESD
            dt = 1 / subset.fs
            s = subset.n/subset.fs
            return (psd*s)**0.5
        # Return PSD
        return psd**0.5
        # returned values are scaled to root power so they can be summed

    # Run analysis on each channel
    d_res = subset.map(fcn, sum_agg=True)

    for key in d_res.keys():
        p = d_res[key]**2  # convert back to power values
        f = np.arange(0, len(p))*subset.fs/window_len
        d_res[key] = np.array([f, p])

    return d_res

def decidecade_calc(x, fs, n, E = False):

    # ------ calculate decidecade bin indexes
    i_max = np.floor(np.log10(fs/2)*10 - 0.5)
    i_min = 15  # arbirarily chosen so we get about 6 samples in the lowest bin

    # get window lenmgth
    wl = min(fs, x.size)
    scale = wl/fs

    i_rng = np.arange(i_min, i_max + 1)
    bin_lower = np.ceil(scale * 10**((i_rng-0.5)/10)).astype('int')
    bin_mid = np.ceil(scale * 10**(i_rng/10)).astype('int')
    bin_upper = np.floor(scale * 10**((i_rng+0.5)/10)).astype('int')

    debug(f'bin_lower: {bin_lower}')
    debug(f'bin_mid: {bin_mid}')
    debug(f'bin_upper: {bin_upper}')
    # ------ calculate values
    _, spec = sig.welch(
            x,
            fs=fs,
            nperseg=wl,
            window="hann",
            noverlap=np.floor(wl * 0.5),
            scaling='spectrum')
    # Convert power to energy
    # (multiple by signal duration)
    if E:
        # Return Exposure
        dt = 1 / fs
        s = n/fs
        spec = spec*s

    # sum within frequency bins
    dd_spec = np.zeros((bin_mid.size,))
    for i in range(0, dd_spec.size):
        dd_spec[i] = sum(spec[bin_lower[i]:bin_upper[i]])**0.5
        # Convert returned values to root-power so they can be vector summed

    # Return array of results
    return np.array((bin_mid/scale, np.array(dd_spec)))

def decidecade(subset, E = False):
    """
    Calculate Power or Energy over decidecade bands.

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    E: boolean
        When true, return the energy spectrum, as opposed to the power
        spectrum.

    Return
    ------
    A dict holding an array of the sample frequncies and the mean, or summed,
    spectrum values for each channel.
    """

    # Run analysis on each channel
    d_res = subset.map(
        lambda x: decidecade_calc(
            x, subset.fs, subset.n, E).T,
        sum_agg=True, trns = lambda x: 20*np.log10(x))

    return d_res

def spectrogram(subset, window_len, window_type, olap):
    """
    Calculate Spectrogram

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    window_len: int
        Window length of fft.
    window_type: str
        Window type for spectrum.  Typically 'hamming' or 'hann'.
    olap: float
        Proportion of overlap for window segments.  Value between [0, 1).


    Return
    ------
    A dict holding an array of the sample frequncies and the mean, or summed,
    spectrum values for each channel.
    """

    # Convert overlap to samples
    nolap = np.floor(window_len*olap)

    # Mapping function
    def fcn(x):
        _, _, psd = sig.spectrogram(
                x, fs=subset.fs, nperseg=window_len, window=window_type,
                noverlap=nolap, scaling='density')
                # Linearize results into 3 x n array
        return psd**0.5

    # Run analysis on each channel
    d_res = subset.map(fcn, sum_agg=True, rast = True,
                       trns = lambda x: 20*np.log10(x))

    # get time offset
    offset = subset.rng.start/subset.fs

    dur = float(subset.rng.stop - subset.rng.start)/subset.fs
    # Add frequency and time into to channels
    for key in d_res.keys():
        p = d_res[key]

        # Calc frequency vals
        fr = np.array(np.arange(0, p.shape[0]))*subset.fs/window_len
        # Calc time vals
        nt = p.shape[1]
        tm = np.arange(0, nt)*(dur/nt)
        tm += offset
        # Arrange results in long format
        d_res[key] = [tm, fr, p]

    return d_res

def spectralProbabilityDensity(subset, window_len, window_type, olap,
                               psd_bin_size, percentiles):
    """
    Calculate Spectral Probability Density

    Parameters
    ----------
    subset: vectorSubset
        Signal subset to analyze
    window_len: int
        Window length of fft.
    window_type: str
        Window type for spectrum.  Typically 'hamming' or 'hann'.
    olap: float
        Proportion of overlap for window segments.  Value between [0, 1).
    psd_bin_size: float
         The width in Hz of each frequency bin to calculate the histogram over.
    percentiles:
        Tuple of floats indicating the percentiles (between 0 and 1) to return.

    Return
    ------
    Tuple holding the spectrogram results, and the specified percentiles
    (if requiested).
    """

    # Convert overlap to samples
    nolap = np.floor(window_len*olap)

    # Mapping function
    def fcn(x):
        _, _, psd = sig.spectrogram(
                x, fs=subset.fs, nperseg=window_len, window=window_type,
                noverlap=nolap, scaling='density')
        # Linearize results into 3 x n array
        return psd**0.5  # return root-powers

    # Run analysis on each channel
    d_res = subset.map(fcn, sum_agg=True, trns = lambda x: 20*np.log10(x), rast = True)
    d_res_prcnt = {}

    # Calc frequency vals
    fr = np.array(np.arange(0, window_len))*subset.fs/window_len
    fr = fr[fr <= subset.fs/2]

    # ------ calculate SPD
    for key in d_res.keys():
        psd = d_res[key]

        # Calculate PSD bins
        psd_min = np.min(psd); psd_max = np.max(psd)
        bins = int(np.ceil((psd_max - psd_min)/psd_bin_size))
        bins_d = (psd_max - psd_min)/bins
        p_range = np.linspace(psd_min, psd_max, bins)

        debug(f'bins: {bins}')
        debug(f'bins_d: {bins_d}')
        debug(f'p_range: {p_range}')
        debug(f'psd min/max: {psd_min} {psd_max}')

        def hist(x):
            """ SPD over time row from spectrogram """
            out, _ = np.histogram(
                x, range = (psd_min, psd_max), bins = bins,
                density = True)
            return out/psd_bin_size

        # Convert spectrogram into SPD
        spd = np.apply_along_axis(hist, 1, psd)
        debug(f'spd.size: {spd.shape}')

        # Arrange results in long format
        d_res[key] = fr, p_range, spd

        # ------ Get percentiles
        if percentiles:
            # Convert spectrogram into SPD
            prcnt = np.apply_along_axis(
                lambda x: np.percentile(
                    x, list(map(lambda x: x*100, percentiles))), 1, psd)
            # Arrange results in long format
            d_res_prcnt[key] = np.concatenate([np.array([fr]).T, prcnt], 1).T
        else:
            d_res_prcnt[key] = None
    return d_res, d_res_prcnt
