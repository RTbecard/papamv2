"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions holding code for plotting each analysis feature in paPAM.
Function inputs are the results dictionaryes from the module
analysisFeatures.py.

Each function returns a list of plot arguments/values which are used to run
and save plots in the main paPAMv2 module.

Metadata for each analysis is taken from the signal subset parameter.
"""

import numpy as np

from matplotlib.figure import Figure
from matplotlib import cm, colors
import colorcet as cc

from papamv2 import outputDictionaries as od

from papamv2.debug import debug

def waveform(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Plot waveform for each channel
    alpha = 1/len(subset.channels)
    for chan, data in d_res.items():

        # ---- Channel plots
        offset = subset.rng.start
        seconds = np.array(range(offset + 0, offset + data.size),
                dtype='f')/subset.fs
        rangify(ax, seconds, data, alpha=alpha,
                color = od.chan_color[chan], label = chan)

    # Add axis labels
    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel('$\mathrm{%s}$' % od.ref_tex[subset.unit_out])
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    return fig

def analytic_envelope(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ rangify each channel
    alpha = 1/len(d_res)
    for chan, data in d_res.items():
        # Build plot objects
        offset = subset.rng.start
        seconds = np.array(range(offset + 0, offset + data.size),
                dtype='f')/subset.fs
        rangify(ax, seconds, 20*np.log10(data), alpha=alpha,
                color = od.chan_color[chan], label = chan)

    # ------ Set plot labels
    ylab = '$L_{q, %s}$ (re $\mathrm{(1 %s)^2 \cdot s)}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    return fig

def binned_kurtosis(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Plot channels
    alpha = 1
    for chan, data in d_res.items():
        ax.plot(data[:,0], data[:,1], alpha=alpha, label = chan,
                color = od.chan_color[chan])

    ylab = 'Kurtosis'

    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    return fig

def binned_rms(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Plot channels
    alpha = 1
    for chan, data in d_res.items():
        ax.plot(data[:,0], data[:,1], alpha=alpha, label = chan,
                color = od.chan_color[chan])

    ylab = '$L_{%s}$ (re ($1 \\mathrm{%s})^2)$' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])

    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    return fig

def binned_zeroToPeak(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Plot channels
    alpha = 1
    for chan, data in d_res.items():
        ax.plot(data[:,0], data[:,1], alpha=alpha, label = chan,
                color = od.chan_color[chan])

    ylab = '$L_{pk,%s}$ (re ($1 \\mathrm{%s})^2)$' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])

    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    return fig

def binned_crestFactor(d_res, subset, bin_size, olapn):
    """
    Parameters
    ----------
    d_res: dict
        Dictionary of waveforms for each channel.
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """
    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Plot channels
    alpha = 1
    for chan, data in d_res.items():
        ax.plot(data[:,0], 20*np.log10(data[:,1]),
                alpha=alpha, label = chan, color = od.chan_color[chan])

    ylab = '$L_{cf,%s}$ (re ($1 \\mathrm{%s})^2)$' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])

    ax.set_xlabel('Time (seconds)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()

    return fig
def esd(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array or results for each channel (frequency, ESD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Loop channels
    for key in d_res.keys():
        idx = np.where((d_res[key][0] > subset.band['highpass']) & \
            (d_res[key][0] < subset.band['lowpass']))
        ax.plot(d_res[key][0][idx], 10*np.log10(d_res[key][1][idx]),
            label=key, color=od.chan_color[key])

    # Add axis labels
    ylab = '$L_{E, %s, f}$  (re $\mathrm{(1 %s)^2\cdot s\cdot Hz^{-1})}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    ax.set_xlim(subset.band['highpass'], subset.band['lowpass'])
    ax.relim(visible_only = True)

    return fig

def psd(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array of results for each channel (frequeny, PSD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Loop channels
    for key in d_res.keys():
        idx = np.where((d_res[key][0] > subset.band['highpass']) & \
            (d_res[key][0] < subset.band['lowpass']))
        ax.plot(d_res[key][0][idx], 10*np.log10(d_res[key][1][idx]),
                label=key, color=od.chan_color[key])

    # ------ Add axis labels
    ylab = '$L_{%s, f}$  (re $\mathrm{(1 %s)^2\\cdot Hz^{-1})}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel(ylab)
    ax.set_xlim(subset.band['highpass'], subset.band['lowpass'])
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    #ax.set_xlim(subset.band['highpass'], subset.band['lowpass'])

    return fig

def decidecade_exposure(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array or results for each channel (frequency, ESD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Loop channels
    for key in d_res.keys():
        x_str = ["%.0f" % x for x in d_res[key][:,0]]
        ax.plot(x_str, d_res[key][:,1], label=key, color=od.chan_color[key])

    # Add axis labels
    ylab = '$L_{E, %s}$  (re $\mathrm{(1 %s)^2\\cdot s)}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    ax.set_xlabel('Decidecade Band (Hz)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    ax.set_xticks(range(0, len(x_str), int(np.ceil(len(x_str)/10))))

    return fig

def decidecade_rms(d_res, subset):
    """
    Parameters
    ----------
    d_res: dict
        Array or results for each channel (frequency, ESD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return
    ------
    A list of argument/value pairs which can be provided to paPAMv2.plot().
    """

    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot()

    # ------ Loop channels
    for key in d_res.keys():
        x_str = ["%.0f" % x for x in d_res[key][:,0]]
        ax.plot(x_str, d_res[key][:,1], label=key, color=od.chan_color[key])

    # Add axis labels
    ylab = '$L_{%s}$ (re $\mathrm{ (1 %s)^2)}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    ax.set_xlabel('Decidecade Band (Hz)')
    ax.set_ylabel(ylab)
    ax.grid(b=True, which='major', alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    ax.set_xticks(range(0, len(x_str), int(np.ceil(len(x_str)/10))))

    return fig

def spectrogram(res, subset):
    """
    Parameters
    ----------
    res: numpy.array
        Array of results for a channel (frequency, time, PSD).
    subset: vectorData.VectorSubset
        Subset used to generate results.  Used for metadata.

    Return:
    ------
    A figure object
    """

    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot(111)

    # Make subset of bandpassed data
    idx_band = [ i for (i,f) in enumerate(res[1]) \
                if f >= subset.band['highpass'] and \
                  f <= subset.band['lowpass'] ]
    debug(f"idx_band: {idx_band}")
    debug(f"res[3].shape: {res[2].shape}")

    spec_band = res[2][idx_band[0]:idx_band[-1],:]
    debug(f"spec_band: {spec_band}")

    # ------ pcolor mesh
    offset = subset.rng.start/subset.fs
    pmesh = ax.pcolormesh(
        res[0] + offset, res[1], res[2],
        cmap='viridis', shading='auto',
        vmin = np.min(spec_band), vmax = np.max(spec_band))

    # ------ Axis labels
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xlabel('Time (seconds)')
    ax.set_axisbelow(True)
    ax.set_ylim(subset.band['highpass'], subset.band['lowpass'])

    # ------ Colorbar
    clab = '$L_{%s, f}$ (re $\mathrm{(1 %s)^2\cdot Hz^{-1})}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    fig.colorbar(pmesh, label=clab)


    return fig

def spectralProbabilityDensity(res, res_prcnt, subset, prcnt):
    """ Plot SPD with overlayed psd percentiles """

    # ------ Setup figure and axis
    fig = Figure()
    ax = fig.add_subplot(111)

    # ------ Get working range
    idx_f = [ i for (i, f) in enumerate(res[0]) \
             if f >= subset.band['highpass'] and \
                 f <= subset.band['lowpass'] ]
    debug(f'idx_f {idx_f}')
    idx_p = [i for i in range(0, res[1].size) \
             if sum(res[2][idx_f, i]) > 0 ]
    debug(f'idx_p {idx_p}')


    # ------ pcolor mesh
    pmesh = ax.pcolormesh(res[0], res[1], res[2].T,
                          cmap='viridis', shading='auto')
    ax.set_xlim(subset.band['highpass'], subset.band['lowpass'])
    ax.set_ylim(res[1][idx_p[0]], res[1][idx_p[-1]])

    # ------ Axis labels
    ax.set_xlabel('Frequency (Hz)')
    ylab = '$L_{%s, f}$  (re $\mathrm{(1 %s)^2 * Hz^{-1})}$ / dB' % \
        (od.unit_tex[subset.unit_out], od.ref_tex[subset.unit_out])
    ax.set_ylabel(ylab)
    ax.set_axisbelow(True)

    # ------ Colorbar
    clab = "Density"
    fig.colorbar(pmesh, label=clab)

    if res_prcnt is None:
        return fig

    res_prcnt = res_prcnt.T
    for col in range(1, res_prcnt.shape[1]):
        ax.plot(res_prcnt[:, 0], res_prcnt[:,col], label = prcnt[col - 1])
    ax.legend()

    return fig

def rangify(ax, x, y, color=None, alpha=None, fcn=None, label=None):
    """
    If arrays (assumed to be 1d) have more than 3000 samples, convert into
    binned ranges.  This is for more legible (and faster) plotting.

    Use this function to build plots for analysis features which return
    time-series data.

    Parameters
    ----------
    ax: axis
        Axis to plot rangify on.
    x: numpy.array
        x axis data to plot.
    y: numpy.array
        y axis data to plot.
    color: str
        Color to plot.
    alpha: float
        Alpha value for ribbon, between 0 and 1.  Does not apply to line
        plots (i.e. if too few samples to make ribbon plot).
    fcn: function
        When set, this will be used to calculate a single return value per bin.

    Return
    ------
    A list holding the plotting actions to take, formatted for input in
    self.plot().
    """

    bins = 10000

    if color is None:
        color = '#1f77b4'

    if alpha is None:
        alpha = 1

    # Must have at least 3 values per bin
    if x.size < bins*3:
        ax.plot(x, y)
    else:
        # calc bin indicies
        idx = np.linspace(0, x.size - 1, num=bins, endpoint=False)
        idx_left = np.floor(idx).astype('i8')
        idx_right = np.ceil(idx + (x.size/bins)).astype('i8')

        if fcn is None:
            # Default action, return fill_between plot (range of values in bin)
            upper = np.empty((bins))
            lower = np.empty((bins))
            for i in range(0, bins):
                idx_rng = range(int(idx_left[i]), int(idx_right[i]))
                upper[i] = max(y[idx_rng])
                lower[i] = min(y[idx_rng])

            ax.fill_between(x[idx_left], upper, lower,
                    color=color, alpha=alpha, step='mid', label=label)
        else:
            # Apply custom function to data
            val = np.empty((bins))
            for i in range(0, bins):
                idx_rng = range(int(idx_left[i]), int(idx_right[i]))
                val[i] = fcn(y[idx_rng])

            ax.plot(x[idx_left], val, color=color, alpha=alpha)

