"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------
"""
import re
import traceback
import os

# Array handling
import numpy as np

# Modules holding paPAM subroutines
from papamv2 import optionParsing
from papamv2 import vectorData
from papamv2 import plot as pt
from papamv2 import bandpass
from papamv2 import outputDictionaries as od
from papamv2 import report as rpt
from papamv2 import propagation_axis as pa
from papamv2.calibration import plot as calibplt

from papamv2.analysis import wrappers as aw
from papamv2.analysis import plot_transient as ptrn
from papamv2.analysis_directional import wrappers as daw

from papamv2.debug import *

# This function only needs to be run once per session
enable_debug()

def run(options, ui=None, sig_term=None):
    """
    Main analysis routine for paPAMv2.

    Parameters
    ----------
    options: dict
        Dicitonary of analysis options.
    subsets: [range, ...]
        List or range objects for subsampling
    ui: Ui object
        For GUI interface, a ui object representing the main window must be
        supplied.
    """
    debug('options: %s' % options)

    # ========================= Setup options ==============================
    # Initalize plotting class
    hide_plots = options['hide_plots'] or options["sequential_subsets"]
    PamPlot = pt.PamPlot(hide_plots, ui)

    # Initialize reportin class
    Rpt = rpt.Report(options)

    # ------ Check that user has write permission to output folder
    if not options['no_output']:
        chk = os.access(options['output_dir'], os.W_OK | os.X_OK)

    # ------ remove irrelevant components and features
    if options['channels'] == 'p':
        Rpt.pamPrint("Only analyzing pressure...")
        options['units_out'] = options['units_out'].replace('d', '')
        options['units_out'] = options['units_out'].replace('v', '')
        options['units_out'] = options['units_out'].replace('a', '')
        options['pm_axis'] = False
        options['si_axis'] = False
        options['directional_spectrogram'] = False
    if 'p' not in options['channels']:
        Rpt.pamPrint("Only analyzing particle motion...")
        options['units_out'] = options['units_out'].replace('p', '')
        options['units_out'] = options['units_out'].replace('e', '')
        options['si_axis'] = False
        debug(f"dir_spec: {options['directional_spectrogram']}")
        if options['directional_spectrogram']:
            options['directional_spectrogram'][3].replace('i', '')

    # ------ Print command string
    # The command string can be used to rerun the analyis using the exact same
    # options.
    Rpt.printHeader('Call', '+', True)
    Rpt.pamPrint("$ " + optionParsing.cmd(options))
    Rpt.printHeader("Analysis Parameters", '-', True)

    # =========== Init vectorData object and set bandpass filters =============
    Vd = vectorData.VectorData(
            options['wav_files'],
            options['channels'],
            options['rho'],
            options['c'])

    # Set reporting options
    Rpt.fs = Vd.fs

    Rpt.pamPrint('Sample rate: ' + str(Vd.fs) + "Hz")
    Rpt.pamPrint('File duration: ' + str(np.round(Vd.n/Vd.fs, 3)) + "s")

    # ------ Define bandpass filters
    bpass = options['bandpass']
    band = (bpass[0], bpass[1], bpass[2])
    if band[2] == 0:
        band = (0, Vd.fs/2, 0)

    Rpt.pamPrint('Output units: ' + \
            ', '.join([od.unit_label[x] for x in options['units_out']]))

    # ====================== Calculate analysis ranges ========================
    # Ranges are a list of range objects.  Each range will be extracted as a
    # subset and have the analysis features applied to it.
    if options["subsets"]:
        Rpt.pamPrint("Subsets analysis")
        # Analyze a subsets
        rngs = list(map(
                lambda x: range(
                    int(float(x[0])*Vd.fs),
                    int(float(x[1])*Vd.fs)),
                options['subsets'][0]))
    elif options['sequential_subsets']:
        Rpt.pamPrint("Sequential subsets analysis")
        dur_n = int(options['sequential_subsets']*Vd.fs)
        itr = zip( range(0, Vd.n - dur_n, dur_n),
                range(dur_n, Vd.n, dur_n) )
        rngs = [range(a, b) for a,b in itr]
    else:
        # Analyze entire file
        Rpt.pamPrint("Whole file analysis")
        rngs = [ range(0, Vd.n) ]

    if not options['hide_plots'] and len(rngs) > 1:
        Rpt.pamPrint("Multiple ranges selected, hiding live output.")
        options['hide_plots'] = True
        PamPlot.hide_plots = True

    # ===================== Create calibration impulses =======================
    # --- Specify extra calibration channels
    # Some analyses require channels that are not specified in the output units
    # i.e. directional analysis, or generating the propagation axis.
    Rpt.pamPrint("\nGenerating calibration filters...")
    calib_units_out = options['units_out']
    # Check for missing required channels
    if calib_units_out.find('p') == -1 and options['si_axis']:
        calib_units_out += 'p'
    if calib_units_out.find('p') == -1 and options['units_out'].find('e') != -1:
        calib_units_out += 'p'
    if options['directional_spectrogram']:
        if calib_units_out.find('v') == -1 and \
                options['directional_spectrogram'][3].find('v') != -1:
            calib_units_out += 'v'
        if calib_units_out.find('a') == -1 and \
                options['directional_spectrogram'][3].find('a') != -1:
            calib_units_out += 'a'
        if calib_units_out.find('v') == -1 and \
                options['directional_spectrogram'][3].find('i') != -1:
            calib_units_out += 'v'
        if calib_units_out.find('p') == -1 and \
                options['directional_spectrogram'][3].find('i') != -1:
            calib_units_out += 'p'
    if calib_units_out.find('p') == -1 and options['binned_soundIntensity']:
        calib_units_out += 'p'
    if calib_units_out.find('v') == -1 and options['binned_soundIntensity']:
        calib_units_out += 'v'
    if calib_units_out.find('v') == -1 and options['si_axis']:
        calib_units_out += 'v'

    # --- Generate the impulses
    Vd.setCalibration(
            options['calibration_magnitude'],
            options['calibration_phase'],
            options['recorder_gain'],
            options['calibration_file_unit'],
            calib_units_out)

    # --- Plot calibration values
    if options['calibration_magnitude'] is not None:
        for unit_out in Vd.units_out:
            nm = "calibration_magnitude_" + unit_out
            fcn = calibplt.plot_mag
            args = [Vd.calib, unit_out]
            Rpt.printPlots(nm, fcn, args, path = Rpt.output_calibrations)

    if options['calibration_phase'] is not None:
        for unit_out in Vd.units_out:
            nm = "calibration_phase_" + unit_out
            fcn = calibplt.plot_phase
            args = [Vd.calib, unit_out]
            Rpt.printPlots(nm, fcn, args, path = Rpt.output_calibrations)

    Rpt.pamPrint("Calibrated units: " + calib_units_out)

    if 'e' in calib_units_out:
        Rpt.pamPrint("\nEstimated particle velocity channel 'e' calculated from:")
        Rpt.pamPrint(" - rho: %.2f" % options['rho'])
        Rpt.pamPrint(" - c: %.2f" % options['c'])

    Rpt.pamPrint('\nRunning analysis...')

    # ================= Loop through analysis ranges ======================
    for rng_id, rng in enumerate(rngs):

        Rpt.pamPrint("="*79)
        Rpt.range(rng_id, rng)
        Rpt.pamPrint('Range: ' + Rpt.rng_str + ' seconds')

        # ------ Set bandpass filter for subset
        Vd.setBandpass(band[0], band[1], band[2])
        band_str = str(int(band[0])) + '-' + str(int(band[1]))
        if(band[2] == 0):
            band_label = "FullBandwidth"
        else:
            band_label = str(round(band[0])) + "-" + str(round(band[1])) + "Hz"

        Rpt.pamPrint(f"Band: {band_label}")
        # ------ Calculate directional spectrogram
        if options['directional_spectrogram']:
            for unit in options['directional_spectrogram'][3]:
                daw.dirspec(
                    vectorData = Vd,
                    params = options['directional_spectrogram'],
                    Rpt = Rpt, rng = rng,
                    PamPlot = PamPlot, unit_out = unit)

        # ------ Binned sound intensity
        if options['binned_soundIntensity']:
            daw.binned_soundIntensity(
                vectorData = Vd,
                binned_params = options['binned_analysis'],
                Rpt = Rpt, rng = rng,
                PamPlot = PamPlot)

        # ------ Check for termination signal
        if (not sig_term is None) & sig_term.term:
            Rpt.pamPrint("Analysis stopped: Ending run early...")
            break

        if rng.start < 0 or rng.stop > Vd.n:
            Rpt.pamPrint("Current range outside of file... skipping...")
            continue

        Rpt.pamPrint("+"*79)

        # ------ Calculate propagation axis
        Rpt.set_dir_none()
        if options['si_axis']:
            # ------ Calculate sound intensity vector
            Rpt.pamPrint('Calculating sound intensity vector...')
            subset_p = Vd.subset('p', rng)
            subset_v = Vd.subset('v', rng)
            prop_vec, pii = pa.getSIAxis(subset_p, subset_v)
            bearing_si, v_angle_si = pa.vec2rad(prop_vec)
            Rpt.set_dir_si(bearing_si, v_angle_si)
            Rpt.pamPrint(" - Relative Bearing: %.2f degs (re x+)" % (bearing_si*180/np.pi))
            Rpt.pamPrint(" - Verticle angle: %.2f degs" % (v_angle_si*180/np.pi))
            Rpt.pamPrint(" - L_K: %.2f dB" % (pii))

        # Loop through particle motion units
        for unit_out in options['units_out']:

            # Get papamv2 wrapper functions These are names of valid analysis types
            features = dir(aw)
            # get list of analysis types to run
            analyses = {k: v for k,v in options.items() if k in features and v != False}

            if len(analyses) == 0 and not options['binned_particleMotionVector']:
                # skip if no analysis are selected
                continue

            # ------ Check for termination signal
            if not sig_term is None and sig_term.term:
                Rpt.pamPrint("Analysis stopped: Ending run early...")
                break

            # ------ Print status to console
            Rpt.printHeader(od.unit_label[unit_out], "-")

            # ------ Copy calibrated signal subset to memory
            Rpt.pamPrint('Calibrating/bandpassing signal...')
            subset = Vd.subset(unit_out, rng)

            # ------ Particle motion directional analysis
            if options['binned_particleMotionVector'] and unit_out in 'dva':
                daw.binned_particleMotionVector(
                    subset = subset,
                    binned_params = options['binned_analysis'],
                    Rpt = Rpt, rng = rng,
                    PamPlot = PamPlot, unit_out = unit_out)

            if options['pm_axis'] and unit_out in 'dva':
                # ------ Calculate particle motion vector
                Rpt.pamPrint('Calculating ' + od.unit_label[unit_out] + ' vector...')
                subset_pm = Vd.subset(unit_out, rng)
                prop_vec, e = pa.getPVAxis(subset_pm)
                bearing_pm, v_angle_pm = pa.vec2rad(prop_vec)
                Rpt.set_dir_pm(bearing_pm, v_angle_pm, e)
                Rpt.pamPrint(" - Relative Bearing: %.2f degs (re x+)" % (bearing_pm*180/np.pi))
                Rpt.pamPrint(" - Verticle angle: %.2f degs" % (v_angle_pm*180/np.pi))
                Rpt.pamPrint(" - Broadband Eccentricity: %.2f" % e)

                Rpt.pamPrint("Projecting new " + od.unit_label[unit_out] + " axis...")
                pa.projectNewAxis(subset, bearing_pm, v_angle_pm, 'm')

            if options['si_axis']:
                # ------ Add propataion axis
                Rpt.pamPrint("Projecting new sound-intensity axis...")
                pa.projectNewAxis(subset, bearing_si, v_angle_si, 'i')



            # Extract binned analysis options
            binned_analysis = options['binned_analysis']

            # ------ Save range info
            Rpt.band = band_str
            Rpt.unit_out = unit_out
            Rpt.write_ranges()

            # ================= Execute Analysis ==========================

            # ------ Loop parameter names
            # For each parameter name that is recognized as a analysis type
            # (i.e. has a function defined analysis.wrappers), an analysis
            # will be run

            for key, value in analyses.items():
                # ------ Check for termination signal
                if not sig_term is None:
                    if sig_term.term:
                        Rpt.pamPrint("User stopped analysis...")
                        break

                # Get wrapper function for analysis
                wrapper_fcn = getattr(aw, key)

                try:

                    debug(f"analysis: {key}")
                    debug(f"options: {analyses[key]}")

                    wrapper_fcn(params = value,
                                subset = subset,
                                Rpt = Rpt,
                                PamPlot = PamPlot,
                                binned_params = options['binned_analysis'])

                except Exception as inst:
                    txt = "\n".join(traceback.format_tb(inst.__traceback__))
                    txt = txt + "\n" + repr(inst)
                    Rpt.pamPrint(txt)
                    Rpt.pamPrint("Analysis Error. Skipping this analysis...")

            # ------ Print transient plot
            trns_keys = [ "impulse_rms", "impulse_kurtosis", "exposure",
                         "zero_to_peak", "crest_factor", "rise_time",
                         "pulse_length", "esd"]

            if any(x in trns_keys for x in analyses.keys()):

                ptrn.plot_transient(options, subset, Rpt, PamPlot)

    Rpt.pamPrint("File Complete.\n")
