# bound check slows down runtime speeds
# language level to specify python3
# cython: boundscheck=False, language_level=3

# From complex values, we require the complex library from cython's incldued cpp headers
# distutils: language=c++

"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020-2022 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Functions for calculating properties of spectral ellipses.
Allows the calculation of the direction and eccentricity of a particles
elliptical path directly from the frequency domain.

This code is written in cython, as it's painfully slow in native python.
This needs to be compiled with c++ support due to the inclusion of the complex
library.

"""
import cython

# support gfor numpy arrays
import numpy as np
cimport numpy as np

# Define all numpy arrays to use single precision floats
DTYPE = np.double

# list of included cython C libraries
# https://github.com/cython/cython/tree/master/Cython/Includes

# Cython imports
from libc.math cimport M_PI, sin, cos, tan, atan2, pow, atan, NAN, acos, log10

# complex.h requires compiling in cpp, and defining the functions we'll use from it.
cdef extern from "<complex.h>" nogil:
    double real(double complex z)
    double abs(double complex z)
    double arg(double complex z)
    double complex conj(double complex z)
# link below is reference for avaiable c++ complex functions
#https://en.cppreference.com/w/cpp/numeric/complex

# =============================================================================
# ======================= Internal LINALG functions ===========================
# =============================================================================

cdef double dotvv(double* x, double* y, int n):
    """
    Dot product for matricies or vectors
    """

    cdef:
        double out = 0

    for i in range(n):
        out += x[i]*y[i]

    return out

cdef double e_norm(double* x, int n):
    """
    Euclidian norm of a vector.
    """

    cdef double out = dotvv(x, x, n)
    out = pow(out, 0.5)

    return out

def test_linalg():

    cdef:
        double x[2]
        double y[2]
        double A[3][2]
        double B[2][3]

    A[0][:] = [1, 2]
    A[1][:] = [3, 4]
    A[2][:] = [5, 6]

    B[0][:] = [7, 8, 9]
    B[1][:] = [10, 11, 12]

    x[:] = [1,2]
    y[:] = [2,3]

    print("x: %s" % x)
    print("y: %s" % x)
    print("A: %s" % A)
    print("B: %s" % A)
    print("e_norm x: %s" % e_norm(x, 2))
    print("dot_vv x: %s" % dotvv(x, x, 2))


# =============================================================================
# ========================= Geometry Functions ================================
# =============================================================================

cdef struct directional:
    double mag
    double bearing
    double eccentricity
    double vert_angle

cdef struct ellipse:
    double* a
    double* b
    double a_len
    double b_len

cdef double eccentricity(double a, double b):
    """
    Calculate eccentricity of an ellipse
    """

    cdef double out
    out = pow(1 - pow(b/a, 2), 0.5)
    return out

cdef ellipse get_ellipse(double complex * x, int dims):
    """
    Return an ellipse from a vector of FFT results
    """

    cdef:
        double mag[3]
        double phase[3]
        double delta, a1_len, a2_len, t_0
        double x_0[2]
        double y_0[2]
        double c_1[3]
        double c_2[3]
        double a_1[3]
        double a_2[3]

    # Get phase and magnitude for each dimension
    for i in range(dims):
        mag[i] = abs(x[i])
        phase[i] = arg(x[i])

    # Get conjugate axes in horizontal plane (in parametric ellipse form)
    delta = phase[1] - phase[0] + (M_PI/2)
    x_0 = [mag[0],0]
    y_0 =[0,mag[1]]
    for i in range(2):
        c_1[i] = x_0[i] + y_0[i]*sin(delta)
        c_2[i] = y_0[i]*cos(delta)

    # print(dims)

    # Add 3D coord to axis
    if dims == 3:
        c_1[2] = mag[2] * cos(acos(c_1[0] / mag[0]) + phase[2] - phase[0])
        c_2[2] = mag[2] * cos(acos(c_2[0] / mag[0]) + phase[2] - phase[0])
    else:
        c_1[2] = 0
        c_2[2] = 0

    # Get perpendicular axis (see wikipedia page for Rytz's construction)
    t_0 = atan( (2 * dotvv(c_1, c_2, dims)) / (dotvv(c_1, c_1, dims) - dotvv(c_2, c_2, dims)) ) / 2
    for i in range(3):
        a_1[i] = c_1[i] * cos(t_0) + c_2[i]*sin(t_0)
        a_2[i] = c_1[i] * cos(t_0 + (M_PI/2)) + c_2[i]*sin(t_0 + (M_PI/2))

    # calc axis lengths and return major/minor axes
    a1_len = e_norm(a_1, dims)
    a2_len = e_norm(a_2, dims)

    if a1_len > a2_len:
        a = a_1
        b = a_2
        a_len = a1_len
        b_len = a2_len
    else:
        a = a_2
        b = a_1
        a_len = a2_len
        b_len = a1_len

    cdef ellipse ret = ellipse(a,b,a_len, b_len)

    return ret

cdef directional_pm(double complex* x, directional* ret, int dims):
    """
    For a given complex particle motion vector in the frequency domain, return the:
     - magnitude
     - bearing
     - eccentricity
     - verticle angle
    """

    cdef:
        ellipse ell
        double horiz_len

    ell = get_ellipse(x, dims)

    ret.mag = e_norm(ell.a, dims)
    ret.bearing = -atan2(ell.a[1],ell.a[0])
    ret.eccentricity = eccentricity(ell.a_len, ell.b_len)

    if dims == 3:
        # ------ 3D localization
        horiz_len = pow(pow(ell.a[0], 2) + pow(ell.a[1], 2), 0.5)
        ret.vert_angle= atan2(ell.a[2], horiz_len)

    else:
        ret.vert_angle = NAN


cdef void directional_si(double* x, directional* ret, int dims):
    """
    Calculate bearing from sound intensity
    """

    cdef:
        double xy

    # Get sound intensity bearing
    ret.bearing = -atan2(x[1], x[0])

    # Get 3D localizations
    if dims == 3:
        xy = pow(pow(x[0], 2) + pow(x[1], 2), 0.5)
        ret.vert_angle = atan2(x[2], xy)
    else:
        ret.vert_angle = NAN


# ========================= TESTING FUNCTIONS =================================

def get_ellipse_py(double complex[::1] x):
    """
    Python wrapper for directional_si
    """

    cdef:
        double complex x2[3]
        ellipse ret

    for i in range(3):
        x2[i] = x[i]

    ret = get_ellipse(x2, x.size)

    cdef:
        double a[3]
        double b[3]

    for i in range(3):
        a[i] = ret.a[i]
        b[i] = ret.b[i]

    return a, b, ret.a_len, ret.b_len

def directional_pm_py(double complex[::1] x):
    """
    python wrapper for directional_pm
    """

    cdef:
        double complex x2[3]
        directional ret

    # Copy to C array
    x2[0] = x[0]
    x2[1] = x[1]
    x2[2] = x[2]

    # Get direcitonal
    directional_pm(x2,  &ret, len(x))

    return ret

def directional_si_py(double[::1] x):
    """
    Python wrapper for directional_si
    """

    cdef:
        double x2[3]
        directional ret

    x2[0] = x[0]
    x2[1] = x[1]
    x2[2] = x[2]

    directional_si(x2, &ret, len(x))

    return ret

# ============================= python wrappers ================================
def localize_pm(double complex[:, ::1] cmplx):

    cdef:
        int dims = cmplx.shape[0]
        int n = cmplx.shape[1]

    # Init numpy arrays for output
    mag = np.empty([n])
    bearing = np.empty([n])
    vert_angle = np.empty([n])
    eccentricity = np.empty([n])

    # init 3d values with NaNs
    vert_angle[:] = NAN
    eccentricity[:] = NAN

    # Make Cython views to numpy arrays
    cdef:
        double[::1] v_mag = mag
        double[::1] v_bearing = bearing
        double[::1] v_vert_angle = vert_angle
        double[::1] v_eccentricity = eccentricity

    cdef double complex sample[3]

    # Make localization return objects
    cdef directional drct

    # for i in range(samples):
    for i in range(n):

        sample[0] = cmplx[0, i]
        sample[1] = cmplx[1, i]
        if dims == 3:
            sample[2] = cmplx[2, i]

        directional_pm(sample, &drct, dims)

        v_mag[i] = drct.mag
        v_bearing[i] = drct.bearing
        v_eccentricity[i] = drct.eccentricity

        if dims == 3:
            v_vert_angle[i] = drct.vert_angle

    return mag, bearing, vert_angle, eccentricity

cpdef double spec2si(double complex v, double complex p):
    """
    Given the frequency domain particle velocity and pressure values, return the
    magnitude of the sound intensity vector.
    """

    cdef:
        double a = abs(v)
        double b = abs(p)
        double alpha = arg(v)
        double beta = arg(p)
        double out

    out = a*b*cos(alpha - beta)/2

    # Convert from fW/m^2 to pW/m^2
    out *= 1e-3

    return out

def localize_si(double complex [:, ::1] x, double complex[:, ::1] p):

    cdef:
        int dims = x.shape[0]
        int n = x.shape[1]

    # Init numpy arrays for output
    bearing = np.empty([n])
    vert_angle = np.empty([n])
    vert_angle[:] = NAN
    magnitude = np.empty([n])
    pii = np.empty([n])

    # Make Cython views to numpy arrays
    cdef:
        double[::1] v_bearing = bearing
        double[::1] v_vert_angle = vert_angle
        double[::1] v_magnitude = magnitude
        double[::1] v_pii = pii

    cdef double intensity[3]

    # Make localization return objects
    cdef:
        directional drct
        #double complex temp_cmplx

    # Loop samples
    for i in range(n):

        # Calculate sound intensity vector
        for j in range(dims):
            intensity[j] = 0.5 * real(x[j, i] * conj(p[0, i])) * 1e3

        # Calculate SI vector length
        v_magnitude[i] = e_norm(intensity, dims)

        directional_si(intensity, &drct, dims)

        v_bearing[i] = drct.bearing

        if dims == 3:
            v_vert_angle[i] = drct.vert_angle

        v_pii[i] = 10*log10(pow(abs(p[0,i]), 2) / v_magnitude[i])

    return magnitude, bearing, vert_angle, pii
