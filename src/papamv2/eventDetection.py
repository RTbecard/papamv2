"""
    paPAMv2: Passive Acoustic Monitoring for Particle Acceleration.

    Copyright 2020 James Campbell

    --------------------------- LICENSE ---------------------------------------
    This file is part of paPAMv2.

    paPAMv2 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    paPAMv2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with paPAMv2.  If not, see <https://www.gnu.org/licenses/>.
    ---------------------------------------------------------------------------

Event Detection
---------------
This file holds the code for event detection.

Event detection can run as an independant analysis.
"""

import copy

import numpy as np

def detectEvents(parameters, Vd, rngs, Rpt):
    """
    If either a relative or absolute threshold for event detection is set,
    run the event detector.

    Parameters
    ----------
    wavs: vectorData
        Vector sensor data to detect events over.
    threshold_relative
    threshold_absolute
    threshold_type
    threshold_units
    event_duration
    event_spacing
    event_padding
    detector_bandpassthre
    """

    if parameters["detector_resolution"] == 0:
        # Auto detection not selected, skip
        return rngs

    # init empty list of ranges
    rngs_out = []

    Rpt.pamPrint("Running auto detection of transients...")

    # Deep copy VectorData object (so we dont alter our analysis settings)
    Vd_2 = copy.deepcopy(Vd)
    # Set bandpass
    Vd_2.setBandpass(*parameters['detector_bandpass'])

    # ------ Loop subsets
    for rng in rngs:

        subset = Vd_2.subset(parameters['threshold_units'], rng)

        # Generate bin idxs
        bin_n = int(np.floor(parameters['detector_resolution']*subset.fs))
        idx_start = np.arange(0, subset.n - bin_n, bin_n, dtype='int')
        # Init vector of rms values
        rm_vec = np.zeros(len(idx_start), dtype='float')
        # init vector of detections
        det_abs = np.zeros(len(idx_start), dtype='bool')
        det_rel = np.zeros(len(idx_start), dtype='bool')

        # Generate vector of RMS values
        Rpt.pamPrint("Calculating binned RMS values...")
        for i_rms, i_subset in enumerate(idx_start):
            # Mean power along time axis
            rm = np.apply_along_axis(
                lambda x: np.mean(x**2),
                subset[i_subset:(i_subset + bin_n)],
                1)
            # Sum power across channels
            rm_vec[i_rms] = sum(rm)

        thresh_abs_l = 10^(parameters['thresold_absolute'][0]/20)
        window_abs = np.floor(
                parameters['threshold_absolute'][1] * subset.fs / bin_n)
        thresh_rel_l = 10^(parameters['thresold_relative']/20)
        # Loop over bins and calculate detections
        Rpt.pamPrint('Calculating detections...')
        for idx in idx_start:
            if parameters['thresold_absolute'] > 0:
                if rm_vec:

    return rngs_out
