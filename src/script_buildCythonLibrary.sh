#/bin/bash

# This will create the .so or .pyd library required for running  papamv2
# For Ubuntu, we need to use C.
# On windows, need to use C++ instead.
# Change langauge at the top of spectral_ellipses_c.pyx

export COMPILE_PAPAM="ALL"
python3 setup.py build_ext --inplace

