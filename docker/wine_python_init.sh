#!/bin/bash

set -e

# Setup dummy screen
# This is required for some installations in wine which expect screens
#Xvfb :0 -screen 0 1024x768x16 &
#jid=$!

export WINEPATH='C:\Program Files\Python310' 
export WINEPREFIX=~/.wine64 

echo " ------ Install pip ------ "
wine python -m pip install --upgrade pip
wine python -m pip install wheel
wine python -m pip install pynsist

echo "------ Install python modules ------"
wine python -m pip install -r requirements_WIN10.txt 
