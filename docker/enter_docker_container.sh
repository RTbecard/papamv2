#!/bin/bash

# SCript for testing isntall scripts in docker containers

set -e

# container name
name="container_bash"

# start container
#sudo docker run --name $name --rm --interactive --tty $1 bash
sudo docker create --rm --tty --name $name $1 bash

# Add files into container
sudo docker cp wine_install.sh $name:./
sudo docker cp wine_python_init.bat $name:./
sudo docker cp ../requirements_WIN10.txt  container_bash:./
sudo docker cp ../requirements_UNIX.txt  container_bash:./

# Enter bash shell
sudo docker start $name

sudo docker exec --interactive --tty $name bash

# Close container
sudo docker stop $name


