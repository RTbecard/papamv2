#!/bin/bash

set -e

echo "---------------------------------"
echo "-------- setup wine prefix ------"
echo "---------------------------------"
# We need the developer version of wine.  We need at least version 4.14 (see link).
# This is the earliest version I've seen reported to work with python3 well
# Without this, we'd have to run the embedded install of python which is riddled
# with annoying issues.

# see: https://appdb.winehq.org/objectManager.php?sClass=version&iId=38187

echo "------ Installing required apt packages ------"
# update dist packages
dpkg --add-architecture i386

apt update
DEBIAN_FRONTEND=noninteractive apt -y dist-upgrade
# Install required programs
DEBIAN_FRONTEND=noninteractive apt install -y wget gnupg software-properties-common apt-utils imagemagick inkscape
apt dist-upgrade

echo "-------- Install wine-dev ------"
apt install -y 	xvfb 		# This is for making a dummy X server disply 
# Add wine repo
wget -nc -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
wget -nc -P /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
# ------ Get newer wine version
apt update
apt install -y --install-recommends winehq-devel
apt install -y winetricks
wine --version

