#!/bin/bash

# This script is for building docker images on your local machine.
# Built images can be uploaded to dockerhub for use in gitlab CI
# Test locally with gitlab-runner

# Start docker daemon
sudo service docker start

cp ../requirements_UNIX.txt ./

# Build Docker image
sudo docker image build \
	-f ./dockerfile-pyinstall-ubuntu \
	-t rtbecard/python3.10-ubuntu:latest \
	./

rm ./requirements_UNIX.txt 

# Show build images
sudo docker image list

# docker login -u "myusername" -p "mypassword" docker.io
# sudo docker push rtbecard/python3.10-ubuntu
