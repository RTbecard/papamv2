#!/bin/bash

set -e

# This script is for building docker images on your local machine.
# Built images can be uploaded to dockerhub for use in gitlab CI
# Test locally with gitlab-runner

# Start docker daemon
sudo service docker start

cp ../requirements_WIN10.txt ./

# Build Docker image
sudo docker image build \
	-f ./dockerfile-python3.10_nsis \
	-t rtbecard/python3.10_nsis:latest \
	./

rm ./requirements_WIN10.txt

# Show build images
sudo docker image list

# sudo docker push rtbecard/python3.10_nsis
