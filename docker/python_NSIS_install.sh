#!/bin/bash

set -e

# set wine ENV vars
export DISPLAY=:0.0
export WINEPREFIX=~/.wine64
export WINARCH=win64

echo "------ Download python ------"
#wget https://www.python.org/ftp/python/3.7.6/python-3.7.6-amd64.exe
# 3.7 doesn't work with pyinstaller and pyqtgraph on windows
#wget https://www.python.org/ftp/python/3.9.9/python-3.9.9-amd64.exe  
wget https://www.python.org/ftp/python/3.10.7/python-3.10.7-amd64.exe
install_exe="python-3.10.7-amd64.exe"

echo "------ Init wine prefix ------"
winetricks corefonts win10

# Setup dummy screen
# This is required for some installations in wine which expect screens
Xvfb :0 -screen 0 1024x768x16 &
jid=$!

echo "------ Install python ------"
wine cmd /c \
	$install_exe \
	/quiet \
	PrependPath=1 \
	InstallAllUsers=1 \
	&& echo "Python Installation complete"
# /quiet uses silent install, 

echo "------ Install NSIS (setup.exe generator) ------"
wget https://prdownloads.sourceforge.net/nsis/nsis-3.08-setup.exe
wine cmd /c nsis-3.08-setup.exe /S \
	&& echo "NSIS installation complete"

