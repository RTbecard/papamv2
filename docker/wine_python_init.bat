#!/bin/bash

# Setup dummy screen
# This is required for some installations in wine which expect screens
Xvfb :0 -screen 0 1024x768x16 &
jid=$!


echo " ------ Install pip ------ "
python -m pip install --upgrade pip
python -m pip install wheel
python -m pip install pynsist

echo "------ Install python modules ------"
python -m pip install -r requirements_WIN10.txt 

IF %ERRORLEVEL% NEQ 0 (
	echo "!!!!!!     Failed installing remaining packages     !!!!!!"
	EXIT /B %ERRORLEVEL%
)

