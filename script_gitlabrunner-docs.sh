#!/bin/bash

# This is an offline test of the gitlab CI system.  This should be used before 
# pushing big updates

set -e

#sudo gitlab-runner exec docker --docker-pull-policy=if-not-present user-guide
sudo gitlab-runner exec docker --docker-pull-policy=if-not-present pages

echo "Tests complete!"
