---
date: 2019-12-30
author: James Campbell
title: Docker Notes
---



[TOC]

# Overview

Docker is the framework that gitlab (and github) use to compile and test code on pull requests.
Each time the paPAM source code gets updated on the repository, the code will be compiled into binaries (`.exe` files for windows users) and these latest versions will be available for download in the link in the repository `README`.
Docker in gitlab works like this:

1. New code is pushed into the repository.
2. A docker container is created.
 This consists of a miniaturised, isolated, Linux operating system which holds all the tools necessary for running/compiling the repository code.
3. The docker container runs the specified commands to package the python app into an executable file for Windows or Ubuntu.
4. If there are errors during the packaging, the build will fail and errors messages will be passed.
5. If the build passes (i.e. there are no errors packaging the app), then the newly packaged python program will be saved in the repo and users can automatically download it if an appropriate link is given in the `README` file.
6. Additional scripts can be added to the container to check for errors after the build.
For this, a CLI version of paPAM is necessary to also be built as we can only test on the command line in docker.

This allows me to automatically distribute the latest stable version of the app on the gitlab repository page with just a single (git) push action.
The popularity of docker is based largely on the fact that docker containers are isolated systems, providing a convenient, unbiased testing platform (often, an app may work fine on the machine its written on, but work poorly on others due to missing dependencies).  In addition, I can also run docker locally on my machine for quick testing.

This document will go through the steps I had to take to setup the docker system for both  local testing and gitlab CI.
Note this document was written for my computers which run Ubuntu 19.10 (my OS at the time of writing). 

# Installing Docker Locally (Ubuntu 19.10)

Docker doesn't currently have stable or testing versions for Ubuntu 19.10, so we'll install docker for Ubuntu 18.10.

The installation is straight forward.

```bash
sudo bash -c 'echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable" > /etc/apt/sources.list.d/docker-ce.list'

sudo apt-get install docker-ce 
```

That's it.
You should now be able to run docker locally.

# Creating the paPAM Compiler Image

For compiling paPAM we're using [pyinstaller](http://www.pyinstaller.org/features.html).
We chose this option as it's cross-platform and  supports popular python libraries such as `pyqt` and `matplotlib`, both of which are used extensively by paPAM.

We want paPAM to be cross-platform.
Using docker images, we can compile a Ubuntu version simply, a windows version with a few tricks, but we cannot compile a Mac version with docker.  This section will go through how to create an appropriate docker image for these 2 cases.

## Creating a Dockerfile

A dockerfile is basically a blueprint for creating a docker image.  Docker images are templates for docker containers; a new container is made by cloning an existing docker image.  Each time you run a new test on gitlab within docker, a new container is created to run the test.

We'll interactively work inside a docker container to find a suitable setup.  Once we know what what commands are necessary to prepare the container for running pyinstaller, we can write those key instructions into a dockerfile.

We'll take a basic Ubuntu image available from [dockerhub](https://hub.docker.com/), run this container locally, then install the missing python packages we need on top of it.  

### Create Container for Testing

First, we can interactively start a container.
```bash
# Interactively open ubuntu docker container in bash
sudo docker run -it ubuntu:18.04 bash
```
`-i` makes the container interactive and `-t` gives our shell name in the prompts (so you know when a command has finished executing).

This will enter us into the bash shell where we can do some tests.
From there we can figure out how to install all the necessary packages.

Before we start installing packages we need some testing data in our container.  In a new terminal, we can now copy the relevant build files into the container.

```bash
# Show running containers
sudo docker ps -a
# Copy directory contents into conrainer named 'lucid_nobel'
sudo docker cp ~/Repos/papamv2/UI lucid_nobel:UI
```

That's it.
Now use the previous terminal where you opened the bash shell inside the container to start testing building of packages.

I'm using `Anaconda3` as my python development platform, so I'm going to use `conda list | less` to search which versions of each package I need so my docker image roughly matches my development environment.
Also, these package versions will be written on the gitlab README in case other users want to run the source code on their machines.
Here are my current dependencies, we'll be doing everything in python 3.7:

- `pyqt`==5.9.2
- `pyinstaller`==3.5
- `numpy`==1.17.2
- `pandas`==0.25.1
- `matplotlib`=3.1.1

We can now install our python environment along with required pip packages in the container:

```bash
apt update && apt install -y python3.7 python3-pip
pip3 install PyQt5==5.9 PyInstaller==3.5 numpy==1.17 pandas==0.25 matplotlib==3.1.1
```

Now lets try packaging the python app.
I previously tested pyinstaller on this code, so I know it should compile if I have the proper dependencies installed in the container.
To make life easier, I also copied all my pyinstaller instructions into a `sh` script.  Inside the container, I'm doing the following:

``````bash
cd /UI  # Move to build directory
sh package_ubuntu.sh  # Run build script (calls pyinstaller)
``````

Now lets copy our package python app back to our local machine and test it it runs as expected.

```bash
# Copy build folder back to local machine 
sudo docker cp lucid_nobel:UI ~/Downloads/test_container
# Run packaged binary
~/Downloads/test_container/dist/paPAMv2
```

The program worked as expected.
Now we can proceed to write the docker file based on our testing container.

### Write Dockerfile

Based on our tests, the Docker file we need to package python apps for Ubuntu is the following:

```dockerfile
FROM ubuntu:18.04

# Commands run at the image build
RUN apt update && apt install -y \
		python3.7 \
		python3-pip && \
	pip3 install \
		PyQt5==5.9 \
		PyInstaller==3.5 \
		numpy==1.17 \
		pandas==0.25 \
		matplotlib==3.1.1

```



We'll save this with the filename `dockerfile-pyinstall-ubuntu`.
If we mode to our directory where we saved the docker file, we can build the image.

```bash
sudo docker build -t rtbecard/pyinstaller-ubuntu -f dockerfile-pyinstall-ubuntu_bionic .
```

Don't worry about the path value `.` we provided.
As we're not adding any files to our image using `ADD` or `COPY`, this path will not be used for anything.

As the image is building, you should see the installation commands executing in the terminal.
Once complete,  you should see the new image listed with:

```bash
sudo docker image list
```

Now we have a docker file, along with a copy of the resulting image stored on our local machine.

Next, we'll test the Gitlab CI on this image.

### Dockerfile for Windows Binary



# Gitlab CI

Now we have a functional docker image for running `pyinstaller` we can focus on integrating this image into the Gitlab CI system.
Before testing on the remote gitlab repository, we can download the runner and test everything locally.
This is also a useful tool during development, as you can maintain a cleaner repository by testing before pushing new changes to your repository.

## Testing Locally

First we'll [install gitlab runner](https://docs.gitlab.com/runner/install/linux-manually.html) on our local machine.
Since our OS isn't supported, we had to manually download and install the deb file `deb/gitlab-runner_amd64.deb`.

Before using it we'll have to write a `gitlab-ci.yml` file holding the instructions for the runner. 
I followed the guide [here](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html) to build the following `yml`:

```yml
image: rtbecard/pyinstaller-ubuntu

build:
  stage: build
  script:
  - echo "------Packaging starting------"
  - cd src
  - sh package_ubuntu.sh && echo "------Packaging succesfull------"

  artifacts:
    paths:
    - src/dist/paPAMv2
```

Note that an indentation in `yml`  is expected to consist of 2 spaces.
Make sure to avoid using tabs or the file may be read incorrectly.

The instructions we wrote are simple:  Make a container from our docker image and run the build script.
The `artifacts` attribute specifies files (or folders) that we want to return after the build completes.
In this case, the returned file is our packaged python app.

Finally, we'll test this on the `gitlab-runner`.

```bash
sudo gitlab-runner exec docker --docker-pull-policy=if-not-present build 
```

Note the option `--docker-pull-policy=if-not-present`.
The default behaviour is to pull an image from docker hub.
Since our image only exists on our machine (so far), we need to tell docker to check for a local image before searching online to avoid errors.
Also, the runner can only run a single job at a time, hence the `build` argument at the end of the command.

So I don't forget this command, I wrapped it in a shell script named `gitlab-runner_local-testing.sh`.

```bash
#!/bin/bash
sudo gitlab-runner exec docker --docker-pull-policy=if-not-present build
```

Finally, to test the gitlab-runner build, I simple run `sh gitlab-runner_local-testing.sh` in my local repo.

If all went well, the terminal output should end with `Job suceeded`.

## Testing on the Remote Repository

### Upload image to Dockerhub

First we need to login to docker hub on our machine.
Below is the command I used to login (note that you need to have registered on [dockerhub](https://hub.docker.com/) first.

```bash
sudo docker login --username=rtbecard
```
Remember how I named my image `rtbecard/pyinstaller-ubuntu` earlier?
The `user/image` naming convention is a criteria for uploading images to docker hub.

After you're logged in, simply run the following commant to upload your docker image.

```
sudo docker push rtbecard/pyinstaller-ubuntu
```

After your  image is uploaded on docker hub, we can test it by rerunning the `gitlab-runner` using only pulled images.

```bash
sudo gitlab-runner exec docker --docker-pull-policy=always build
```

### Test on Repository

To test this on our gitlab repo, simply commit all the steps we've done and push it.
If all went well, the repo will detect the `.gitlab-ci.yml` file and intialte the CI pipeline.
After the build has passed, you can download the [artifacts](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html) from the sucessful build.
In this case, the artifact is our packed python app.
This artifact be used as a *download the latest version* link in the repo readme.

# Packaging for Windows

