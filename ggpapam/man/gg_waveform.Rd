% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/waveform.R
\name{gg_waveform}
\alias{gg_waveform}
\title{Plot waveform.}
\usage{
gg_waveform(
  filename,
  text_size = 11,
  xlim = NULL,
  units = NULL,
  text_size_axis_labels = 11,
  max_samples = 10000
)
}
\arguments{
\item{filename}{Name of a binary file holding the results of paPAM's waveform analysis.}

\item{text_size}{Size text of axis titles.}

\item{xlim}{Limits of x axis.  Must be a 2 element vector.}

\item{units}{A value which is a partial match of "pressure", "displacement", "velocity", or "acceleration".}

\item{max_samples}{Maximum numper of samples to plot.  Data will be uniformly subsampled to meet this.}

\item{text_size_axis_label}{Size of axis label text.}
}
\value{
A ggplot object.
}
\description{
Plot waveform.
}
\examples{
file <- "20220921181542_Boat10s_analyticEnvelope_sound-pressure_band-0-22050_rangeID-0_chan-p_fs-44100.bin")
gg_analytic_envelope(file, units = "pressure")
}
