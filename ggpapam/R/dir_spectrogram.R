#' Plot a directional spectrogram in ggplot2
#'
#' @param df A data.frame holding the output of papamv2's directional spectrogram analysis.
#' @param type One of "bearing", "vertical", "horiz_e", or "free_e"
#' @param text_size Size text of axis titles.
#' @param clim Limits of colorbar range.  Must be a 2 element vector.
#' @param units A value which is a partial match of "pressure", "displacement", "velocity", or "acceleration".
#' @param text_size_colorbar Size of colorbar label text.
#' @param text_size_axis_label Size of axis label text.
#' @param legend Position of legend: "right", "left", "bottom", or "top".
#' @param colormap Sets the colormap of the plot.  Value is passed to the `option` argument in `scale_color_viridis_c()`.
#' @param colorbar_length Length of the colorbar in normalized units.
#'
#' @return A ggplot object
#' @export
#' @examples
#' df <- read.csv("./20220814231554_S22A_directional-spectrogram_band-None_rangeID-0_fs-44100.csv")
#' gg_directional_spectrogram(df, units = "pressure")
gg_directional_spectrogram <- function(
    df, type = "bearing", text_size = 11, clim = NULL, units = NULL,
    text_size_colorbar = NULL, text_size_axis_labels = NULL,
    legend = "right", colormap = "viridis", colorbar_length = 0.1){

  opts <- c("top", "left", "right", "bottom")
  if(!legend %in% opts){
    stop("legend must be one of: \"top\", \"left\", \"right\", \"bottom\"")
  }

  opts <- c("bearing", "vertical", "eccentricity", "L_K")
  if(!type %in% opts){
    stop("type must be one of: \"bearing\", \"vertical\", \"eccentricity\", \"L_K\"")
  }

  label = list(
    bearing = "Relative Bearing (deg.)",
    vertical = "Vertical Angle (deg.)",
    eccentricity = "Spectral Eccentricity (e)")

  map = list(
    bearing = aes(y = Frequency, x = Time,
                  fill = Relative_bearing,
                  color = Relative_bearing),
    vertical = aes(y = Frequency, x = Time,
                   fill = Vertical_angle,
                   color = Vertical_angle),
    eccentricity = aes(y = Frequency, x = Time,
                  fill = Spectral_Eccentricity,
                  color = Spectral_Eccentricity),
    L_K = aes(y = Frequency, x = Time,
              fill = Pressure_intensity_index,
              color = Pressure_intensity_index)
    )

  ggplot(df) +
    theme_bw(base_size = text_size) +
    geom_tile(map[[type]]) +
    scale_fill_viridis_c(name = label[[type]] ,limits = clim, option = colormap) +
    scale_color_viridis_c(guide = "none", limits = clim, option = colormap) +
    ylab("Frequency (Hz)") + xlab("Time (seconds)") +
    guides(size = "none",
           fill = guide_colourbar(
             title.position = guide_pos_colorbar_title_pos[[legend]])) +
    theme(legend.title.align = 0.5,
          legend.text = element_text(size = text_size_axis_labels),
          axis.text = element_text(size = text_size_axis_labels)) +
    legend_colorbar_theme(colorbar_length, text_size_colorbar, legend)
}
