export COMPILE_PAPAM := ALL

.PHONY: cython source clean

cython:
	cd src; python3 setup.py build_ext --inplace

source:
	cd src; \
	python3 setup.py sdist; \
	mv dist/papamv2-*.tar.gz ../

all:
	cd src; \
	python3 setup.py build_ext --inplace; \
	python3 setup.py sdist; \
	mv dist/papamv2-*.tar.gz ../

clean:
	cd src
	rm -rf build
	rm -rf dist
	rm -rf papamv2.egg-info
