#!/bin/bash

echo "Making venv..."
#python3.10 -m venv venv --system-site-packages
python3.10 -m venv venv
source venv/bin/activate

echo "Installing python packages..."
# Cannot have multiple versions of setuptools
#pip3 install  "setuptools==60.*"
# This will use pyqt5 from site-packages, and install local copies of all other required packages.
# MPLLOCALFREETYPE makes matplotlib download required freetype library

# install all packages (except pyqt)
MPLLOCALFREETYPE=1 pip install -r  requirements_MAC.txt

deactivate
