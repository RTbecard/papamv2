#!/bin/bash

# sometimes a later version of setuptools is automatically installed
# force revert to correct version
./venv/bin/pip3.10 install --no-build-isolation ./src

