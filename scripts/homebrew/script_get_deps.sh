#!/bin/bash

# Get a list of package dependancies from a VENV
# This is used to make the dependancy list for creating new venvs

# make venv
python3.10 -m venv venv
source venv/bin/activate

# install packages
pip3 install homebrew-pypi-poet 
pip3 install -r requirements_UNIX.txt 
pip3 install ./papamv2-2.0.7.tar.gz

# get stanzas
poet scipy
poet tornado
poet dill
poet numpy
poet PyQt5
poet matplotlib
poet setuptools
poet pyqtgraph
poet colorcet
poet pandas
poet Cython

# Destroy the virtual environment
deactivate
rm -rf venv
