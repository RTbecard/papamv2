#!/bin/bash

# Set error flag (exit on command fail)
set -e

# Make venv
python3.10 -m venv ./venv --system-site-packages
./venv/bin/pip3.10 install "wheel==0.37.*"
./venv/bin/pip3.10 install -r ./requirements_UNIX.txt
./venv/bin/pip3.10 install ./papamv2-VERSION.tar.gz
# sed is used to replave VERSION with the proper pacakge version number.

# Make executable
printf "%s\n%s\n" '#!/bin/bash' "./venv/bin/python3.10 -m papamv2" > run_papamv2
chmod +x run_papamv2
