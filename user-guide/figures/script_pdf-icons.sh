#!bin/bash

# exit on error
set -e

# convert svg icons into pdfs

# ------ required apps
# sudo apt-get install librsvg2-bin

# Make pdf folder
if [[ ! -d pdf ]]; then mkdir pdf; fi

# get list of svg files
files=$(ls ./svg/*.svg)

echo "$files"

for file in $files; do

	bfile=$(basename $file)
	bfile2="${bfile%.*}"
	echo "$bfile2"

	rsvg-convert -f pdf -o "./pdf/$bfile2.pdf" "./svg/$bfile2.svg"

done

echo "Finsied converting to pdf"
